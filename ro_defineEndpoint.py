# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Dialog to get coordinates by mouse-click on the map.

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""

#from qgis.core import *
from qgis.gui import QgsMessageBar
from PyQt4.QtCore import pyqtSignal
from PyQt4.QtGui import QDialog

from setup import setup__ as SETUP
from tools.iterate_well2 import get_position_of_catchment_end
from tools.iterate_well2 import create_wellPointLayer
from tools.qgis_toolbox import checkForFloat
from pyqtDialogs.dialog_RO_defineEndpoint import Ui_Dialog_RO_defineEndpoint
from qgis.core import QgsMapLayerRegistry


DX = {'coordinateEndX': 0, 'coordinateEndY': 0}

class Start_Dialog_RO_defineEndpoint(QDialog, Ui_Dialog_RO_defineEndpoint):
    '''class for the dialog'''

    signalXY = pyqtSignal(list)
    signalIter = pyqtSignal(bool)
    signalCoordEP = pyqtSignal(list)
    
    def __init__(self, parent, getC=None):
        QDialog.__init__(self, parent.iface.mainWindow())
        self.getC = getC
        self.setupUi(self)
        self.iface = parent.iface
        self.parent = parent
        #self.dictP = parent.dictP
        self.endPoint = [0, 0]
        self.pushButtonCoord.clicked.connect(self.getCoordinates)
        self.pushButtonMouse.clicked.connect(self.startC)
        self.pushButtonActive.clicked.connect(self.getActiveLayer)
        self.pushButtonAccept.clicked.connect(self.accept)
        self.pushButtonDismiss.clicked.connect(self.cancel)
        self.state = None
        self.getC.signalCoordEP.connect(self.signal_CoordEP)
        
        
    def signal_CoordEP(self):    
        self.lineEdit_xc.setText(str(self.getC.point[0]))
        self.lineEdit_yc.setText(str(self.getC.point[1]))        
        
    def cancel(self):
        self.closePlugin()


    def accept(self):
        if not self.state == 2: 
            self.parent.userDict['coordinateEndX'] = self.endPoint[0]
            self.parent.userDict['coordinateEndY'] = self.endPoint[1]
        #print('defend', self.parent.userDict['coordinateEndX'], self.parent.userDict['coordinateEndY'], 
        #      self.parent.userDict['staticLCalc'], self.state)
        self.parent.userDict['staticLCalc'] = True
        self.signalIter.emit(True)        
        self.closePlugin()


    def getCoordinates(self, point):
        '''read the X and Y coordinated from the lineEdits in the mask'''
        coorx = self.lineEditWest.text()
        coory = self.lineEditNord.text()
        if checkForFloat(coorx) and checkForFloat(coory):
            self.endPoint = [0, 0]
            self.endPoint[0] = float(coorx)
            self.endPoint[1] = float(coory)
            newWellDict = {'coordinateX': self.endPoint[0], 
                           'coordinateY': self.endPoint[1], 
                           'x': 0,
                           'ID': -999}    
            endp = create_wellPointLayer(newWellDict, crsID=SETUP.CRS, 
                                         name='Endpunkt', geo3='x')
            QgsMapLayerRegistry.instance().addMapLayer(endp)
            #print('self.endpoint',self.endPoint)
            self.state = 1
        else:
            self.iface.messageBar().pushMessage(u'Bitte gültige Koordinaten eingeben',
                                                level=QgsMessageBar.WARNING, duration=5)
            self.state = 1
            return


    def startC(self):
        '''read the X and Y coordinated from mouse clicks'''
        self.getC.set_function(1)
        self.getC.show()
        self.state = 2

    def getActiveLayer(self):
        '''read the enpoint with name given in
        SETUP.ENDPOINT from the active layer'''
        self.endPoint = get_position_of_catchment_end(active=self.iface.activeLayer())
        #print(self.endPoint)
        self.state = 3
        self.lineEdit_xa.setText(str(self.endPoint[0]))
        self.lineEdit_ya.setText(str(self.endPoint[1]))


    def closePlugin(self):
        self.iface.messageBar().pushMessage(u'Endpunkt ist ({}; {})...'.format(self.parent.userDict['coordinateEndX'],
                                                                               self.parent.userDict['coordinateEndY']),
                                            level=QgsMessageBar.INFO, duration=7)

        self.close()


    def closeEvent(self, event):
       event.accept()
