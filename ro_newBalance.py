# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Dialog to provide a list of additional extractions or injections for the total balance.

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""

from PyQt4.QtCore import pyqtSignal
from PyQt4.QtGui import QDialog
from PyQt4.QtGui import QTableWidgetItem

from qgis.gui import QgsMessageBar

from .tools.qgis_toolbox import checkForFloat
from pyqtDialogs.dialog_RO_newBalance import Ui_Dialog_RO_newBalance

class Start_Dialog_RO_newBalance(QDialog, Ui_Dialog_RO_newBalance):
    '''class for the dialog'''

    ### class attribute
    signalAddBal = pyqtSignal(list)


    def __init__(self, parent=None):
        QDialog.__init__(self, parent.mainWindow())
        self.setupUi(self)
        self.sumOld = 0
        self.sumNew = 0
        self.nameNew = ['' for i in range(10)]
        self.nameOld = ['' for i in range(10)]
        self.balNew = [0 for i in range(10)]
        self.balOld = [0 for i in range(10)]
        self.iface = parent
        self.pushButtonAccept.clicked.connect(self.acceptP)
        self.pushButtonReject.clicked.connect(self.rejectP)
        self.tableWidget.setSortingEnabled(True)

    def closePlugin(self):
        self.close()


    def closeEvent(self, event):
       event.accept()


    def acceptP(self):
        '''read the new balance components from the table.
        first column for names, second columns for rates'''
        for i in range(10):
            #print(i)
            if not self.tableWidget.item(i, 0) is None:
                txt = self.tableWidget.item(i, 0).text()
                self.nameNew[i] = txt
            if not self.tableWidget.item(i, 1) is None:
                bal = self.tableWidget.item(i, 1).text()
                if not checkForFloat(bal):
                    message = u'{} ist keine gültige Angabe!'.format(bal)
                    self.iface.messageBar().pushMessage(message,
                                                        level=QgsMessageBar.INFO,
                                                        duration=7)
                    return
                self.balNew[i] = float(bal)
        self.sumNew = self.get_sum()
        #print(self.sumNew, self.nameNew, self.balNew)
        self.signalAddBal.emit([self.sumNew, self.nameNew, self.balNew])
        self.closePlugin()


    def rejectP(self):
        self.set_bal()
        self.closePlugin()


    def get_sum(self):
        '''compute the sum over all given rates in the table

        Returns
        -------
        sum_rates : float
            the sum of all given balance rates'''
        return sum([float(i) for i in self.balNew])


    def set_bal(self, nameOld=None, nameNew=None, balOld=None, balNew=None):
        '''reset the monthly extraction rates in the table

        Parameters
        ----------
        nameOld : list, optional
            the initial (old) names of the components
        nameNew : list, optional
            the updated (changed) names of the components
        valueOld : list, optional
            the initial (old) values of the components
        valueNew : list, optional
            the updated (changed) values of the components'''

        if not nameNew is None:
            self.nameNew = nameNew
        if not balNew is None:
            self.balNew = balNew
        if not nameOld is None:
            self.nameOld = nameOld
        if not balOld is None:
            self.balOld = balOld
            
        for i, _ in enumerate(self.nameOld):
            txt = self.nameOld[i]
            self.tableWidget.setItem(i, 0, QTableWidgetItem(str(txt)))
            txt = self.balOld[i]
            self.tableWidget.setItem(i, 1, QTableWidgetItem(str(txt)))
        self.sumOld = self.get_sum()


