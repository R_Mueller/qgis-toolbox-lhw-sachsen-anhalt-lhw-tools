# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Provides the water balance info dialog

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""

#from PyQt4.QtCore import *
#from PyQt4.QtGui import *
#from qgis.core import *
from qgis.gui import QgsMessageBar
from PyQt4.QtGui import QDialog
from PyQt4.QtCore import pyqtSignal
from PyQt4.QtGui import QFont

#from .setup import setup__ as SETUP
#from .tools import qgis_toolbox
from pyqtDialogs.dialog_WB_main import Ui_Dialog_WB_main
from ro_components import Start_Ui_Dialog_RO_components


def fill(entryName, unit=None, result=None):
    '''format an ouput line
    
    Parameters
    ----------
    entryName : str
        the name of the water balance component
    unit : str, optional
        the unit of the water balance component
    result : str or float, optional
        the quantity for the water balance component
        
    Returns
    -------
    entry : str
        the formated str of the output line'''
        
    if unit is None:
        entry =  u'{}'.format(entryName).ljust(30,' ')
    else:
        entry = u'{}'.format(entryName).ljust(20,' ') \
                + u'{}'.format(unit).rjust(10,' ')
        
    if not result is None:
        if isinstance(result, float):
            entry += u'{:6.2f}'.format(result).rjust(10,' ') 
        else:
            entry += u'{}'.format(result).rjust(10,' ') 
    return entry + '\n'


class Start_Dialog_WB_main(QDialog, Ui_Dialog_WB_main):
    '''class for the main dialog of the stand alone water balance dialog'''
    signalGwn = pyqtSignal(list)

    def __init__(self, parent=None):
        QDialog.__init__(self, parent.mainWindow())
        self.setupUi(self) 
        self.iface = parent
        self.gwrC = [1, 1, 1, 0]
        self.idx = 0
        self.efl_layer = None
        self.textEdit.clear()
        self.textEdit.setCurrentFont(QFont("Courier New"))
        self.textEdit.setFontPointSize(8)        
     
        self.iface.mapCanvas().selectionChanged.connect(self.whhBalance)
        self.iface.legendInterface().currentLayerChanged.connect(self.updateLabel)
        
        self.gwrComp = Start_Ui_Dialog_RO_components(parent)
        self.gwrComp.set_interface([1, 1, 1, 0], [1, 1, 1, 0])
        self.pushButton_01.clicked.connect(self.gwrComp.show)
        self.gwrComp.signalGwn.connect(self.signal_gwr)
        
    
    def closePlugin(self):
        self.close()
        
    def closeEvent(self, event):
       event.accept()
       
    def updateLabel(self):
        '''update the label in dialog to show the 
        name of the current active layer'''
        if self.isVisible():
            layerName = self.iface.activeLayer().name()
            self.label_activeLayer.setText(layerName)
        
        
    def selectionchange(self, value):
        '''get new selection form the comboBox'''
        self.idx = value
        #print(self.idx)
        
       
    def signal_gwr(self, gwrList):
        '''register the list of flow components'''
        self.gwrC = gwrList
        
        
    def whhBalance(self):
        '''calculate the water balance for the selection show the results'''

        if not self.isVisible():
            return #print('not visible is false: ', self.isVisible())
        else:
            #print('whh is visible')
            results = self.calculateBalance()
            #print(results)
            ### publish results
            text = self.generateText(results)
            #print(text)
            self.textEdit.setPlainText(text)
            
            
    def calculateBalance(self):
        '''loop over all selected features to compute 
         area weighted water balance components
         
         Returns
         -------
         dict with the computed results and total area. 
         the keys are results and area'''

        rg1, rg2, drain, rh, pi, er, ep = 0, 0, 0, 0, 0, 0, 0
        total_weight = 0

        self.efl_layer = self.iface.activeLayer()
        
        id_rg2_efl = self.efl_layer.fieldNameIndex( "RG2" )    
        ### if id_rg2_efl is -1, than the layer has no results. end calculation
        if id_rg2_efl == -1:
            tmpstr = u"Aktiver Layer {} besitzt keine Wasserhaushaltsinformationen"
            layerName = self.iface.activeLayer().name()
            self.iface.messageBar().pushMessage(tmpstr.format(layerName),
                    level=QgsMessageBar.INFO, duration=3)
            return {'results': [rg2, rg1, drain, rh, pi, ep, er], 'area': total_weight}
        
        id_rg1_efl = self.efl_layer.fieldNameIndex( "RG1" )
        id_drain_efl = self.efl_layer.fieldNameIndex( "DRAIN" )
        id_rh_efl = self.efl_layer.fieldNameIndex( "RH" )        
        id_pi = self.efl_layer.fieldNameIndex( "PI" )
        id_ep = self.efl_layer.fieldNameIndex( "EP" )
        id_er = self.efl_layer.fieldNameIndex( "ER" )   
    
        efl_Features = self.efl_layer.selectedFeatures()

        ### area weighted mean for the balance components
        for efl_Feature in efl_Features:
            efl_Geometrie = efl_Feature.geometry()
            efl_Attributte = efl_Feature.attributes()
            # Geometry of intersection
            weight = efl_Geometrie.area()
            # use area of intersection as weight 
            # total of weights
            total_weight += weight
            rg2 += efl_Attributte[id_rg2_efl] * self.gwrC[0] * weight 
            rg1 += efl_Attributte[id_rg1_efl] * self.gwrC[1] * weight
            drain += efl_Attributte[id_drain_efl] * self.gwrC[2] * weight
            rh += efl_Attributte[id_rh_efl] * self.gwrC[3] * weight
            
            pi += efl_Attributte[id_pi] * weight 
            ep += efl_Attributte[id_ep] * weight
            er += efl_Attributte[id_er] * weight
            
        if total_weight > 0:
            rg2 /= total_weight
            rg1 /= total_weight
            drain /= total_weight
            rh = rh / total_weight
            
            pi /= total_weight
            ep /= total_weight
            er /= total_weight
        #print([rg2, rg1, drain, rh, pi, ep, er])        
        return {'results': [rg2, rg1, drain, rh, pi, ep, er], 'area': total_weight}
        

    def generateText(self, results):
        '''Create an string with the results for presentation in the textEdit.
        
        Parameters
        ----------
        results : dict
            dictionary with the results and the area
        
        Returns
        -------
        string : str
            the results
        '''
        string = fill(u'Fläche', unit='[km2]', result=results['area']/1000000)
        string += '\n'
        string += fill(u'Komponente', result='mm/Jahr')
        #string += 'Komponente\t\t\t[mm/Jahr]\n'
        string += '='*40 + '\n'
        if self.gwrC[0] == 1:
            string += fill('RG2', result=results['results'][0])
        if self.gwrC[1] == 1:
            string += fill('RG1', result=results['results'][1])
        if self.gwrC[2] == 1:
            string += fill('Drain', result=results['results'][2])                  
        if self.gwrC[3] == 1:
            string += fill('Rh', result=results['results'][3])
        string += '-'*40 + '\n'
        sumGWN = sum(results['results'][0:4])
        string += fill('GWN', result=sumGWN) 
        string += '\n'*2
        string += fill('PI', result=results['results'][4])
        string += fill('ETP', result=results['results'][5])
        string += fill('ETR', result=results['results'][6])
        string += '-'*40 + '\n'
        q = max(results['results'][4] - results['results'][6], 0)
        string += fill(u'PI-ETR (~Q)', result=q)
        q = max(results['results'][4] - results['results'][6]-max(sumGWN, 0), 0)
        string += fill(u'PI-ETR-GWN (~Qo)', result=q)
        return string
