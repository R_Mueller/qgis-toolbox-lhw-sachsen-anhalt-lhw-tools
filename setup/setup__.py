# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Provides the configuration

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""

import json
####################################################################
####################################################################
### CONFIGURATION 
### Konfiguration
####################################################################
####################################################################



#######################
### PATHS / Pfadangaben
#######################

### PATH TO THE ArcEGMO SHAPE FILES
### Pfad zu den Shapedateien mit den ArcEGMO Simulationsergebnissen
BASEFOLDER = 'C:/Users\Ruben.Mueller/.qgis2/python/plugins/shapes/'  
### PATH TO FOLDER WHERE THE TEMPORARY USER DATA (WORK STATUS) IS SAVED
### Pfad zum Verzeichnis in dem die temporären Nutzerdaten abgelegt werden
TEMPFOLDER = 'C:/Users\Ruben.Mueller/.qgis2/temp/' #

### EXPANSION // ERWEITERUNG ###
### PATH TO THE BASEFOLDER OF THE LOKAL ArcEGMO INSTANCE FOR SIMULATION
### Pfad zum Basisverzeichnis der lokalen ArcEGMO-Installation zur Berechnung
ARCEGMOPATH = 'd:/SA_Lokal'
### PATH TO THE MODEL OF THE FEDERAL STATE
### Pfad zum Landesmodell
LMPATH = 'd:/SA_Modell'
### NAME OF THE ArcEGO EXECUTABLE (in ARCEGMOPATH)
### Name der ausführbaren Datei von ArcEGMO  (in ARCEGMOPATH)
ARCEGMOEXE = 'ae5.exe'

#######################
### MISC SETTINGS // sonstige Einstellungen
#######################

### TOLERANCE FOR ACCOUNTING (GWR equals EXTRACTION)
### Toleranz für die Bilanzierung (Vergleich GWR und Entnahme)
EPSBALANCE = 0.01
### TOLERANCE FOR ITERATION (WELL CATCHMENT)
### Toleranz für die Iterative Bestimmung des Brunneneinzugsgebiets
EPSITER = 0.01
### MAX ITERATION STEPS (WELL CATCHMENT)
### Maximale Anzahl Iterationen (Bestimmung des Brunneneinzugsgebiets)
ITERMAX = 80
### ITERATION STEP TRESHOLDS AFTER WHICH RELAXATION KICKS IN (RELAXB1 < RELAXB2 < MAXITER!)
### Iterationen nach denen die Relaxation eskaliert (RELAXB1 < RELAXB2 < MAXITER!)
RELAXB1 = 30
RELAXB2 = 50
### COORDINATE SYSTEM CRS NUMBER FOR QGIS
### Kennung des Koordinatenbezugssystems
CRS = 25832
### SCALE TO ZOOM ON COORDINATE IN METERS
### Zoom auf x Meter auf Koordinaten
SCALE = 500
### ENCODING OF OPERATING SYSTEM (ENCODING that is read from QT widgets)
### Encoding des Betriebssystems (wird von QT widgets gelesen)
ENCODING = 'ISO-8859-1'
### NUMBER OF EXTRACTION DAYS PER YEARS 
### Anzahl der Entnahmetage je Jahr
EXTRDAYS = 130
#######################
### LAYER NAMES IN THE FEDERAL STATE MODEL// Layernamen im Landesmodell
#######################

### FEDERAL STATE AREA LAYER NAME (equals FILENAME)
### Name (und Dateiname) des Untersuchungsgebiets-Layer (Sachsen-Anhalt)
AREALAYER = 'untersuchungsgebiet'
### ELEMENTARY AREA LAYER NAME (equals FILENAME)
### Name (und Dateiname) des Elementarflächen-Layers
EFLLAYER = 'efl_ergebnisse_2018'
### SUB CATCHMENT LAYER NAME (equals FILENAME)
### Name (und Dateiname) des Teileinzugsgebiets-Layer
TGLAYER = 'tg_ergebnisse_2018'
### USER DEFINED END POINT LAYER NAME (equals FILENAME)
### Name (und Dateiname) des Layers mit den nutzerdefinierten Endpunkten
ENDPOINT = 'Endpunkt'
### RIVER ARC LAYER NAME (equals FILENAME)
### Name (und Dateiname) des Layers mit den Fließgewässerabschnitten (FGW)
FGWLAYER = 'fgw24'
### RIVER NODE LAYER NAME (equals FILENAME)
### Name (und Dateiname) des Layers mit den FGW Knoten
FGWNLAYER = 'fgw24_n'
### GAUGE LAYER NAME (equals FILENAME)
### Name (und Dateiname) des Layers mit Pegeln, die DIFGA-Ergebnisse tragen
GAUGELAYER = 'difga_pegel'

### THIS LIST IS PRESENTED IN THE DROP DOWN MENU TO CHOOSE THE 
### LAYER WITH THE WATER BALANCE DATA FOR ACOOUNTING + WATER BALANCE DIALOG
### Die Einträge dieser Liste werden im DropDown-Menü in der Bilanzierung
### und dem Wasserhaushaltsfenster zur Auswahl angeboten
WHHLAYER1 = [EFLLAYER, 'Aktiver Layer', TGLAYER]


#######################
### EXTENSION // Erweiterung
### LAYER NAMES IN THE FEDERAL STATE MODEL// Layernamen im Landesmodell
#######################

### SUB CATCHMENT LAYER NAME (equals FILENAME)
### Name (und Dateiname) des Teileinzugsgebiets-Layer
TGLAYERM = 'tg_ergebnisse_2018'
### STREAM SECTION POINTS LAYER NAME (equals FILENAME)
### Name des Layers mit Punktinformationen Fließgewässerabschnitte
GWLAYERM = 'fgw24_n'
### STREAM SECTIONS LAYER NAME (equals FILENAME)
### Name des Layers mit Informationen Fließgewässerabschnitte
FGWLAYERM = 'fgw24'
### ELEMENTARY AREA LAYER NAME (equals FILENAME)
### Name (und Dateiname) des Elementarflächen-Layers
EFLLAYERM = 'efl_ergebnisse_2018'


#######################
### data base settings
#######################

### HOST
HOST_RW = '127.0.0.1'         # !!!
### PORT
PORT_RW = '5432'              # !!!
### NAME
DB_RW = 'whh_tools'           # !!!

USERFILE = 'C:\\Users\\Ruben.Mueller\\.qgis2\\python\\users.json'      # !!!
### THIS DICT PROVIDES A RELATION BETWEEN WINDOWS USER AND POSTGRES USER + PASSWORT
### Beziehung zwischen Windows-Anmeldungsname und Postgresnutzer + Passwort
with open(USERFILE, 'r') as fb:
    USERMAP = json.load(fb)

### IF A WELL IS WITHIN THIS TANGE, IT IS DETECTED TO BE A VIOLATION IN THE DB
### Innerhalb dieser Angabe in m zählen alle Koordinaten von Brunnen als gleich
WELL_PROXIMITY = 10

####################################################################
####################################################################
### DO NOT EDIT BELOW 
### Ab hier nicht verändern
####################################################################
####################################################################

### EXTENSION
LN_RELATIONSHIP = {'GEWAESSER': 1100,
    'FEUCHTFLAECHEN': 1110,
    'GRUENLAND': 1210,
    'TROCKEN/HALBTROCKENRASEN': 1230,
    'STAUDENFLUR': 1240,
    'HEIDE': 1300,
    'WALD': 1400,
    'LAUBWALD': 1410,
    'NADELWALD': 1420,
    'MISCHWALD': 1430,
    'FEUCHTWALD': 1440,
    'GEHOELZ': 1450,
    'BLOCKSCHUTTWALD': 1460,
    'STREUOBSTWIESE': 1470,
    'ACKER': 1510,
    'ERWERBSGARTENBAU': 1520,
    'WEINBAU': 1530,
    'BEBAUTER BEREICH': 1600,
    'BEBAUTER BEREICH1': 1610,
    'BEBAUTER BEREICH2': 1620,
    'BEBAUTER BEREICH3': 1630,
    'BEBAUTER BEREICH4': 1640,
    'VERKEHRSFLAECHEN': 1700,
    'VEGETATIONSFREIE FLAECHE': 1900}


### DATABASE
DB_USERS = {### KEY IN USERDICT --> LOOK UP @ FIELD IN DB
            'table': 'wnv_lhw',
            'id': 'id',
            'geom': 'geom',
            'coordinateY': 'hw',
            'coordinateX': 'rw',
            #'JAHR_TM3': 'g_max_tqm_a',
            'applicationNumber': 'az',
            'nutzzweck': 'bezeichnung_nutzzweck',
            'nutzart': 'bezeichnung_der_nutzungsart',
            'nachtrag': 'nachtrag_nr',
            'regnr1': 'reg_nr2',
            'BIL_JAHR_TM3': 'b_max_tqm_a',                  # !!!
            'userName': 'name',
            'userVorName': 'adr_ vorname',
            ### NAME OF FIELD TO FILL --> KEY IN USERDICT
            'fields': {'name': 'userName',
                       'adr_ vorname': 'userVorName',###
                       'az': 'applicationNumber',
                       'b_max_tqm_a': 'BIL_JAHR_TM3',       # !!!
                       'hw': 'coordinateY',
                       'rw': 'coordinateX',
                       'nutzart': 'nutzart',
                       'nutzzweck': 'nutzzweck',
                       'lkr': 'lkr',
                       'gemeinde': 'gemeinde',
                       'owk_st': 'owk_st',
                       'ezg_name': 'ezg_name',
                       'geweasser': 'gewaesser',
                       'gwk_st': 'gwk_st',
                       'bilanzgebiet': 'bilanzgebiet',
                       'gw_stock': 'gw_stock',
                       'gw_schutz': 'gw_schutz',
                       'bearb_lhw': 'bearb_lhw',
                       'bemerkungen': 'bemerkungen',
                       'b_qm_d': 'b_qm_d',
                       'b_qm_h': 'b_qm_h',
                       'b_mit_tqm_a': 'b_mit_tqm_a', 
                       'g_qm_h': 'g_qm_h',
                       'g_mit_tqm_a': 'g_mit_tqm_a',
                       'g_max_tqm_a': 'g_max_tqm_a',
                       'g_qm_d': 'g_qm_d',
                       'date_sn': 'date_sn',
                       'date_a': 'date_a',
                       'date_b': 'date_b',
                       'Status': 'currentStatusT',
                       'gemark': 'gemark',
                       'flur': 'flur',
                       'flurst': 'flurst',
                       'bescheid': 'bescheid',
                       'befr_zeitr': 'befr_zeitr',
                       'bereg_flaech': 'bereg_flaech',
                       'pegel': 'pegel',
                       'einstellwert': 'einstellwert',
                       'status': 'currentStatusT'}
            }
            
### DATABASE
DB_USERS = {### KEY IN USERDICT --> LOOK UP @ FIELD IN DB
            'table': 'wnv_lhw',
            'id': 'id',
            'geom': 'geom',
            'coordinateY': 'hw',
            'coordinateX': 'rw',
            #'JAHR_TM3': 'g_max_tqm_a',
            'applicationNumber': 'az',
            'nutzzweck': 'bezeichnung_nutzzweck',
            'nutzart': 'bezeichnung_der_nutzungsart',
            'nachtrag': 'nachtrag_nr',
            'regnr1': 'reg_nr2',
            'BIL_JAHR_TM3': 'b_max_tqm_a',                  # !!!
            'userName': 'name',
            'userVorName': 'adr_ vorname',
            ### NAME OF FIELD TO FILL --> KEY IN USERDICT
            'fields': {'name': 'userName',
                       'adr_ vorname': 'userVorName',###
                       'az': 'applicationNumber',
                       'b_max_tqm_a': 'BIL_JAHR_TM3',       # !!!
                       'hw': 'coordinateY',
                       'rw': 'coordinateX',
                       'nutzart': 'nutzart',
                       'nutzzweck': 'nutzzweck',
                       'lkr': 'lkr',
                       'gemeinde': 'gemeinde',
                       'owk_st': 'owk_st',
                       'ezg_name': 'ezg_name',
                       'geweasser': 'gewaesser',
                       'gwk_st': 'gwk_st',
                       'bilanzgebiet': 'bilanzgebiet',
                       'gw_stock': 'gw_stock',
                       'gw_schutz': 'gw_schutz',
                       'bearb_lhw': 'bearb_lhw',
                       'bemerkungen': 'bemerkungen',
                       'b_qm_d': 'b_qm_d',
                       'b_qm_h': 'b_qm_h',
                       'b_mit_tqm_a': 'b_mit_tqm_a', 
                       'g_qm_h': 'g_qm_h',
                       'g_mit_tqm_a': 'g_mit_tqm_a',
                       'g_max_tqm_a': 'g_max_tqm_a',
                       'g_qm_d': 'g_qm_d',
                       'date_sn': 'date_sn',
                       'date_a': 'date_a',
                       'date_b': 'date_b',
                       'Status': 'currentStatusT',
                       'gemark': 'gemark',
                       'flur': 'flur',
                       'flurst': 'flurst',
                       'bescheid': 'bescheid',
                       'befr_zeitr': 'befr_zeitr',
                       'bereg_flaech': 'bereg_flaech',
                       'pegel': 'pegel',
                       'einstellwert': 'einstellwert',
                       'status': 'currentStatusT'}
            }
            
DB_LAND = {### KEY IN USERDICT --> LOOK UP @ FIELD IN DB
           'table': 'wnv_land',
           'id': 'id',
           'geom': 'geom',
           'coordinateY': 'utm_nord',
           'coordinateX': 'utm_ost',
           'BIL_JAHR_TM3': 'q_max_tm3_a',                    # !!!
           'applicationNumber': 'reg_nr1',
           'nutzart': 'bezeichnung_der_nutzungsart',
           'nutzzweck': 'bezeichnung_nutzzweck',
           'nachtrag': 'nachtrag_nr',
           'regnr1': 'reg_nr2',
           'userName': 'adr_ name',
           'userVorName': 'adr_ vorname',
           ### NAME OF FIELD TO FILL --> KEY IN USERDICT
           'fields': {'adr_ name': 'userName',###
                      'adr_ vorname': 'userVorName',###
                      'reg_nr1': 'applicationNumber',###
                      'q_max_tm3_a': 'BIL_JAHR_TM3',###      # !!!
                      'utm_ost': 'coordinateX',###
                      'utm_nord': 'coordinateY',###
                      'nutzungsart': 'nutzart', ###
                      'nutzungszweck': 'nutzzweck',###
                      u'oberfl\xe4chenk\xe4rper': 'owk_st',###
                      u'grundwasserk\xe4rper': 'gwk_st',###
                      'bemerkung': 'bemerkungen',###
                      }###
            }

DB_EZG = {### KEY IN USERDICT --> LOOK UP @ FIELD IN DB
          'table': 'ezg',
          'id': 'id_wnv_lhw',
          'geom': 'geom',
          'DAY_M3': 'entnahme_m3d',
          'days': 'entnahmetage', 
          ### NAME OF FIELD TO FILL --> KEY IN USERDICT
           'fields': {'flaeche': 'area',
                      'gwn': 'GWR flex',
                      'dateipfad': 'path_statement',
                      'endpunkt_ost': 'coordinateEndX',
                      'endpunkt_nord': 'coordinateEndY',
                      'entnahme_m3d': 'DAY_M3',               # !!!
                      'entnahmetage': 'days'}}                # !!!


DB_EXTR = {### KEY IN USERDICT --> LOOK UP @ FIELD IN DB
           'table': 'ist_mengen',
           'id': 'id_wnv_lhw',
           'geom': None,
           ### NAME OF FIELD TO FILL --> KEY IN USERDICT
           'fields': {'name': 'userName',
                      'az': 'applicationNumber',
                      'g_max_tqm/a': 'BIL_JAHR_TM3'}}         # !!!

DB_CONF = {'name': 'whh_daten',
           'wnv_lhw': DB_USERS,
           'wnv_land': DB_LAND,
           'ezg': DB_EZG,
           'ist_mengen': DB_EXTR}