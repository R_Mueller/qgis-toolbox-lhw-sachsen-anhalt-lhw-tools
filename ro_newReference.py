# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Dialog to provide a new application number if user already exists.

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""

from PyQt4.QtGui import QDialog
from pyqtDialogs.dialog_RO_newReference import Ui_Dialog_RO_newReference
from .setup import setup__ as SETUP

        

class Start_Dialog_RO_newReference(QDialog, Ui_Dialog_RO_newReference):
    '''class for the dialog'''
    
    def __init__(self, parent=None, userDict = None):
        QDialog.__init__(self, parent.mainWindow())     
        self.setupUi(self) 
        self.iface = parent
        self.userDict = userDict
        self.pushButton.clicked.connect(self.readRef)
        txt = u'Neues Aktenzeichen für "'+ self.userDict['applicationNumber'] +u'" eingeben:'
        self.label.setText(txt.encode(encoding=SETUP.ENCODING))

        
    def readRef(self):
        self.userDict['applicationNumber'] = self.lineEdit.text()
        self.close()
        
        
    def setLabel(self):
        txt = u'Neues Aktenzeichen für "'+ self.userDict['applicationNumber'] +u'" eingeben:'
        self.label.setText(txt.encode(encoding=SETUP.ENCODING))
        self.lineEdit.setText(self.userDict['applicationNumber'].encode(encoding=SETUP.ENCODING))

        
    def closeEvent(self, event):
       event.accept()
