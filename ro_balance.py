# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Dialog for the water balance.

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""

from PyQt4.QtGui import QDialog
from PyQt4.QtGui import QFont
from PyQt4.QtCore import QPyNullVariant
from qgis.gui import QgsMessageBar
from qgis.core import QgsMapLayerRegistry
from PyQt4.QtCore import QSettings
from qgis.core import QgsFeatureRequest

from .setup import setup__ as SETUP
from pyqtDialogs.dialog_RO_balance import Ui_Dialog_RO_balance
from ro_selectExtraction import Start_Dialog_RO_selectExtraction

def fill(entryName, unit=None, result=None):
    '''format an ouput line

    Parameters
    ----------
    entryName : str
        the name of the water balance component
    unit : str, optional
        the unit of the water balance component
    result : str or float, optional
        the quantity for the water balance component'''
    if unit is None:
        entry =  u'{}'.format(entryName).ljust(37,' ')
    else:
        entry = u'{}'.format(entryName).ljust(30,' ') \
                + u'{}'.format(unit).rjust(7,' ')

    if not result is None:
        if isinstance(result, float):
            entry += u'{:6.3f}'.format(result).rjust(10,' ')
        else:
            entry += u'{}'.format(result).rjust(10,' ')
    return entry + '\n'


def checkv(v):
    return True if isinstance(v,(float, int)) else False


class Start_Dialog_RO_balance(QDialog, Ui_Dialog_RO_balance):
    '''class for the  dialog'''
    #signalAcc = pyqtSignal(list)

    def __init__(self, parent=None,
                 userDict=None
                ):
        QDialog.__init__(self, parent.mainWindow())
        self.setupUi(self)
        self.iface = parent
        self.userDict = userDict
        self.pushButton.clicked.connect(self.account)
        self.pushButton_2.clicked.connect(self.reset)
        #self.signal_values = [userDict['GWR total'], userDict['JAHR_TM3 total']]
        self.textEdit.clear()
        self.textEdit.setCurrentFont(QFont("Courier New"))
        self.textEdit.setFontPointSize(8)

        self.selextr = Start_Dialog_RO_selectExtraction(parent, self.userDict)


    def closePlugin(self):
        self.close()


    def closeEvent(self, event):
       event.accept()


    def reset(self):
        '''clear the selection (otherwise we have will problems 
        when doing the selection multiple times)'''
        self.userDict['uIC'] = ([], [], [], [])
        message = u'Auswahl zurückgesetzt. Bilanzierung mit neuer Auswahl starten.'
        self.iface.messageBar().pushMessage(message,
                                                level=QgsMessageBar.WARNING,
                                                duration=7)
        return


    def account(self):
        '''calculate the water balance for the selection show the results'''
        #SETUP.EPSBALANCE = 0.01
        #print('-->', self.userDict['JAHR_TM3'])
        try:
            float(self.userDict['BIL_JAHR_TM3'])
        except:
            message = 'Achtung Entnahmerate nicht definiert!'
            self.iface.messageBar().pushMessage(message,
                                                level=QgsMessageBar.WARNING,
                                                duration=7)
            return
        
        numItems = self.comboBox.count()
        ### only if catchments are given
        if numItems > 0:
            layerNameWell = self.comboBox.currentText()
            layerNameWHH = self.comboBox_2.currentText()

            extrTotal = 0
            if not self.userDict is None \
            or self.userDict['balanceCatchment'] == '(nicht gesetzt)':
                extrTotal, tmp = self.calculateTotalExtraction(layerNameWell)
                
            if not self.userDict['uIC'][0]:
                self.userDict['uIC'] = tmp
                
            
            self.userDict['TM3_JAHR total'] = extrTotal
            
            ### dialog for selection of other users intersecting the catchment
            self.selextr.popTable()
            self.selextr.exec_()
            
            ### do accounting
            results = self.calculateBalance(layerNameWell, layerNameWHH,
                                            self.userDict['gwRechargeComponents'])
            ### generate text
            text = self.generateText(results)
            self.userDict['results_GWN'] = results
            self.userDict['GWR total'] = results['GWR']
            self.userDict['account_text'] = text
            ### signal new results
            #self.signal_values = [results['GWR'], extrTotal, text]
            #self.signalAcc.emit(self.signal_values)
            ### publish results
            self.textEdit.setPlainText(text)


    def calculateBalance(self, layerNameWell, layerNameWHH, check_wert):
        '''loop over all selected features to compute
         area weighted water balance components

         Parameters
         ----------
         layerNameWell : str
             name of the layer with the wells
         layerNameWHH : str
            name of the layer with water balance data
        check_wert : list
            the selected flow components

         Returns
         -------
         dict with the computed results and total area.
         the keys are results and area'''
        try:
            catch_layer = QgsMapLayerRegistry.instance().mapLayersByName(layerNameWell)[0]
        except:
            fmtstr = u"Ungültiger Layer gesetzt."
            self.iface.messageBar().pushMessage(fmtstr,
                                                level=QgsMessageBar.WARNING, duration=7)
            return
        #catch_layerFeat = catch_layer.getFeatures()

        if layerNameWHH == 'Aktiver Layer':
            efl_layer = self.iface.activeLayer()
        else:
            try:
                efl_layer = QgsMapLayerRegistry.instance().mapLayersByName(layerNameWHH)[0]#SETUP.EFLLAYER
            except:
                fmtstr = u"Ungültiger Layer gesetzt."
                self.iface.messageBar().pushMessage(fmtstr,
                                                    level=QgsMessageBar.WARNING, duration=7)
                return
        QSettings()
        id_rg2_efl = efl_layer.fieldNameIndex( "RG2" )
        id_rg1_efl = efl_layer.fieldNameIndex( "RG1" )
        id_drain_efl = efl_layer.fieldNameIndex( "DRAIN" )
        id_rh_efl = efl_layer.fieldNameIndex( "RH" )
        #id_EFLID_efl = efl_layer.fieldNameIndex( "EFLID" )
        #total_weight = 0
        GWN = 0
        RG1 = 0
        RG2 = 0
        RD = 0
        RH = 0
        weight_t = 0
        i = 1
        fid = None
        failed = 0
        for feature in catch_layer.getFeatures():
            fid = feature.id()
            break
        request = QgsFeatureRequest().setFilterFid(fid)
        feat = next(catch_layer.getFeatures(request))
        
        #for feat in  catch_layerFeat:
        areaG = feat.geometry().area()
        
        cands = efl_layer.getFeatures(QgsFeatureRequest().setFilterRect(feat.geometry().boundingBox()))
        
        for area_feature in cands:
            if feat.geometry().intersects(area_feature.geometry()):
                ueber = feat.geometry().intersection(area_feature.geometry())
                # use area of intersection as weight
                if ueber is None:
                    failed += 1
                    #print('failed', type(ueber))
                    continue
                weight = ueber.area()
                efl_Attributte = area_feature.attributes()
                # total of weights
                #total_weight += weight
                tmp = efl_Attributte[id_rh_efl]
                tempRH = tmp * weight if check_wert[3] and checkv(tmp) else 0
                tmp = efl_Attributte[id_drain_efl]
                tempRD = tmp * weight if check_wert[2] and checkv(tmp) else 0
                tmp = efl_Attributte[id_rg1_efl]
                tempRG1 = tmp * weight if check_wert[1] and checkv(tmp) else 0
                tmp = efl_Attributte[id_rg2_efl]
                tempRG2 = tmp * weight if check_wert[0] and checkv(tmp) else 0
                tempGWN = tempRH + tempRD + tempRG1 + tempRG2
                i += 1
                #print(tempRG1, efl_Attributte[id_rg1_efl], weight, i) 
                GWN += tempGWN
                RG1 += tempRG1
                RG2 += tempRG2
                RH += tempRH
                RD += tempRD
                
                weight_t += weight
        #print(weight_t, areaG)
        if failed:
            fmtstr = u"{} fehlerhafte EFL oder TG entdeckt (korrupter Geometry?)."
            self.iface.messageBar().pushMessage(fmtstr.format(failed),
                                                level=QgsMessageBar.WARNING, duration=7)
        return {'GWR': GWN/1e6, 'Area': areaG,
                'RG1': RG1/1e6, 'RG2': RG2/1e6,
                'RH': RH/1e6, 'RD': RD/1e6,
                'checks': check_wert}
        

    def calculateTotalExtraction(self, layerName):
        '''look if there are other extractions in the vicinity
        (intersecting with the catchment) and
        add them for a total extraction rate.

        Parameters
        ----------
        layerName : str
            name of the catchment layer

        Returns
        -------
        total_extr : float
            the total extraction rate'''
        try:
            catch_layer = QgsMapLayerRegistry.instance().mapLayersByName(layerName)[0]
            for feature in catch_layer.getFeatures():
                fid = feature.id()
                break
            request = QgsFeatureRequest().setFilterFid(fid)
            feat = next(catch_layer.getFeatures(request))
        except:
            fmtstr = u"Layer {} nicht mehr vorhanden."
            self.iface.messageBar().pushMessage(fmtstr.format(layerName),
                                                level=QgsMessageBar.WARNING, duration=7)
            return 1, ([], [], [], [])

        name = []
        adr_vorname = []
        adr_name = []
        rate = []
        art = []
        zweck = []
        nachtrag = []
        regnr1 = []

        QSettings()

        total_extr = 0
        
        def san(item):
            if isinstance(item, QPyNullVariant):
                item = '-'
            return item

        for layername in ('wnv_land', 'wnv_lhw'):
            lname = SETUP.DB_CONF[layername]['table']
            efl_layer = QgsMapLayerRegistry.instance().mapLayersByName(lname)[0]
            #print(layername)
            id_extr = efl_layer.fieldNameIndex(SETUP.DB_CONF[layername]["BIL_JAHR_TM3"])
            id_name = efl_layer.fieldNameIndex(SETUP.DB_CONF[layername]['applicationNumber'])
            id_adr_vorname = efl_layer.fieldNameIndex(SETUP.DB_CONF[layername]['userVorName'])
            id_adr_name = efl_layer.fieldNameIndex(SETUP.DB_CONF[layername]['userName'])
            
            id_art = efl_layer.fieldNameIndex(SETUP.DB_CONF[layername]['nutzart'])
            id_zweck = efl_layer.fieldNameIndex(SETUP.DB_CONF[layername]['nutzzweck'])
            id_nachtrag = efl_layer.fieldNameIndex(SETUP.DB_CONF[layername]['nachtrag'])
            id_reg_nr2 = efl_layer.fieldNameIndex(SETUP.DB_CONF[layername]['regnr1'])

            cands = efl_layer.getFeatures(QgsFeatureRequest().setFilterRect(feat.geometry().boundingBox()))
            try:
                for area_feature in cands:
                    efl_Geometrie = area_feature.geometry()
                    efl_Attributte = area_feature.attributes()
                    if efl_Geometrie.intersects(feat.geometry()):
                        if not isinstance(efl_Attributte[id_extr], QPyNullVariant):
                            #print(efl_Attributte[id_extr], type(efl_Attributte[id_extr]))
                            total_extr += efl_Attributte[id_extr]
                            name.append(san(efl_Attributte[id_name]))
                            
                            tmp = efl_Attributte[id_adr_vorname] if id_adr_vorname > -1 else '-'
                            adr_vorname.append(san(tmp))
                            
                            tmp = efl_Attributte[id_adr_name] if id_adr_name > -1 else '-'
                            adr_name.append(san(tmp))
                            
                            tmp = efl_Attributte[id_extr] if id_extr > -1 else '-'
                            rate.append(san(tmp))
                            
                            tmp = efl_Attributte[id_art] if id_art > -1 else '-'
                            art.append(san(tmp))
                            
                            tmp = efl_Attributte[id_zweck] if id_zweck > -1 else '-'
                            zweck.append(san(tmp))
                            
                            tmp = efl_Attributte[id_nachtrag] if id_nachtrag > -1 else '-'
                            nachtrag.append(san(tmp))
                            
                            tmp = efl_Attributte[id_reg_nr2] if id_reg_nr2 > -1 else '-'
                            regnr1.append(san(tmp))
                            
            except TypeError:
                 fmtstr = u"""Bitte die SETUP-Datei auf korrekte Zuordnung in wnv_land und wnv_lhw prüfen.
                 [BIL_JAHR_TM3, applicationNumber, userVorName, userName, nutzart, nutzzweck, nachtrag, regnr1]"""
                 self.iface.messageBar().pushMessage(fmtstr.format(layerName),
                                                    level=QgsMessageBar.WARNING, duration=7)
                            
        extraction = ['n' for i in name]
        selection = ['j' for i in name]
        #print('total_extr', total_extr)
        return total_extr, (name, adr_name, adr_vorname, rate, zweck, art, nachtrag, regnr1, selection, extraction)


    def generateText(self, results):
        '''format an string for the textEdit

        Parameters
        ----------
        results : dict
            the water balance from calculateBalance
        extrTotal : float
            the total extraction rate from calculateTotalExtraction

        Returns
        -------
        the formated results string : str'''
        ### legal stuff
        string = fill('Nutzer', result=self.userDict['userName'])
        string += fill('Antragsnummer',
                       result=self.userDict['applicationNumber'])
        ### area and Q
        string += fill(u'Bezugsgebietsfläche', unit=u'     [km²]',
                       result=results['Area']/1000000)
        string += fill(u'QL',  unit=u' [l/s/km²]',
                       result=self.userDict['QL'])
        string += '-'*57 + '\n'


        string += fill('Entnahme lokal', unit=u'[Tm³/Jahr]',
                       result=self.userDict['BIL_JAHR_TM3'])
        
        ### Q from intersecting well catchments
        for i, use in enumerate(self.userDict['uIC'][-2]):
            if use in ('j', '1', 'y') and not self.userDict['uIC'][0][i].encode(encoding=SETUP.ENCODING)  \
            == self.userDict['applicationNumber'].encode(encoding=SETUP.ENCODING):
                if self.userDict['uIC'][-1][i] in ('j', '1', 'y'):
                    res = self.userDict['uIC'][3][i] * -1
                else:
                    res = self.userDict['uIC'][3][i]
                string += fill(self.userDict['uIC'][0][i], unit=u'[Tm³/Jahr]',
                               result=res)

        ### additional balance
        for i, namex in enumerate(self.userDict['names_addBalance']):
            if len(namex) > 0:
                string += fill(namex,  unit=u'[Tm³/Jahr]',
                       result=self.userDict['list_addBalance'][i])

        #print(type(self.userDict['JAHR_TM3']), type(self.userDict['total_addBalance']))
        ### the sum of all
        sumx = float(self.userDict['BIL_JAHR_TM3']) \
               + float(self.userDict['total_addBalance']) \
               + self.userDict['TM3_JAHR uIC']

        string += '-'*57 + '\n'
        string += fill('Summe Entnahmen',  unit=u'[Tm³/Jahr]',
                       result=sumx) + '\n'

        if results['checks'][0]:
            string += fill('RG2', unit=u'[Tm³/Jahr]',
                           result=results['RG2']).strip() \
                           + ' '*2 + '= ' \
                           + u'{:3.1f}'.format(results['RG2']/results['Area']*1000000) \
                           + u' mm/a\n'
        if results['checks'][1]:
            string += fill('RG1', unit=u'[Tm³/Jahr]',
                           result=results['RG1']).strip() \
                           + ' '*2 + '= ' \
                           + u'{:3.1f}'.format(results['RG1']/results['Area']*1000000) \
                           + u' mm/a\n'
        if results['checks'][2]:
            string += fill('RD', unit=u'[Tm³/Jahr]',
                                   result=results['RD']).strip() \
                           + ' '*2 + '= ' \
                           + u'{:3.1f}'.format(results['RD']/results['Area']*1000000) \
                           + u' mm/a\n'
        if results['checks'][3]:
            string += fill('RH', unit=u'[Tm³/Jahr]',
                                   result=results['RH']).strip() \
                           + ' '*2 + '= ' \
                           + u'{:3.1f}'.format(results['RH']/results['Area']*1000000) \
                           + u' mm/a\n'
                           
        string += '-'*57 + '\n'
        string += fill('Summe ist GWN',  unit=u'[Tm³/Jahr]',
                       result=results['GWR'])

        string += '='*57 + '\n'*2

        balance = results['GWR'] - sumx

        if abs(balance) <= SETUP.EPSBALANCE:
            tempa = u'Entnahme und GWN sind jeweils ' \
                       + self.userDict['BIL_JAHR_TM3'] + ' [Tm³/Jahr]. Bilanz ist ausgeglichen.'
            string += u'Entnahme und GWN sind jeweils ' \
                       + self.userDict['BIL_JAHR_TM3'] + ' [Tm³/Jahr]. Bilanz ist ausgeglichen.'
        else:
            temp = u' HÖHER ' if balance > SETUP.EPSBALANCE else u' NIEDRIGER '
            tempa = u'GWN ist ' + u'{:6.2f}'.format(abs(balance)) + u' [Tm³/Jahr]' + temp + 'als Entnahme(n)'
            string += u'GWN ist ' + u'{:6.2f}'.format(abs(balance)) + u' [Tm³/Jahr]' + temp + 'als Entnahme(n)'
            self.userDict['bilance_text'] = tempa
        return string

