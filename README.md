# README

## LHW_Toolbox
In this repository you will find the LHW_Toolbox for Landesbetrieb für Hochwasserschutz und Wasserwirtschaft Sachsen-Anhalt (LHW Sachsen-Anhalt).
The LHW_Toolbox assists in making reasoned statements about new groundwater extractions in the state of Saxony-Anhalt.

The main feature is the iterative approximation of the drawdown cone of an extraction well based on shape files of groundwater recharge rates 
(results of simulations with the water balance model ArcEGMO(c)) and informations about inflow direction and slope of the ground water.

Other users in the well catchment (drawdown cone) can be identified and the total balance of extractions and ground water recharge assessed.

This software will only work correctly if suitable a database and shape files are available.
For the configuration of the plug-in, set the setup__.py file in the SETUP folder accordingly.

This Software is designed as a plug-in for QGIS 2.18.14.

## Author
Ruben Müller
E-Mail: ruben.mueller@bah-berlin.de
Company: Büro für Angewandte Hydrologie, Berlin
web: www.bah-berlin.de

## Conception
Ruben Müller, Bernd Pfützner, Petra Hesse, Silke Mey
Büro für Angewandte Hydrologie Berlin

Martin Schneppmüller, Uta Rehn, Angelika Huber, Karin Gruschinski, Franziska Halbing, Ulrike Leifholz
LHW Sachsen-Anhalt

## Licence
This Software is provided under the GPL3.