# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Provides the DIFGA* info dialog
    * https://tu-dresden.de/bu/umwelt/hydro/ihm/hydrologie/services/software#ck_DIFGA2000

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""

import os
from qgis.gui import QgsMessageBar
from PyQt4.QtGui import QDialog
from PyQt4.QtCore import pyqtSignal


from .tools import qgis_toolbox
from .setup import setup__ as SETUP
from pyqtDialogs.dialog_DG_mains import Ui_Dialog_DG_mains


class Start_Dialog_DG_main(QDialog, Ui_Dialog_DG_mains):
    '''class for the main dialog of the stand alone water balance dialog'''
    signalGwn = pyqtSignal(list)

    def __init__(self, parent=None):
        QDialog.__init__(self, parent.mainWindow())
        self.setupUi(self) 
        self.iface = parent
  
        self.readParameter()
        if self.parameter is None:
            tmpstr = u"Konnte Difga_Parameter.txt nicht lesen. DIFGA nicht funktionsfähig."
            self.iface.messageBar().pushMessage(tmpstr,
                                                level=QgsMessageBar.WARNING, 
                                                duration=7)
        self.iface.mapCanvas().selectionChanged.connect(self.read_difga_layer)
        #self.iface.legendInterface().currentLayerChanged.connect(self.updateLabel)

    
    def closePlugin(self):
        self.close()
        
    def closeEvent(self, event):
       event.accept()
       
    def read_difga_layer(self):
        '''update the label in dialog to show the 
        name of the current active layer'''     
        if self.isVisible():
            #print(1)
            layerName = self.iface.activeLayer().name()
            
            if not layerName == SETUP.GAUGELAYER:
                 return
            else:
                #print(2)
                layer = self.iface.activeLayer()
                selFeature = layer.selectedFeatures()
                
                if len(selFeature) == 0:
                    return
                elif len(selFeature) > 1:
                    tmpstr = u"{}x Pegel selektiert. Bitte nur eine Selektion!"
                    self.iface.messageBar().pushMessage(tmpstr.format(len(selFeature)),
                        level=QgsMessageBar.INFO, duration=3)
                else:
                    if isinstance(selFeature, list):
                        selFeature = selFeature[0]
                    ### gauge
                    self.label_gauge.setText(selFeature.attributes()[0])
                    ### catchment
                    idt = layer.fieldNameIndex('EZG')
                    ezg = selFeature.attributes()[idt]
                    idt = layer.fieldNameIndex('Area')
                    area = selFeature.attributes()[idt]
                    tmp = ezg + ' (' + str(area) + u' km2)' 
                    self.label_catchment.setText(tmp)
                    ### timeseries
                    idt = layer.fieldNameIndex('Periode')
                    temp = selFeature.attributes()[idt]
                    temp = 'Zeitreihe von ' + temp
                    self.label_timeseries.setText(temp)
                    
                    ### mnq
                    idt = layer.fieldNameIndex('MNQ')
                    self.lineEdit_mnq.setText(str(selFeature.attributes()[idt]))
                    ### mq
                    idt = layer.fieldNameIndex('MQ')
                    self.lineEdit_mq.setText(str(selFeature.attributes()[idt]))
                    ### rd
                    idt = layer.fieldNameIndex('QD_y')
                    rd = float(selFeature.attributes()[idt])
                    self.lineEdit_rd.setText(str(selFeature.attributes()[idt]))
                    ### r
                    idt = layer.fieldNameIndex('QG1_y')
                    rg1 = float(selFeature.attributes()[idt])
                    self.lineEdit_rg1.setText(str(selFeature.attributes()[idt]))
                    ### r
                    idt = layer.fieldNameIndex('QG2_y')
                    rg2 = float(selFeature.attributes()[idt])
                    self.lineEdit_rg2.setText(str(selFeature.attributes()[idt]))
                    ### r
                    r = rd + rg1 + rg2
                    self.lineEdit_r.setText(str(r))
                    
                    tmp = rd/r*100
                    self.lineEdit_rdp.setText(u'{:3.1f}'.format(tmp))
                    tmp = rg1/r*100
                    self.lineEdit_rg1p.setText(u'{:3.1f}'.format(tmp))
                    tmp = rg2/r*100
                    self.lineEdit_rg2p.setText(u'{:3.1f}'.format(tmp))
                    tmp = (rg1 + rd)/r*100
                    self.lineEdit_rdp_2.setText(u'{:3.1f}'.format(tmp))

    def readParameter(self):
        bpath = os.path.dirname(__file__)
        #print(bpath)
        with open(os.path.join(bpath, 'setup', 'Difga_Parameter.txt')) as fid:
            tmp = fid.readlines()[1:]
            self.parameter = [i.strip() for i in tmp]

