# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Dialog to define the end points of a well catchment.
    An Endpoint can be defined either by the active point layer (with a single
    endpoint with a name from SETUP.ENDPOINT), or by giving the X and Y coorinates.

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""

from qgis.gui import QgsMessageBar
from PyQt4.QtGui import QDialog
from PyQt4.QtGui import QTableWidgetItem
from PyQt4.QtCore import QPyNullVariant
from pyqtDialogs.dialog_RO_selectExtraction import Ui_Dialog_RO_selectExtraction
from .setup import setup__ as SETUP


NUMENTRY = 10

class Start_Dialog_RO_selectExtraction(QDialog, Ui_Dialog_RO_selectExtraction):
    '''class for the dialog to define the end points of a well catchment'''


    def __init__(self, parent=None, dictP=None):
        QDialog.__init__(self, parent.mainWindow())
        self.setupUi(self)
        self.iface = parent
        self.dictP = dictP
        self.pushButton.clicked.connect(self.startSelection)


    def popTable(self):
        '''populate the table'''
        ### set the table
        self.tableWidget.setRowCount(len(self.dictP['uIC'][0]))
        self.tableWidget.setColumnCount(NUMENTRY)
        for i, name in enumerate(self.dictP['uIC'][0]):
            #print(name, str(self.dictP['uIC'][:][i]))
            if not self.dictP['uIC'][0][i].encode(encoding=SETUP.ENCODING)  \
            == self.dictP['applicationNumber'].encode(encoding=SETUP.ENCODING):
                if not isinstance(name, QPyNullVariant):
                    self.tableWidget.setItem(i, 0, QTableWidgetItem(name.encode(encoding=SETUP.ENCODING)))
                else:
                    self.tableWidget.setItem(i, 0, QTableWidgetItem('?'))
                for j in range(1, NUMENTRY):
                    if isinstance(self.san_t(j, i), QPyNullVariant):
                        self.tableWidget.setItem(i, j, QTableWidgetItem('-'))
                    else:
                        self.tableWidget.setItem(i, j, QTableWidgetItem(self.san_t(j, i)))

    def san_t(self, n, i):
        '''correct encoding for each entry. Nasty hack...'''
        tmp = '?'
        try:
            tmp = str(self.dictP['uIC'][n][i]).encode(encoding=SETUP.ENCODING)
        except:
            try:
                tmp = self.dictP['uIC'][n][i]  
            except:
                tmp = str(self.dictP['uIC'][n][i])
        finally:
            return tmp


    def startSelection(self):
        '''read table and include entry in balance or not'''
        addExtr = 0
        valid = [False, False]
        for i, name in enumerate(self.dictP['uIC'][0]):
            if not self.dictP['uIC'][0][i].encode(encoding=SETUP.ENCODING)  \
            == self.dictP['applicationNumber'].encode(encoding=SETUP.ENCODING):
                ### read
                rate = float(self.dictP['uIC'][3][i])
                use, valid[0] = self.check(self.tableWidget.item(i, NUMENTRY-2).text())
                plumi, valid[1] = self.check(self.tableWidget.item(i, NUMENTRY-1).text())
                
                if any(valid):
                    message = u'Falsche Angabe! Gültig: (0, 1, j, n).'
                    self.iface.messageBar().pushMessage(message,
                                                    level=QgsMessageBar.WARNING,
                                                    duration=4)
                    return
                ### save to userDict
                self.dictP['uIC'][-2][i] = use
                self.dictP['uIC'][-1][i] = plumi
                
                if use in ('j', '1', 'y'):
                    add = rate * -1 if plumi in ('j', '1', 'y') else rate
                    addExtr += add
        #print(name, str(self.dictP['uIC'][1]), str(self.dictP['uIC'][2]), str(self.dictP['uIC'][3]))
        #print(type(addExtr), addExtr)
        self.dictP['TM3_JAHR uIC'] = addExtr
        self.close()


    def check(self, k):
        '''ensure legal keywords'''
        chk = False if k in ('0', '1', 'j', 'n', 'y') else True
        return k, chk


    def closeEvent(self, event):
       event.accept()
