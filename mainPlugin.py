# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Provides the class Main. Top level of the plug-in.

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""


import os

from qgis.core import QgsCoordinateReferenceSystem
from qgis.gui import QgsMessageBar

from PyQt4.QtGui import QAction
from PyQt4.QtGui import QIcon

pluginPath = os.path.dirname(__file__)

from main_RO import Start_Dialog_RO_main
from main_WB import Start_Dialog_WB_main
#from main_AE import Start_Dialog_AE_main
from main_DGs import Start_Dialog_DG_main

from .setup import setup__ as SETUP
from .tools import qgis_toolbox


def load_layers():
    '''load some layers, if they are not already'''
    qgis_toolbox.load_layer(SETUP.EFLLAYER, set_active=False)
    qgis_toolbox.load_layer(SETUP.TGLAYER, set_active=False)
    qgis_toolbox.load_layer(SETUP.AREALAYER, set_active=False)
    qgis_toolbox.load_layer(SETUP.GAUGELAYER, set_active=False)

class Main:
    '''class for the plugin'''
    def __init__(self, iface):
        # super(Main, self).__init__() ### does not work here, base is old-school function!
        ### attributes
        self.iface = iface
        target_crs = QgsCoordinateReferenceSystem()
        target_crs.createFromId(SETUP.CRS, QgsCoordinateReferenceSystem.InternalCrsId )
        iface.mapCanvas().setDestinationCrs(target_crs)
        self.maske0 = Start_Dialog_RO_main(iface)
        self.maske1 = Start_Dialog_WB_main(iface)
        #self.maske2 = Start_Dialog_AE_main(iface)
        self.maske3 = Start_Dialog_DG_main(iface)
        self.iface.messageBar().pushMessage('Plugin geladen',
                                            level=QgsMessageBar.INFO, duration=3)

    def initGui(self):
        '''prepares the three entries for (1) reasoned opinion,
        (2) water balance (3) simulations with ArcEGMO(c)'''
        self.startButton0 = QAction('Bearbeitung von Stellungnahmen', self.iface.mainWindow())
        self.startButton0.setIcon(QIcon(os.path.join(pluginPath, 'icons', 'ro.png')))
        self.startButton0.triggered.connect(self.maske0.show)
        self.iface.addPluginToMenu('LHW Tools', self.startButton0)

        # self.startButton2 = QAction('Lokale Untersuchungen', self.iface.mainWindow())
        # self.startButton2.setIcon(QIcon(os.path.join(pluginPath, 'icons', 'arc_egmo.png')))
        # self.startButton2.triggered.connect(self.maske2.show)
        # self.iface.addPluginToMenu('LHW Tools', self.startButton2)
        
        self.startButton1 = QAction('Wasserhaushalt', self.iface.mainWindow())
        self.startButton1.setIcon(QIcon(os.path.join(pluginPath, 'icons', 'wb.png')))
        self.startButton1.triggered.connect(self.maske1.show)
        self.iface.addPluginToMenu('LHW Tools', self.startButton1)
        
        self.startButtonD = QAction('Ergebnisse DIFGA', self.iface.mainWindow())
        self.startButtonD.setIcon(QIcon(os.path.join(pluginPath, 'icons', 'difga.ico')))
        self.startButtonD.triggered.connect(self.maske3.show)
        self.iface.addPluginToMenu('LHW Tools', self.startButtonD)
        
        self.startButtonM = QAction('Kartengrundlagen laden', self.iface.mainWindow())
        self.startButtonM.setIcon(QIcon(os.path.join(pluginPath, 'icons', 'map.png')))
        self.startButtonM.triggered.connect(load_layers)
        self.iface.addPluginToMenu('LHW Tools', self.startButtonM)

    def unload(self):
        self.iface.removePluginMenu('LHW Tools', self.startButton0)
        self.iface.removePluginMenu('LHW Tools', self.startButton1)
        #self.iface.removePluginMenu('LHW Tools', self.startButton2)
        self.iface.removePluginMenu('LHW Tools', self.startButtonM)
        self.iface.removePluginMenu('LHW Tools', self.startButtonD)


