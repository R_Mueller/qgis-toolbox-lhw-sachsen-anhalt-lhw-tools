# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Dialog to select how to start the dialog for user management and reasoned statement.

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""

import os
import sys
import datetime

from PyQt4.QtGui import QDialog
from qgis.core import QgsMapLayerRegistry
from qgis.core import QgsFeatureRequest
from PyQt4.QtGui import QFileDialog
from PyQt4.QtGui import QApplication
#from PyQt4.QtCore import pyqtSignal

from qgis.gui import QgsMessageBar

from .tools import qgis_toolbox
from .setup import setup__ as SETUP
from .tools import database_toolbox as db_toolbox

from pyqtDialogs.dialog_RO_main import Ui_Dialog_RO_main
from ro_userManagement import Start_Dialog_RO_userManagement


DEBUG = False #True # 
###############
# default dict for all plugin data --> the "user dictionary"
###############
### TODO: clear entries after debug

if DEBUG:
    JAHR_TM3 = 240
    BC = 'Brunneneinzugsgebiet1'
    TAG_M3 = 2.0
    BIL_JAHR_TM3 = 500
else:
    JAHR_TM3 = 0.0
    BC = '(nicht gesetzt)'
    TAG_M3 = 0.0
    BIL_JAHR_TM3 = 0.0

def createUserContainer():
    userDict = {
    'mode': 0,
    'userName': '',#'undefiniert',
    'applicationNumber': '',#'undefiniert',
    'coordinateX': 0, #672935,
    'coordinateY': 0, #5831620,
    'coordinateEndX': 0, #672900,
    'coordinateEndY': 0, #5831500,
    'staticLCalc': False,
    'total_addBalance': 0,
    'names_addBalance': [],
    'list_addBalance': [],
    'extractionType': 'Y', ### Y annual, F monthly percent, M monthly amount
    'extractionPercents': [0, 0, 0, 5, 10, 25, 25, 25, 10, 0, 0, 0],
    'extractionRates': [0 for i in range(12)],
    'JAHR_TM3': JAHR_TM3,
    'BIL_JAHR_TM3': BIL_JAHR_TM3,
    'days': SETUP.EXTRDAYS,
    'DAY_M3': TAG_M3,
    'gwRechargeComponents': [1, 1, 1, 0],
    'catchmentWell': 'standard',
    'catchmentBalance' : 'standard',
    'currentStatus': 0, ### 0 în process, 1 rejected, 2 accepted,
    'currentStatusT': 'in Bearbeitung', ### 0 în process, 1 rejected, 2 accepted,
    'report': {
               'comments': '',
               'settings': [0, 0, 0]},
    'folderPermanent': 'default',
    'folderTemporary': 'default',
    'ID': 'default',
    'TG': 'default',
    'balanceCatchment': BC,
    'wellCatchment': '(nicht gesetzt)',
    'balanceCatchmentOrig': 'default',
    'wellPoint': '(nicht gesetzt)',
    'JAHR_TM3 total': 0,
    'GWR total': 0,  ### GWR from accounting,
    'QL': 0,
    'QL_eff': 0,
    'QL_selection': 0,
    'lastWellLayerNr': 1,
    'uIC': ([], [], [], [], [], [], [], []),
    'TM3_JAHR uIC' : 0,
    'layerName': None,
    'GWR flex': 0, ### GWR from flexible and fixed end point catchment computation
    'fromLayer': 'new',
    #########
    ## DB STUFF
    #########
    'lkr': '--',
    'userVorName': '',#'undefiniert',    
    'gemeinde': '--',
    'owk_st': '--',
    'gwk_st': '--',
    'tempPath': 'c:\\',
    'ezg_name': '',
    'bilanzgebiet': '',
    'b_qm_h': 0,
    'b_qm_d': 0,
     #'b_qm_a': [None, 0.5],
    'b_mit_tqm_a': 0,
    'b_max_tqm_a': 0,
    'g_qm_h': 0,
    'g_qm_d': 0,
     #'g_qm_a': [None, 0.5],
    'g_mit_tqm_a': 0,
    'g_max_tqm_a': 0,
    'bemerkungen': '',
    'bearb_lhw': '',
    'gw_stock':  '',
    'gw_schutz': '',
    'date_a': datetime.datetime(2000, 1, 1),
    'date_sn': datetime.datetime.now(),
    'date_b': datetime.datetime(2000, 1, 1),
    'nutzart': '',
    'nutzzweck': '',
    'gewaesser': '',
    'area': 1,
    'gemark': '',
    'flur': '',
    'flurst': '',
    'bescheid': '',
    'befr_zeitr': datetime.datetime(2000, 1, 1),
    'bereg_flaech': 0,
    'pegel': '',
    'einstellwert': 0,
    'results_GWN': {'GWR': 0, 'Area': 0, 'RG1': 0, 'RG2': 0,
                    'RH': 0, 'RD': 0, 'checks': 0},
    'bilance_text': 'Bilanz noch nicht berechnet',
    'path_statement' : 'c:\\'
    }
    return userDict


class Start_Dialog_RO_main(QDialog, Ui_Dialog_RO_main):
    '''class for the main dialog of the reasoned opinion builder'''

    #signalExtrRead = pyqtSignal()


    def __init__(self, parent=None):
        QDialog.__init__(self, parent.mainWindow())
        self.setupUi(self)
        self.iface = parent
        self.pushButton_01.clicked.connect(self.newUser)
        self.pushButton_02.clicked.connect(self.permanentUser)
        self.pushButton_03.clicked.connect(self.temporaryUser)

        self.nv = None
        self.db_rw = None


    def closePlugin(self):
        self.close()


    def permanentUser(self):
        '''select an user from the WELL layer,
        load it's data and open the dialog of the reasoned opinion'''
        # select from layer
        self.setDB()
        qgis_toolbox.load_layer(SETUP.TGLAYER, set_active=False)
        layerT = QgsMapLayerRegistry.instance().mapLayersByName(SETUP.TGLAYER)[0]
        layerW = self.iface.activeLayer()
        if not layerW:
            message = u"Bitte Nutzer von gültigem Layer selektieren."
            self.iface.messageBar().pushMessage(message,
                                                level=QgsMessageBar.INFO,
                                                duration=7)
            return
        selectedFeatures = layerW.selectedFeatures()
        errorFlag = False

        ### ensure selection is done
        if not (layerW.name() == SETUP.DB_CONF['wnv_lhw']['table'] \
                or layerW.name() == SETUP.DB_CONF['wnv_land']['table']):
            message = u"Bitte Nutzer von gültigem Layer selektieren."
            errorFlag = True
        elif len(selectedFeatures) > 1:
            num = str(len(selectedFeatures))
            message = num +  "x Nutzer selektiert. Nur eine Selektion zulaessig."
            errorFlag = True
        elif len(selectedFeatures) == 0:
            message =  "0 Nutzer selektiert. Bitte eine Selektion vornehmen."
            errorFlag = True

        ### print warning if necessary (selection available?)
        if errorFlag:
            self.iface.messageBar().pushMessage(message,
                                                level=QgsMessageBar.INFO,
                                                duration=7)
        else:
            ### read user specifics from well layer
            userDict = qgis_toolbox.read_selectedUser(layerW, selectedFeatures,
                                                      createUserContainer())
            userDict['fromLayer'] = layerW.name()
            
            #### set currentStatus to internal coding accordingly
            if userDict['currentStatusT'] == "in Bearbeitung":
                userDict['currentStatus'] = 0
            elif userDict['currentStatusT'] == "abgelehnt":
                userDict['currentStatus'] = 1
            else:
                userDict['currentStatus'] = 2

            if layerW.name() == SETUP.DB_CONF['wnv_lhw']['table']:           
                try:
                    ### id of user in wnv_lhw
                    idx_id = layerW.fieldNameIndex(SETUP.DB_CONF['wnv_lhw']['id'])
                    if isinstance(selectedFeatures, list):
                        selectedFeatures = selectedFeatures[0]
                    curr_id = selectedFeatures.attributes()[idx_id]
                    
                    ### request feature with id in ezg
                    layer_ezg = QgsMapLayerRegistry.instance().mapLayersByName(SETUP.DB_CONF['ezg']['table'])[0]
                    req = '"{}" = {}'.format(SETUP.DB_CONF['ezg']['id'], curr_id)
                    request = QgsFeatureRequest().setFilterExpression(req)
                    feat_ezg_l = layer_ezg.getFeatures(request)
                    
                    feat_ezg = list(feat_ezg_l)[0]

                    for key in ('DAY_M3', 'days'):
                        idx_d = layer_ezg.fieldNameIndex(SETUP.DB_CONF['ezg'][key])
                        userDict[key] = feat_ezg.attributes()[idx_d]

                except Exception as e:
                    message =  "Auswahl ohne Auswertung des EZG-Layers. Ist dieser geladen?"
                    self.iface.messageBar().pushMessage(message,
                                                level=QgsMessageBar.INFO,
                                                duration=7)
            
            #print(userDict['BIL_JAHR_TM3'])
            userDict = self.getNewUserQL(userDict,
                                         layerT,
                                         selectedFeatures)
            #print('perm user dict  {}'.format(userDict))
            self.nv = Start_Dialog_RO_userManagement(self.iface,
                                                     userDict,
                                                     userDict,
                                                     1,
                                                     self.db_rw)
            #self.signalExtrRead.emit()
            self.nv.show()


    def temporaryUser(self):
        '''select an working process snatshot for an unfinished reasoned opinion
        load it's data and open the dialog of the reasoned opinion'''
        # select folder
        self.setDB()
        fnameC = QFileDialog.getExistingDirectory(self,
                                                  'Temporaeren Nutzer laden',
                                                  SETUP.TEMPFOLDER)
        if len(fnameC) == 0:
            return
        userDict = qgis_toolbox.load_obj_compressed(os.path.join(fnameC,
                                                                 'project'))
        userDict['tempPath'] = fnameC
        #print('loaded  {}'.format(userDict))
        self.nv = Start_Dialog_RO_userManagement(self.iface,
                                                 userDict,
                                                 userDict,
                                                 2,
                                                 self.db_rw)
        try:
            qgis_toolbox.load_layer('BrunnenEGZ', 
                                    set_active=False, 
                                    folderx=fnameC, 
                                    altName=userDict['wellCatchment'])
        except:
            message = 'Konnte Brunneneinzugsgebiet nicht laden.'
            self.iface.messageBar().pushMessage(message,
                                                level=QgsMessageBar.INFO,
                                                duration=3)
        try:
            qgis_toolbox.load_layer('Bilanzgebiet', 
                                    set_active=False, 
                                    folderx=fnameC, 
                                    altName=userDict['balanceCatchment'])
        except:
            message = 'Konnte Bilanzierungsgebiet nicht laden.'
            self.iface.messageBar().pushMessage(message,
                                                level=QgsMessageBar.INFO,
                                                duration=3)
            
        self.nv.show()


    def newUser(self):
        '''create an new user'''
        self.setDB()
        self.nv = Start_Dialog_RO_userManagement(self.iface,
                                                 createUserContainer(),
                                                 createUserContainer(),
                                                 0,
                                                 self.db_rw)
        self.nv.show()


    def getNewUserQL(self, userDict, tgLayer, selectedFeatures):
        '''read the QL value for an user from the WELL layer

        Parameters
        ----------
        userDict : dict
            the user dictionary
        tgLayer : layer
            sub catchment layer
        selectedFeatures : selectedFeatures object
            the selected well

        Returns
        -------
        updated user dictionary : dict'''
        request = QgsFeatureRequest().setFilterExpression(u'"TGID" = ' + str(userDict['TG']))
        itL = tgLayer.getFeatures(request)
        idx_ql = tgLayer.fieldNameIndex("QL_SPENDE")
        idx_qleff = tgLayer.fieldNameIndex("QL_EFF_SPE")
        for it in itL:
            userDict['QL'] = it.attributes()[idx_ql]
            userDict['QL_eff'] = it.attributes()[idx_qleff]
            break
        return userDict


    def setDB(self):
        self.db_rw = db_toolbox.userDB('rw')
        self.db_rw.table_to_layer(SETUP.DB_CONF['ist_mengen']['table'],
                                  SETUP.DB_CONF['ist_mengen']['table'])
        self.db_rw.table_to_layer(SETUP.DB_CONF['ezg']['table'],
                                  SETUP.DB_CONF['ezg']['table'])
        self.db_rw.table_to_layer(SETUP.DB_CONF['wnv_land']['table'],
                                  SETUP.DB_CONF['wnv_land']['table'])
        self.db_rw.table_to_layer(SETUP.DB_CONF['wnv_lhw']['table'],
                                  SETUP.DB_CONF['wnv_lhw']['table'])



if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = Start_Dialog_RO_main(None)
    w.show()
    sys.exit(app.exec_())
