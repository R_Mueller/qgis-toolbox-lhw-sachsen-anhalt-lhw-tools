# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Dialog for user handling and reasoned statements.

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""

import os
import datetime
from qgis.gui import QgsMessageBar
from PyQt4.QtGui import QMessageBox
from PyQt4.QtCore import pyqtSignal
from PyQt4.QtGui import QButtonGroup
from PyQt4.QtGui import QDialog
from qgis.core import QgsGeometry
from qgis.core import QgsRectangle
from qgis.core import QgsPoint
from qgis.core import QgsVectorLayer
from qgis.core import QgsMapLayerRegistry
from PyQt4.QtCore import QSettings
from PyQt4.QtGui import QFileDialog
from qgis.core import NULL
from qgis.core import QgsDistanceArea
from qgis.core import QgsFeatureRequest

from .tools import qgis_toolbox
from .tools.iterate_well2 import Iteration
from .tools.iterate_well2 import make_catchmentLayer
from .tools.iterate_well2 import draw_catchment
from .tools.iterate_well2 import first_estimate
from .tools.iterate_well2 import get_idxDictTgEfl
from .tools.iterate_well2 import get_idxDictWell
from .tools.iterate_well2 import get_QL
from .tools.iterate_well2 import create_wellPointLayer
from .tools.create_html import create_html
from .setup import setup__ as SETUP
#from .tools.qgis_toolbox import modify_attrDict_featList

from pyqtDialogs.dialog_RO_userManagement import Ui_Dialog_RO_userManagement

from ro_components import Start_Ui_Dialog_RO_components
from ro_balance import Start_Dialog_RO_balance
#from ro_reasonedOpinion import Start_Dialog_RO_reasonedOpinion
from ro_defineEndpoint import Start_Dialog_RO_defineEndpoint
from ro_newBalance import Start_Dialog_RO_newBalance
from ro_additionalData import Start_Dialog_RO_additionalData
from ro_getCoordinates import Start_Dialog_RO_getCoordinates
from ro_newReference import Start_Dialog_RO_newReference

class Start_Dialog_RO_userManagement(QDialog, Ui_Dialog_RO_userManagement):
    '''class for the dialog of the user management'''

    ### class attribute
   # signalExtrRead = pyqtSignal(float)
    signalGwn = pyqtSignal(list)
    signalAcc = pyqtSignal(list)
    signalAddBal = pyqtSignal(list)
    signalIter = pyqtSignal(bool)
    signalCoord = pyqtSignal(list)
    signalCoordEP = pyqtSignal(list)
    signalQL = pyqtSignal()

    def __init__(self, parent=None, userDict=None,
                 userDictOld=None, mode=0, db_rw=None):
        '''parent... Qt Class, userDict... dict, userDictOld... dict,
        mode... int (0 new well, 1 temporary, 2 selection)'''
        QDialog.__init__(self, parent.mainWindow())
        self.setupUi(self)
        self.iface = parent
        self.userDictOld = userDictOld
        self.userDict = userDict
        if mode == 2:
            ### for older saves assume new user...
            if not 'mode' in self.userDict:
                self.userDict.update({'mode': 0})
            self.mode = self.userDict['mode']
        else:
            self.mode = mode
            self.userDict['mode'] = mode
        self.db_rw = db_rw
        self.ButtonGroupState = QButtonGroup(parent)
        self.ButtonGroupState.addButton(self.radioButtonP_2, 0)
        self.ButtonGroupState.addButton(self.radioButtonR, 1)
        self.ButtonGroupState.addButton(self.radioButtonA, 2)
        self.wellBorigin = 0

        ### classes
        self.gwrComponents = Start_Ui_Dialog_RO_components(parent)
        self.accounting = Start_Dialog_RO_balance(parent,
                                                  self.userDict)
        #self.report = Start_Dialog_RO_reasonedOpinion(parent)
        self.addBalance = Start_Dialog_RO_newBalance(parent)
        self.addData = Start_Dialog_RO_additionalData(parent,
                                                      self.userDict,
                                                      self.userDictOld)
        self.getC = Start_Dialog_RO_getCoordinates(parent)
        self.staticC = Start_Dialog_RO_defineEndpoint(self,
                                                      self.getC)
        self.newRef = Start_Dialog_RO_newReference(parent,
                                                   self.userDict)

        ### pushButtons
        self.pushButtonAddBal_2.clicked.connect(self.select_addBalance)
        self.pushButtonGWRComp.clicked.connect(self.call_gwrComponents)
        self.pushButtonCalcCatchVar.clicked.connect(self.calculate_WellCatchmentV)
        self.pushButtonCalcCatchStat.clicked.connect(self.calculate_WellCatchmentS)
        self.pushButtonSaveBalCatch_2.clicked.connect(self.get_activeBalanceLayer)
        self.pushButtonSaveBalCatch.clicked.connect(self.get_shapeBalanceLayer)
        self.pushButtonSaveWellCatch.clicked.connect(self.get_activeShapeWellLayer)
        self.pushButtonBalance.clicked.connect(self.doAccounting)
        self.pushButtonReject_3.clicked.connect(self.close)
        self.pushButtonSaveTemp_3.clicked.connect(self.saveTemporary)
        self.pushButtonSave_3.clicked.connect(self.saveEvent)
        self.pushButtonReport.clicked.connect(self.call_report)
        self.pushButtonAddData.clicked.connect(self.additionalData)
        self.pushButtonCoordinates.clicked.connect(self.getCoordinates)
        self.pushButtonZoom.clicked.connect(self.zoomToCoordinates)
        self.pushButtonRecalcGwn.clicked.connect(self.recalcGWN)
        self.pushButton_save.clicked.connect(self.save_map)
        ### radioButton
        self.ButtonGroupState.buttonClicked.connect(self.groupState_toggled)

        ### signals
        self.gwrComponents.signalGwn.connect(self.signal_gwrComponents)
        self.addBalance.signalAddBal.connect(self.signal_addBal)
        self.getC.signalCoord.connect(self.signal_Coord)
        self.getC.signalCoordEP.connect(self.signal_CoordEP)
        self.getC.signalQL.connect(self.new_QL)
        self.staticC.signalIter.connect(self.start_WellCatchmentS)
        
        ### functions in __init__
        self.populateInterface()
        
        self.lineEditName.textChanged[str].connect(lambda x: self.signal_defUser(x,
                                                   'userName'))
        self.lineEditNumber.textChanged[str].connect(lambda x: self.signal_defUser(x,
                                                     'applicationNumber'))
        self.lineEditX.textChanged[str].connect(lambda x: self.signal_defUser(x,
                                                'coordinateX'))
        self.lineEditY.textChanged[str].connect(lambda x: self.signal_defUser(x,
                                                'coordinateY'))
        self.lineEditDate.textChanged[str].connect(lambda x: self.signal_defUser(x,
                                                'date_sn'))
        self.doubleSpinBox_qday.valueChanged[float].connect(self.new_extraction_day)
        self.doubleSpinBox_qyear.valueChanged[float].connect(self.new_extraction_year)
        self.spinBox_days.valueChanged[int].connect(self.new_extraction_year)
        ### set signal for QL update only for new user mode
        #if self.mode is None:
        self.lineEditX.editingFinished.connect(self.new_QL)
        self.lineEditY.editingFinished.connect(self.new_QL)

        

        tempLayer = self.iface.activeLayer()
        self.crsID = SETUP.CRS


        if self.mode == 0:
            try:
                tempLayer = qgis_toolbox.getLayerHandle(SETUP.DB_CONF['wnv_lhw']['table'],
                                                        set_active=False,
                                                        folderx=None,
                                                        altName=None)
                idx = tempLayer.fieldNameIndex('ID')
                self.userDict['ID'] = int(tempLayer.maximumValue(idx)) + 1
            except:
                self.iface.messageBar().pushMessage(u'{} nicht gefunden.'.format(SETUP.DB_CONF['wnv_lhw']['table']),
                                                    level=QgsMessageBar.WARNING,
                                                    duration=7)

    ###########################################################################
    ### PUSH BUTTONS
    ###########################################################################
    def save_map(self):
        if self.userDict['path_statement'] == 'c:\\':
            self.iface.messageBar().pushMessage(u'Bitte erst Stellungnahme erstellen.',
                                                    level=QgsMessageBar.WARNING,
                                                    duration=7)
            return
        mapx = os.path.join(os.path.dirname(self.userDict['path_statement']), 'Karte.png')
        #print(mapx)
        self.iface.mapCanvas().saveAsImage(mapx, None, "PNG")
    
    
    def recalcGWN(self):
        '''recalculate the groundwater recharge for changed balance catchments'''
        layerNameWell = self.userDict['balanceCatchment']
        layerNameWHH = SETUP.EFLLAYER
        bal = self.accounting.calculateBalance(layerNameWell, layerNameWHH,
                                               self.userDict['gwRechargeComponents'])
        
        layerWell = qgis_toolbox.getLayerHandle(layerNameWell)
        #request = QgsFeatureRequest().setFilterFid(1)
        #feat = next(layerWell.getFeatures(request))
        #for feat in  catch_layerFeat:
        
        id_gwn = layerWell.fieldNameIndex(u"GWN (mm)")
        if id_gwn < 0:
            self.iface.messageBar().pushMessage(u'Bitte erst Feld "GWN (mm)" hinzufügen.',
                                                    level=QgsMessageBar.WARNING,
                                                    duration=7)
            return
        else:
            layerWell.startEditing()
            idx = layerWell.fieldNameIndex(u"GWN (mm)")
            for feature in layerWell.getFeatures():
                print(feature.id())
                area = feature.geometry().area()
                gwn_new = bal['GWR']/area*1000000
                layerWell.changeAttributeValue(feature.id(), idx, gwn_new)
                
            layerWell.commitChanges()
            #modify_attrDict_featList(layerWell, [1], {"GWN (mm)": bal['GWR']})
            self.iface.messageBar().pushMessage(u'GWN mit {} berechnet.'.format(gwn_new),
                                                    level=QgsMessageBar.INFO,
                                                    duration=7)


    def getCoordinates(self):
        '''show the dialog for the additional balance components'''
        self.getC.set_function(0)
        self.getC.show()


    def zoomToCoordinates(self):
        rect = QgsRectangle(float(self.userDict['coordinateX'])-SETUP.SCALE,
                            float(self.userDict['coordinateY'])-SETUP.SCALE,
                            float(self.userDict['coordinateX'])+SETUP.SCALE,
                            float(self.userDict['coordinateY'])+SETUP.SCALE)
        self.iface.mapCanvas().setExtent(rect)
        #pt = QgsPoint(float(x),float(y))
        self.iface.mapCanvas().refresh()


    def select_addBalance(self):
        ''''show the dialog for the additional balance components'''
        self.addBalance.show()


    def call_report(self):
        '''show the dialog for the reasoned opinion'''
        try:
            path_name = QFileDialog.getExistingDirectory(None,
                                                   'Ausgabeverzeichnis',
                                                   SETUP.TEMPFOLDER)
            if not path_name:
                self.iface.messageBar().pushMessage(u'Speichern abgebrochen',
                                                level=QgsMessageBar.INFO,
                                                duration=7)
            fileAndPath = os.path.join(path_name.encode('utf-8'), u'Stellungnahme.html')
            self.userDict['path_statement'] = fileAndPath
            #print(fileAndPath)
            if not self.userDict['path_statement'] == 'c:\\':
                create_html(self.userDict)
                self.iface.messageBar().pushMessage(u'Im Ornder erstellt. Karte.png und Legende.png nicht vergessen!',
                                                    level=QgsMessageBar.INFO,
                                                    duration=7)
        except Exception as e:
            self.iface.messageBar().pushMessage(u'Fehlgeschlagen!. Bilanzierung erstellt? Fehler war: {}.'.format(e),
                                                level=QgsMessageBar.WARNING,
                                                duration=7)
            


    def new_QL(self):
        '''given the x and y values of the well, look for enclosing TG
        and look-up the QL value'''
        ### generate geometry
        try:
            x = float(self.lineEditX.text())
            y = float(self.lineEditY.text())
        except:
            return
        tgLayer = qgis_toolbox.getLayerHandle(SETUP.TGLAYER)
        buffer_Geometrie = QgsGeometry.fromPoint(QgsPoint(x, y))
        ql =  None
        qlEff = None
        ### check for intersection
        for tgFeat in tgLayer.getFeatures():
            if buffer_Geometrie.intersects(tgFeat.geometry()):
                attrs = tgFeat.attributes()
                id_QL = tgLayer.fieldNameIndex("QL_SPENDE")
                id_QLeff = tgLayer.fieldNameIndex("QL_EFF_SPE")
                ql = attrs[id_QL]
                qlEff = attrs[id_QLeff]
                ql = ql #* 365 * 86400 / 1000000
                qlEff = qlEff #* 365 * 86400 / 1000000
                break
        ### read QL values
        if not (ql is None or ql == NULL):
            self.lineEditQL.setText(str(ql))
            self.userDict['QL'] = ql


    def doAccounting(self):
        '''show the accounting dialog'''
        self.accounting.comboBox.clear()
        self.accounting.comboBox_2.clear()
        self.accounting.userDict = self.userDict
        if not self.userDict['balanceCatchment'] == '(nicht gesetzt)':
            self.accounting.comboBox.addItem(self.userDict['balanceCatchment'])
        if not self.userDict['wellCatchment'] == '(nicht gesetzt)':
            self.accounting.comboBox.addItem(self.userDict['wellCatchment'])
            self.accounting.comboBox_2.clear()
        for layer in SETUP.WHHLAYER1:
            self.accounting.comboBox_2.addItem(layer)
        self.accounting.show()


    def get_activeBalanceLayer(self):
        '''well catchment: update the label_wellB and with
        the active layer and set userDict'''
        layer = self.iface.activeLayer()
        if not layer is None:
            self.userDict['balanceCatchment'] = layer.name()
            self.label_wellBset.setText(layer.name())
            self.wellBorigin = 1


    def get_activeShapeWellLayer(self):
        '''well catchment: update the label_wellC and with
        the active layer and set userDict'''
        layer = self.iface.activeLayer()
        if not layer is None:
            self.userDict['wellCatchment'] = layer.name()
            self.label_wellCset.setText(layer.name())


    def get_shapeBalanceLayer(self):
        '''well catchment: update the label_wellB
        with an shape and set userDict.
        Shows the standard openFile dialog to
        get the name of the shape'''
        ### load and prepare names
        fnameC = QFileDialog.getOpenFileName(self, 'Bilangebiet von Shape laden',
                                             SETUP.TEMPFOLDER, "Shape Dateien (*.shp)")
        fname = os.path.basename(fnameC)
        fname = fname[:fname.find('.')]
        ### load shape
        poly_efl = QgsVectorLayer(fnameC, fname, 'ogr')
        QgsMapLayerRegistry.instance().addMapLayer(poly_efl)
        ### set stuff
        self.label_wellBset.setText(fname)
        self.userDict['balanceCatchment'] = fname
        self.userDict['balanceCatchmentOrig'] = fnameC
        self.wellBorigin = 0


    def saveTemporary(self):
        '''save all settings in an compressed prickle file'''
        filename =  "".join([x if x.isalnum() else "_" for x in self.userDict['applicationNumber']])
        fileAndPath = os.path.join(SETUP.TEMPFOLDER, filename)
        if not os.path.exists(fileAndPath):
            os.mkdir(fileAndPath)
        self.userDict['tempPath'] = fileAndPath
        ### save userDict
        qgis_toolbox.save_obj_compressed(os.path.join(fileAndPath, "project"),
                                         self.userDict)

        ### save layers
        qgis_toolbox.write_loaded_layer(self.userDict['wellCatchment'],
                                        fileAndPath ,
                                        'BrunnenEGZ.shp')
        qgis_toolbox.write_loaded_layer(self.userDict['balanceCatchment'],
                                        fileAndPath ,
                                        'Bilanzgebiet.shp')


    def saveToDB(self):
        ### MODES##########
        # 0 --> created new user
        # 1 --> used existing user
        ###################

        #if self.mode == 1:
        #    cuIDuserO = self.userDict['ID']

        ### test extraction rate
        try:
            float(self.userDict['JAHR_TM3'])
        except:
            fmtstr = u'Speichern abgebrochen. Keine gültige Gesamtentnahme "{}"!'
            self.iface.messageBar().pushMessage(fmtstr.format((self.userDict['JAHR_TM3']),
                                            level=QgsMessageBar.WARNING, duration=7))
            return

        ### test X
        try:
            float(self.userDict['coordinateX'])
        except:
            fmtstr = u'Speichern abgebrochen. Keine gültiger Rechtwert "{}"!'
            self.iface.messageBar().pushMessage(fmtstr.format((self.userDict['coordinateX']),
                                            level=QgsMessageBar.WARNING, duration=7))
            return

        ### test Y
        try:
            float(self.userDict['coordinateY'])
        except:
            fmtstr = u'Speichern abgebrochen. Keine gültiger Hochwert "{}"!'
            self.iface.messageBar().pushMessage(fmtstr.format((self.userDict['coordinateY']),
                                            level=QgsMessageBar.WARNING, duration=7))
            return

        ### test geometry  --> require a catchment for new user
        feature = None
        #print('SAVING...', self.userDict['fromLayer'], SETUP.DB_CONF['wnv_land']['table'])
        polyWKT = None
        try:
            feature = qgis_toolbox.get_featuresReg(self.userDict['wellCatchment'],
                                            get_id=0)
            geometry = feature.geometry()
            polyWKT = geometry.exportToWkt()
            #print('Found geometry')
        except:
            if not self.mode == 1 \
            or self.userDict['fromLayer'] == SETUP.DB_CONF['wnv_land']['table']:
                fmtstr = u'Speichern abgebrochen. Kein Bilanzgebiet vorgegeben oder Bilanzgebiet {} ohne Geometrie!'
                self.iface.messageBar().pushMessage(fmtstr.format((self.userDict['wellCatchment']),
                                                level=QgsMessageBar.WARNING, duration=7))
                return
            else:
                pass

        #print('geometry is {}'.format(polyWKT))
        ### if we have a catchment get GWN in case we added it from an active layer
        ###
        if not 'GWR flex' in self.userDict:
            self.userDict.update({'GWR flex': 0})

        try:
            if not feature is None and self.userDict['GWR flex'] == 0:
                layer = QgsMapLayerRegistry.instance().mapLayersByName(self.userDict['wellCatchment'])[0]
                idx_gwn = layer.fieldNameIndex( "GWN (mm)" )
                if idx_gwn == -1:
                    raise KeyError()
                gwn_tqa = feature.attributes()[idx_gwn]
                self.userDict['GWR flex'] = gwn_tqa
        except KeyError:
            if self.userDict['GWR total'] <= 0:
                fmtstr = u'''Zugewiesener Layer besitzt kein Feld <GNW (mm)> und Bilanzierung wurde NICHT durchgeführt.
                Eines von beiden erforderlich. Speichere NICHT!'''
                self.iface.messageBar().pushMessage(fmtstr,
                                                    level=QgsMessageBar.WARNING, duration=7)
            else:
                fmtstr = u'''Zugewiesener Layer besitzt kein Feld <GNW (mm)>.
                GWN nach Bilanzierung ist {}. Mit OK diesen Wert übernehmen, oder mit
                Abbrechen OHNE Speichern zurückkehren.'''.format(self.userDict['GWR total'])
                self.iface.messageBar().pushMessage(fmtstr,
                                                    level=QgsMessageBar.WARNING, duration=7)
            return

        ###############
        ### CHECK FOR PROXIMITY
        ###############
        inDict = {'x': self.userDict['coordinateX'],
                  'y': self.userDict['coordinateY'],
                  'table': SETUP.DB_CONF['wnv_lhw']['table']}

        id_prox_lhw = self.db_rw.check_for_coordinates(inDict, close=False)

        if isinstance(id_prox_lhw, (list, tuple)):
                id_prox_lhwList = id_prox_lhw
                id_prox_lhw = max(id_prox_lhw)
        else:
                id_prox_lhwList = id_prox_lhw
                id_prox_lhw = id_prox_lhw

        #print('id_prox_lhwList', id_prox_lhwList)

        inDict = {'x': self.userDict['coordinateX'],
                  'y': self.userDict['coordinateY'],
                  'table': SETUP.DB_CONF['wnv_land']['table']}

        id_prox_land = self.db_rw.check_for_coordinates(inDict, close=False)

        if isinstance(id_prox_land, (list, tuple)):
                id_prox_landList = id_prox_land
                id_prox_land = max(id_prox_land)
        else:
                id_prox_landList = id_prox_land
                id_prox_land = id_prox_land

        #print('id_prox_landList', id_prox_landList)

        ###############
        ### check if az is already in DB
        ###############
        inDict = {'az': self.userDict['applicationNumber'],
                  'table': SETUP.DB_CONF['wnv_lhw']['table']}
        #id_az_lhw =  self.db_rw.check_for_az(inDict, close=False)

        inDict = {'az': self.userDict['applicationNumber'],
                  'table': SETUP.DB_CONF['wnv_land']['table']}
        #id_az_land =  self.db_rw.check_for_az(inDict, close=False)

        ###############
        ### check for the id
        ###############
        inDict = {'id': self.userDict['ID'],
                  'table': SETUP.DB_CONF['wnv_lhw']['table']}
        #id_in_lhw = self.db_rw.check_for_id(inDict, close=False)


        ###############
        ### how to proceed logic
        ###############

        ### last ID in wnv_lhw
        inDict = {'table': SETUP.DB_CONF['wnv_lhw']['table']}
        cuIDuser = self.db_rw.get_last_id(inDict) + 1

        proceed = True
        ### NEW
        if self.mode == 0:
            #print('mode', self.mode)
            ### is it in LAND?
            if id_prox_land:
                    #print('id_prox_land --> TRUE', id_prox_land)
                    ### is in LAND?
                    if id_prox_lhw:
                        #print('id_prox_lhw --> TRUE', id_prox_lhw)
                        ### is in LAND and LHW --> ASK for new AZ
                        fmtstr = u'''Neuer Nutzer anlegen:
Brunnen {} in nächster Nähe bereits vorhanden (wnv_lhw und/oder wnv_land).
Eintrag in wnv_lhw mit neuem/bestätigtem Aktenzeichen anlegen?'''
                        proceed = self.otherEvent(fmtstr.format([id_prox_lhwList,
                                                                id_prox_landList]
                                                                ))
                        if proceed:
                            ### new entry with new AZ
                            self.newRef.setLabel()
                            self.newRef.exec_()
                            make_new = True
                        else:
                            ### do nothing
                            proceed = False
                    ### is in LAND but NOT in LHW --> new
                    else:
                        #print('id_prox_lhw --> FALSE', id_prox_lhw)
                        make_new = True
            ### is NOT in LAND!
            else:
                #print('id_prox_land --> FALSE', id_prox_land)
                ### if in LHW?
                if id_prox_lhw:
                    #print('id_prox_lhw --> TRUE', id_prox_lhw)
                    ### is NOT in LAND but in LHW: ASK for new AZ
                    fmtstr = u'''Neuer Nutzer anlegen:
Brunnen {} in nächster Nähe bereits in wnv_lhw vorhanden.
Eintrag in wnv_lhw mit neuem/bestätigtem Aktenzeichen anlegen?'''
                    proceed = self.otherEvent(fmtstr.format(id_prox_lhwList))
                    if proceed:
                        ### new entry with new AZ
                        self.newRef.setLabel()
                        self.newRef.exec_()
                        make_new = True
                    else:
                        ### do nothing
                        proceed = False
                ### NOT in LAND and NOT in LHW --> perfect case
                else:
                    #print('id_prox_lhw --> FALSE', id_prox_lhw)
                    make_new = True

        ### UPDATE
        else:
            #print('mode', self.mode)
            ### is it in LAND?
            if id_prox_land:
                #print('id_prox_land --> TRUE', id_prox_land)
                ### is in LHW?
                if id_prox_lhw:
                    fmtstr = u'''Bestehenden Nutzer in LHW_Land aktualisieren:
Ja, um bestehenden Nutzer zu aktualisieren
Nein, um neuen Nutzer anzulegen.'''
                    make_new = self.otherEvent(fmtstr.format(id_prox_lhwList))
                    #print(make_new)
                    if not make_new is None:
                        make_new = not(make_new)                        
                else:
                    #print('id_prox_lhw --> FALSE', id_prox_lhw)
                    ### is in LAND -- make new in LHW
                    make_new = True
            else:
                #print('id_prox_land --> FALSE', id_prox_land)
                if id_prox_lhw:
                    fmtstr = u'''Bestehenden Nutzer in LHW_Land aktualisieren:
Ja, um bestehenden Nutzer zu aktualisieren
Nein, um neuen Nutzer anzulegen.'''
                    make_new = self.otherEvent(fmtstr.format(id_prox_lhwList))
                    #print(make_new)
                    if not make_new is None:
                        make_new = not(make_new)
                else:
                    #print('id_prox_lhw --> FALSE', id_prox_lhw)
                    ### NOT in LHW and NOT in LHW but update --> raise ERROR!
                    self.iface.messageBar().pushMessage(u'FEHLER! Update gefordert, aber Nutzer nicht in wnv_lhw oder wnv_land gefunden.',
                                                level=QgsMessageBar.WARNING,
                                                duration=7)
                    return
                

        ### check for exit flag
        if not proceed or make_new is None:
            #################
            ### Gandalf says: "You shall not pass!!1!"
            #################
            self.iface.messageBar().pushMessage(u'Speichern in DB durch Nutzer abgebrochen.',
                                                level=QgsMessageBar.INFO,
                                                duration=7)
            return

        
        ### if we make a new entry, we need a new id
        if make_new:
            cuIDuser = self.db_rw.get_last_id(inDict) + 1
        else:
            cuIDuser = id_prox_lhw

        #################
        ### create new entry ELSE update geometries only
        #################

        printSQL = False # True
        #print('makenew', make_new)
        if make_new:
            if not geometry:
                self.iface.messageBar().pushMessage(u'Kein Shape in EZG gesetzt. Speichere nicht.',
                                                level=QgsMessageBar.INFO,
                                                duration=7)
                return
            ### INSERT USER
            inDict = {'x' : self.userDict['coordinateX'],
                      'y': self.userDict['coordinateY'],
                      'table': SETUP.DB_CONF['wnv_lhw']['table']}
            ### set idp so we do not update it again
            cuIDuser = self.db_rw.insert_wellpoint(inDict,
                                                   commit=True,
                                                   idp=cuIDuser)

            ### INSERT EZG
            self.userDict['area'] = geometry.area()
            inDict = {'id_wnv_lhw' : cuIDuser,
                      'id': 'id_wnv_lhw',
                      'table': SETUP.DB_CONF['ezg']['table'],
                      'polgonWKT': polyWKT}
            self.db_rw.insert_catchment(inDict, commit=True)

            ### INSERT EXTR
            inDict = {'id_wnv_lhw' : cuIDuser,
                      'id': 'id_wnv_lhw',
                      'table': SETUP.DB_CONF['ist_mengen']['table']}
            self.db_rw.insert_extrpoint(inDict, commit=True)

        else:
            #print('updating WELL')
            inDict = {'x' : self.userDict['coordinateX'],
                      'y': self.userDict['coordinateY'],
                      'cid' : cuIDuser,
                      'table': SETUP.DB_CONF['wnv_lhw']['table'],
                      'idx': SETUP.DB_CONF['wnv_lhw']['id']}
            self.db_rw.update_wellpoint(inDict, print_sql=printSQL,
                                        close=False, commit=True)

            ### in case of a catchment --> update
            #print(polyWKT)
            if polyWKT:
                inDict = {'cid' : cuIDuser,
                          'table': SETUP.DB_CONF['ezg']['table'],
                          'idx': SETUP.DB_CONF['ezg']['id']}
                #print('updating EZG')
                inDict.update({'polgonWKT': polyWKT})
                self.db_rw.update_catchment(inDict, print_sql=printSQL,
                                            close=False, commit=True)

        #################
        ### anyway, let's update all other fields
        #################
        ### UPDATE USER
        self.userDict['table'] = SETUP.DB_CONF['wnv_lhw']['table']
        self.userDict['cid'] = cuIDuser
        self.userDict['idx'] = SETUP.DB_CONF['wnv_lhw']['id']
        self.update('wnv_lhw', cuIDuser, print_sql=printSQL)

        ### UPDATE EZG
        if feature:
            d = QgsDistanceArea()
            area = d.measurePolygon(feature.geometry().asPolygon()[0])
            self.userDict['area'] = area
        self.update('ezg', cuIDuser, print_sql=printSQL)   
        ### UPDATE IST_MENGEN
        self.update('ist_mengen', cuIDuser, print_sql=printSQL)


    def update(self, tablename, cuIDuser, print_sql=False):
        '''update fields in an table'''
        if tablename == 'wnv_lhw':
            idx = 'id'
        else:
            idx = 'id_wnv_lhw'

        for key in SETUP.DB_CONF[tablename]['fields'].items():
            #print(key[0], self.userDict[key[1]])
            upDict  = {key[0]: self.userDict[key[1]],
                       'cid': cuIDuser,
                       'idx': idx, 'table': tablename}
            self.db_rw.update_single_entry(upDict,
                                           print_sql=print_sql,
                                           close=False,
                                           commit=True)


    def calculate_WellCatchmentV(self):
        '''compute the well catchment iteratively with free direction and length'''
        if self.userDict['JAHR_TM3'] == 0:
            message = u'Achtung! Jahresentnahme ist 0.00 Tm3/Jahr. Keine Berechnung erfolgt.'
            self.iface.messageBar().pushMessage(message,
                                                level=QgsMessageBar.WARNING,
                                                duration=7)
            return
        self.calculate_WellCatchment(False)


    def calculate_WellCatchmentS(self):
        '''comute the well catchment iteratively with fixed endpoint'''
        self.userDict['staticLCalc'] = False
        self.userDict['coordinateEndX'] = 0
        self.userDict['coordinateEndY'] = 0
        self.staticC.show()


    def start_WellCatchmentS(self):
        ### TEST ENDPOINT
        self.calculate_WellCatchment(True)
        #print(self.userDict['coordinateEndX'], self.userDict['coordinateEndY'])


    def calculate_WellCatchment(self, staticL):
        '''calculate the well catchment for new or updated users.

        Parameters
        ----------
        staticL : bool
            if staticL is true, keep the direction and length constant.
            for this, an layer with an endPoint has to be loaded.'''

        try:
            float(self.userDict['JAHR_TM3'])
        except:
            message = u'Achtung Entnahmerate nicht definiert!'
            self.iface.messageBar().pushMessage(message,
                                                level=QgsMessageBar.WARNING,
                                                duration=7)
            return

        if staticL and self.userDict['coordinateEndX'] == 0:
            message = u'Kein gütliger Endpunkt definiert.'
            self.iface.messageBar().pushMessage(message,
                                                level=QgsMessageBar.WARNING,
                                                duration=7)
            return

        ### Project CRS
        s = QSettings()
        # possible values are: prompt, useProject, useGlobal
        s.setValue("/Projections/defaultBehaviour", "useProject")

        ### check layers
        tgLayer = qgis_toolbox.getLayerHandle(SETUP.TGLAYER,
                                              set_active=False,
                                              folderx=None,
                                              altName=None)
        eflLayer = qgis_toolbox.getLayerHandle(SETUP.EFLLAYER,
                                               set_active=False,
                                               folderx=None,
                                               altName=None)

        ### layer features
        tgFeatures = qgis_toolbox.get_featuresReg(SETUP.TGLAYER)
        areaSAFeatures = qgis_toolbox.get_featuresReg(SETUP.AREALAYER)
        #eflFeatures = qgis_toolbox.get_featuresReg(SETUP.EFLLAYER)

        ### new well layer and dictionary
        wellLayer = create_wellPointLayer(self.userDict,
                                          self.crsID)
        QgsMapLayerRegistry.instance().addMapLayer(wellLayer)


        wellFeatures = wellLayer.getFeatures()
        idxDict = get_idxDictWell(wellLayer)
        ### dictionary with indices of fields in tgLayer

        idxDict = get_idxDictTgEfl(tgLayer, idxDict)
        idxDict = get_QL(tgLayer, idxDict)


        wellDict = {'coordinateX': self.userDict['coordinateX'],
                    'coordinateY': self.userDict['coordinateY'],
                    'ID': self.userDict['ID']}

        ### wellDict['Q'] is inclusive self.userDict['total_addBalance']
        ### after the first_estimate
        wellDict = first_estimate(areaSAFeatures,
                                  wellFeatures,
                                  tgFeatures,
                                  idxDict,
                                  wellDict,
                                  self.userDict['gwRechargeComponents'],
                                  self.userDict['total_addBalance'])
        if wellDict is None:
            return
        ### the estimation of the 'real' catchment
        idxDict = get_idxDictTgEfl(eflLayer,
                                   idxDict,
                                   'EFLID')

        results = Iteration(wellDict,
                            self.userDict['gwRechargeComponents'],
                            self.userDict,
                            staticL)
        results['GWR flex'] = results['GWR']
        self.userDict['GWR flex'] = results['GWR']

        if staticL:
            endpnt = [self.userDict['coordinateEndX'], self.userDict['coordinateEndY']]
        else:
            endpnt = [0, 0]

        wellLayerC = make_catchmentLayer(results,
                                         wellDict,
                                         self.userDict['lastWellLayerNr'],
                                         endpnt)



        self.userDict['wellCatchment'] = 'Brunneneinzugsgebiet' + str(self.userDict['lastWellLayerNr'])
        self.label_wellCset.setText(self.userDict['wellCatchment'].encode('utf8'))
        draw_catchment(wellLayerC)
        self.userDict['lastWellLayerNr'] += 1

        if self.mode == 0:
            self.userDict['wellPoint'] = 'neuer_Brunnen'


    def groupState_toggled(self, v):
        '''keep track which input type for extraction rates is used
        Parameters
        ----------
        v : radioGroup object
            get the name of the active radioButton'''
        buttonName = v.objectName()
        if buttonName == 'radioButtonP_2':
            self.userDict['currentStatus'] = 0
        elif buttonName == 'radioButtonR':
            self.userDict['currentStatus'] = 1
        else:
            self.userDict['currentStatus'] = 2
        ### currentStatus to text
        self.userDict['currentStatusT'] = ("in Bearbeitung",
                                           "abgelehnt",
                                           "angenommen")[self.userDict['currentStatus']]
        #print(self.userDict['currentStatusT'])


    ###########################################################################
    ### CALL DIALOGS
    ###########################################################################

    def additionalData(self):
        '''open the dialog for addition data'''
        self.addData.populate()
        self.addData.show()


    def call_gwrComponents(self):
        '''open the dialog for the ground water recharge components'''
        self.gwrComponents.set_interface(self.userDict['gwRechargeComponents'],
                                         self.userDict['gwRechargeComponents'])
        self.gwrComponents.show()


    ###########################################################################
    ### SIGNALS
    ###########################################################################

    ### GET signals
    def signal_addBal(self, value):
        '''signal to collect the additional balance components'''
        #print(value)
        self.userDict['names_addBalance'] = value[1]
        self.userDict['total_addBalance'] = value[0]
        self.userDict['list_addBalance'] = value[2]


    def new_extraction_day(self, tmp):
        self.userDict['DAY_M3'] = self.doubleSpinBox_qday.value()
        

    def new_extraction_year(self, tmp):
        self.userDict['BIL_JAHR_TM3'] = float(self.doubleSpinBox_qyear.value())
        self.userDict['days'] = int(self.spinBox_days.value())
        self.userDict['JAHR_TM3'] = self.userDict['BIL_JAHR_TM3'] \
        / self.userDict['days'] * 365
        self.addData.lineEdit_bil_tm3Jahr.setText(u'{:3.1f}'.format(self.userDict['BIL_JAHR_TM3']))
        self.label_tm3a_well.setText(u'{:3.2f}'.format(self.userDict['JAHR_TM3']))
        
        
    def signal_gwrComponents(self, value1):
        '''signal to collect the ground water recharge components'''
        self.userDict['gwRechargeComponents'] = value1


    def signal_Coord(self, point):
        '''signal to set coordinate from mouse event for WELLPOINT'''
        self.lineEditX.setText(str(point[0]))
        self.lineEditY.setText(str(point[1]))

    def signal_CoordEP(self, point):
        '''signal to set coordinate from mouse event for ENPOINT'''
        self.userDict['coordinateEndX'] = point[0]
        self.userDict['coordinateEndY'] = point[1]


    def signal_defUser(self, strInput, lEdit):
        '''update the userDict on an Input event for lineEdits

        Parameters
        ----------
        strInput : str
            str input from the lineEdits
        lEdit : str
            specifies the key for the userDict'''
        if lEdit in ('coordinateY', 'coordinateX'):
            if strInput:
                if strInput[-1] == '.' or strInput[-1] == ',':
                    strInput = strInput[:-1]
                if qgis_toolbox.checkForFloat(strInput):
                    strInput = float(strInput)
                else:
                    strInput = 0
                    self.iface.messageBar().pushMessage('Keine Koordinaten lesbar',
                                                        level=QgsMessageBar.INFO, duration=3)
            else:
                strInput = 0
        self.userDict[lEdit] = strInput


    ###  other stuff
    def closePlugin(self):
        self.close()


    def setUserDict(self, userDict):
        '''set the userDictionary'''
        self.userDict = userDict


    def setMode(self, value):
        '''set the user mode'''
        self.mode = value


    def populateInterface(self):
        '''update the dialog of RO_userManagement'''
        self.lineEditName.setText(self.userDictOld['userName'].encode(SETUP.ENCODING))
        self.lineEditNumber.setText(self.userDictOld['applicationNumber'].encode(SETUP.ENCODING))
        tmp = self.userDictOld['date_sn']
        if isinstance(tmp, datetime.datetime):
            tmp = datetime.datetime.strftime(tmp, '%Y-%m-%d')
        self.lineEditDate.setText(tmp.encode(SETUP.ENCODING))

        try:
            x = str(self.userDictOld['coordinateX'])
            y = str(self.userDictOld['coordinateY'])
        except:
            x = '0'
            y = '0'
            fmtstr = 'Keine Koordinaten aus Datenbank lesbar!'
            self.iface.messageBar().pushMessage(fmtstr,
                                                level=QgsMessageBar.WARNING, duration=5)
        self.lineEditY.setText(y)
        self.lineEditX.setText(x)

        self.label_wellBset.setText(self.userDictOld['wellCatchment'].encode(SETUP.ENCODING))
        self.label_wellCset.setText(self.userDictOld['balanceCatchment'].encode(SETUP.ENCODING))

        self.doubleSpinBox_qday.setValue(self.userDictOld['DAY_M3'])
        self.spinBox_days.setValue(self.userDictOld['days'])   
        #print('setting ', self.userDictOld['BIL_JAHR_TM3'], self.userDict['BIL_JAHR_TM3'])
        try:
            self.userDict['JAHR_TM3'] = float(str(self.userDictOld['BIL_JAHR_TM3'])) \
                                        / self.userDictOld['days'] * 365
            self.doubleSpinBox_qyear.setValue(float(self.userDictOld['BIL_JAHR_TM3']))
            self.label_tm3a_well.setText(u'{:3.2f}'.format(self.userDictOld['JAHR_TM3']))
        except:
            self.iface.messageBar().pushMessage('Achtung keine Entnahmeeintrage gefunden!',
                                                level=QgsMessageBar.INFO, duration=7)
        
        
        if self.userDictOld['currentStatus'] == 0:
            self.radioButtonP_2.setChecked(True)
        elif self.userDictOld['currentStatus'] == 1:
            self.radioButtonR.setChecked(True)
        else:
            self.radioButtonA.setChecked(True)

        self.lineEditQL.setText(str(self.userDictOld['QL']))

        self.addBalance.set_bal(nameOld=self.userDict['names_addBalance'],
                                balOld=self.userDict['list_addBalance'])

    def saveEvent(self):
        '''show messageBox for the event "save to data base"'''
        msg = "Das Projekt jetzt in DB speichern?"
        reply = QMessageBox.question(self, 'Speicherabfrage',
                                     msg,
                                     QMessageBox.Yes,
                                     QMessageBox.No)

        if reply == QMessageBox.Yes:
            self.saveToDB()
        else:
            return False

    def otherEvent(self, msg):
        '''show messageBox with random message'''
        tmp = None
        reply = QMessageBox.question(self, 'Speicherabfrage',
                                     msg,
                                     QMessageBox.Yes,
                                     QMessageBox.No,
                                     QMessageBox.Cancel)

        if reply == QMessageBox.Yes:
            tmp = True
        elif reply == QMessageBox.No:
            tmp = False
        elif reply == QMessageBox.Cancel:
            tmp = None
            
        return tmp
        
        def closeEvent(self, event):
             #print('exit')
             event.ignore()
            
        
    def closeEvent(self, event):
        quit_msg = u"Wollen Sie das Fenster wirklich schließen?\nAlle nicht gespeicherten Einstellungen gehen verloren."
        reply = QMessageBox.question(self, 'Message', 
                         quit_msg, QMessageBox.Yes, QMessageBox.No)
    
        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

