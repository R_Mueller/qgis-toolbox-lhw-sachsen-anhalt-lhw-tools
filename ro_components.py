# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Dialog to select the flows which together form the ground water recharge rate.

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""

import os
from PyQt4.QtGui import QDialog
from PyQt4.QtCore import pyqtSignal

from functools import partial

from pyqtDialogs.dialog_RO_components import Ui_Dialog_RO_components


pluginPath = os.path.dirname(__file__)                                              # liest den Pfad des Plugins aus, um ihn spaeter an das Logo zu uebergeben


class Start_Ui_Dialog_RO_components(QDialog, Ui_Dialog_RO_components):
    '''class for the dialog'''
    
    ### class attribute    
    signalGwn = pyqtSignal(list)

    def __init__(self, parent=None):
        ### non super inits
        QDialog.__init__(self, parent.mainWindow())
        self.setupUi(self)
        self.radioSettingsOld = []
        self.radioSettingsNew = []
        self.list_of_checkboxes = [self.checkBox_01, self.checkBox_02, self.checkBox_03, self.checkBox_04]

        # checkboxes: de/activation --> signalGwn
        slot01 = partial(self.enable_disable_mod01, self.checkBox_01)
        self.checkBox_01.stateChanged.connect(lambda x: slot01())
        slot02 = partial(self.enable_disable_mod02, self.checkBox_02)
        self.checkBox_02.stateChanged.connect(lambda x: slot02())
        slot03 = partial(self.enable_disable_mod03, self.checkBox_03)
        self.checkBox_03.stateChanged.connect(lambda x: slot03())
        slot04 = partial(self.enable_disable_mod04, self.checkBox_04)
        self.checkBox_04.stateChanged.connect(lambda x: slot04())

        self.pushButtonAccept.clicked.connect(self.acceptC)
        self.pushButtonReject.clicked.connect(self.rejectC)

    def closePlugin(self):
        self.close()

    ### SET attibutes of the class, connected to the checkBoxes
    def enable_disable_mod01(self, checkbox):
        checked = 0
        if checkbox.isChecked():
            checked = 1
        self.radioSettingsNew[0] = checked

    def enable_disable_mod02(self, checkbox):
        checked = 0
        if checkbox.isChecked():
            checked = 1
        self.radioSettingsNew[1] = checked

    def enable_disable_mod03(self, checkbox):
        checked = 0
        if checkbox.isChecked():
            checked = 1
        self.radioSettingsNew[2] = checked

    def enable_disable_mod04(self, checkbox):
        checked = 0
        if checkbox.isChecked():
            checked = 1
        self.radioSettingsNew[3] = checked

    
    def set_interface(self, valueOld=None, valueNew=None):
        '''set the state of checkBoxes when the dialog is opened
        
        Parameters
        ----------
        valueOld : list, optional
            the initial (old) state of the programm (bool for each checkBox)
        valueNew : list, optional
            the updated (changed) state of the programm (bool for each checkBox)'''    
        if not valueOld is None:
            self.radioSettingsOld = valueOld
        if not valueOld is None:
            self.radioSettingsNew = valueNew
        for i in range(4):
            self.list_of_checkboxes[i].setChecked(int(self.radioSettingsOld[i]))

    def acceptC(self):
        self.signalGwn.emit(self.radioSettingsNew)
        self.close()

    def rejectC(self):
        self.set_interface()
        self.close()
