# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Provides a template to generate a written statement.

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""

import os
import datetime
from .. setup import setup__ as SETUP
import traceback
import codecs

def create_html(userDict):
    try:
        def statement(sumx, a5):
            t1 = abs(sumx - a5 )
            print(sumx, a5)
            if sumx > a5:
               t2 = u'kleiner'
            else:
               t2 = u'höher' 
            tmp1 = u"""Die Summe der unterirdischen Abflusskomponenten ist um {0:.3f} [Tm3/Jahr] {1:s} als die Summe aller Entnahmen im betrachteten unterirdischen Einzugsgebiet.""".format(t1, t2)
            return tmp1
        
        try:
            #print(0)
            sumx = float(userDict['BIL_JAHR_TM3']) \
                       + float(userDict['total_addBalance']) \
                       + userDict['TM3_JAHR uIC']
            #print(0)
            userDict['area'] = userDict['results_GWN']['Area']
            userDict['area'] += 0.01
            a1 = userDict['results_GWN']['RG1']
            a1t = userDict['results_GWN']['RG1'] / userDict['area'] * 1000000
            a2 = userDict['results_GWN']['RG2']
            a2t = userDict['results_GWN']['RG2'] / userDict['area'] * 1000000
            a3 = userDict['results_GWN']['RD']
            a3t = userDict['results_GWN']['RD'] / userDict['area'] * 1000000
            a4 = userDict['results_GWN']['RH']
            a4t = userDict['results_GWN']['RH'] / userDict['area'] * 1000000
            a5 = a1 + a2 + a3 + a4
            a5t = a1t + a2t + a3t + a4t
            
            userDict['area'] += 0.01
            relation = [userDict['userName'], 
                        userDict['applicationNumber'], 
                        userDict['date_sn'],
                        userDict['bearb_lhw'],
                        userDict['coordinateX'],
                        userDict['coordinateY'],
                        userDict['area'] / 1000000,
                        userDict['BIL_JAHR_TM3'],
                        userDict['total_addBalance'],
                        sumx,
                        sumx / userDict['area'], # * 1000000,
                        a1,
                        a1t,
                        a2,
                        a2t,
                        a3,
                        a3t,
                        a4,
                        a4t,
                        a5,
                        a5t,
                        statement(sumx, a5),
                        userDict['coordinateEndX'],
                        userDict['coordinateEndY'],
                        userDict['QL'],
                        userDict['BIL_JAHR_TM3'],
                        userDict['DAY_M3'],
                        userDict['days'],
                        userDict['JAHR_TM3'],
                        userDict['b_qm_h'],
                        userDict['b_qm_d'],
                        userDict['b_mit_tqm_a'],
                        userDict['b_max_tqm_a'],
                        userDict['g_qm_h'],
                        userDict['g_qm_d'],
                        userDict['g_mit_tqm_a'],
                        userDict['g_max_tqm_a'],
                        userDict['date_sn'],
                        userDict['bearb_lhw']
                       ]
        except:
            
            sumx = float(userDict[b'BIL_JAHR_TM3']) \
                   + float(userDict[b'total_addBalance']) \
                   + userDict[b'TM3_JAHR uIC']
            userDict[b'area'] = userDict[b'results_GWN'][b'Area']
            userDict[b'area'] += 0.01
            a1 = userDict[b'results_GWN'][b'RG1']
            a1t = userDict[b'results_GWN'][b'RG1'] / userDict[b'area'] * 1000000
            a2 = userDict[b'results_GWN'][b'RG2']
            a2t = userDict[b'results_GWN'][b'RG2'] / userDict[b'area'] * 1000000
            a3 = userDict[b'results_GWN'][b'RD']
            a3t = userDict[b'results_GWN'][b'RD'] / userDict[b'area'] * 1000000
            a4 = userDict[b'results_GWN'][b'RH']
            a4t = userDict[b'results_GWN'][b'RH'] / userDict[b'area'] * 1000000
            a5 = a1 + a2 + a3 + a4
            a5t = a1t + a2t + a3t + a4t 
            relation = [userDict[b'userName'], 
                        userDict[b'applicationNumber'], 
                        userDict[b'date_sn'],
                        userDict[b'bearb_lhw'],
                        userDict[b'coordinateX'],
                        userDict[b'coordinateY'],
                        userDict[b'area'] / 1000000,
                        userDict[b'BIL_JAHR_TM3'],
                        userDict[b'total_addBalance'],
                        sumx,
                        sumx / userDict[b'area'], # * 1000000,
                        a1,
                        a1t,
                        a2,
                        a2t,
                        a3,
                        a3t,
                        a4,
                        a4t,
                        a5,
                        a5t,
                        statement(sumx, a5),
                        userDict[b'coordinateEndX'],
                        userDict[b'coordinateEndY'],
                        userDict[b'QL'],
                        userDict[b'BIL_JAHR_TM3'],
                        userDict[b'DAY_M3'],
                        userDict[b'days'],
                        userDict[b'JAHR_TM3'],
                        userDict[b'b_qm_h'],
                        userDict[b'b_qm_d'],
                        userDict[b'b_mit_tqm_a'],
                        userDict[b'b_max_tqm_a'],
                        userDict[b'g_qm_h'],
                        userDict[b'g_qm_d'],
                        userDict[b'g_mit_tqm_a'],
                        userDict[b'g_max_tqm_a'],
                        userDict[b'date_sn'],
                        userDict[b'bearb_lhw']
                       ]
        
        def counter():
            n = 0
            while True:
                yield n
                n += 1
        
      
        def check_encoding(part):
            part = repr(part)
            part = part[2:-1]
           # print(part)
            #part = part.replace(u'\xf6', '&ouml;')#[2:-1]
            #part = part.replace(u'\xe4', '&auml')#[2:-1]
            #part = part.replace(u'\xfc', '&uuml')#[2:-1]
           # part = part.replace(u'\xdf', '&szlig')#[2:-1]
            return part
        
        def check_encoding2(part):
            part = part.replace('\\xf6', '&ouml;')#[2:-1]
            part = part.replace('\\xe4', '&auml')#[2:-1]
            part = part.replace('\\xfc', '&uuml')#[2:-1]
            part = part.replace('\\xdf', '&szlig')#[2:-1]
            return part
        
        def replace_all_XXX(part, relation, cnter):
            while part.find('XXX') > -1: 
                cnt = next(cnter) 
                #print(relation[cnt], cnt)
                try:
                    if isinstance(relation[cnt], datetime.datetime):
                        #print(relation[cnt])
                        ins_text = datetime.datetime.strftime(relation[cnt], '%d.%m.%Y %H:%M')
                    elif isinstance(relation[cnt], str):
                        #print(relation[cnt])
                        ins_text = relation[cnt].strip()
                    elif isinstance(relation[cnt], float):
                        #print(relation[cnt])
                        ins_text = u'{0:.3f}'.format(relation[cnt])
                    else:
                        ins_text = str(relation[cnt])
                except:
                    #print(relation[cnt])
                    ins_text = check_encoding(relation[cnt])
                    #print(ins_text)
                part = part.replace('XXX', ins_text, 1)
                    
            return part
        
        
        def replace_etxr_in_ctch(relation):
            part = u"""<tr valign="top">
        			<td width="249" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #ffffff; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in"><p>
        				<font size="3" style="font-size: 12pt">AZ: XXX</font></p>
        			</td>
        			<td width="71" style="background: transparent" style="border: 1px solid #ffffff; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<font size="3" style="font-size: 12pt">XXX</font></p>
        			</td>
        			<td width="64" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #ffffff; border-left: 1px solid #ffffff; border-right: 1px solid #000000; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<font size="3" style="font-size: 12pt">XXX</font></p>
        			</td>
        		</tr>"""
            cnt = 0
            while part.find('XXX') > -1:      
                #print(relation[cnt], cnt)
                if isinstance(relation[cnt], datetime.datetime):
                    ins_text = datetime.datetime.strftime(relation[cnt], '%d%m%Y %H:%M')
                elif isinstance(relation[cnt], str):
                    ins_text = relation[cnt].strip()
                elif isinstance(relation[cnt], (float, int)):
                    ins_text = '{0:.3f}'.format(relation[cnt])
                else:
                    try:
                        ins_text = check_encoding2(relation[cnt])
                    except:
                        ins_text = '!!!Fehler!!!' #print(type(relation[cnt]), relation[cnt])
                part = part.replace('XXX', ins_text, 1)
                cnt += 1
            return part
        
        
        def replace_add_bal(relation):
            part = u"""<tr valign="top">
        			<td width="250" style="background: transparent" style="border: none; padding: 0cm"><p>
        				<font size="3" style="font-size: 12pt">XXX</font></p>
        			</td>
        			<td width="150" style="background: transparent" style="border: none; padding: 0cm"><p>
        				<font size="3" style="font-size: 12pt">XXX</font></p>
        			</td>
        		</tr>"""
            cnt = 0
            while part.find('XXX') > -1:      
                #print(relation[cnt], cnt)
                if isinstance(relation[cnt], datetime.datetime):
                    ins_text = datetime.datetime.strftime(relation[cnt], '%d%m%Y %H:%M')
                elif isinstance(relation[cnt], str):
                    ins_text = relation[cnt].strip()
                elif isinstance(relation[cnt], float):
                    ins_text = '{0:.3f}'.format(relation[cnt])
                else:
                    ins_text = str(relation[cnt])
                part = part.replace('XXX', ins_text, 1)
                cnt += 1
            return part
        
        
        def table_for_extr_in_ctch(userDict):
            try:
                userDict['uIC']
                tx = 'uIC'
            except:
                tx = b'uIC'
            text = ''
            for i, use in enumerate(userDict[tx][-2]):
                
                try: 
                    area = userDict['area']
                except:
                    area = userDict[b'area']
                
                if use in ('j', '1', 'y') and not userDict['uIC'][0][i].encode(encoding=SETUP.ENCODING)  \
                == userDict['applicationNumber'].encode(encoding=SETUP.ENCODING):
                    if userDict[tx][-1][i] in ('j', '1', 'y'):
                        res = userDict[tx][3][i] * -1
                    else:
                        res = userDict[tx][3][i]
                    tmp = [userDict[tx][0][i], res, res/area*1000000]
                    text += replace_etxr_in_ctch(tmp)
            return text
        
        
        def table_for_add_bal(userDict):
            try:
                userDict['names_addBalance']
                tx = 'names_addBalance'
                lx = 'list_addBalance'
            except:
                tx = b'names_addBalance'
                lx = b'list_addBalance'
            text = ''
            for i, use in enumerate(userDict[tx]):
                if userDict[tx][i]:
                    tmp = [userDict[tx][i], userDict[lx][i]]
                    text += replace_add_bal(tmp)
            return text
        
        
        part1 = u"""<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
        <html>
        <head>
        	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        	<title></title>
        	<meta name="generator" content="LibreOffice 6.1.3.1 (Linux)"/>
        	<meta name="author" content="Schneppmüller, Martin & Müller, Ruben"/>
        	<meta name="created" content="2018-10-24T09:27:00"/>
        	<meta name="changed" content="2018-10-25T14:07:12.797027230"/>
        	<meta name="AppVersion" content="15.0000"/>
        	<meta name="Company" content="LHW"/>
        	<meta name="DocSecurity" content="0"/>
        	<meta name="HyperlinksChanged" content="false"/>
        	<meta name="LinksUpToDate" content="false"/>
        	<meta name="ScaleCrop" content="false"/>
        	<meta name="ShareDoc" content="false"/>
        	<style type="text/css">
        		@page { size: 8.27in 11.69in; margin-left: 0.98in; margin-right: 0.98in; margin-top: 0.98in; margin-bottom: 0.49in }
        		p { margin-bottom: 0.1in; direction: ltr; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent }
        		a:link { color: #000080; so-language: zxx; text-decoration: underline }
        		a:visited { color: #800000; so-language: zxx; text-decoration: underline }
        	</style>
        </head>
        <body lang="de-DE" link="#000080" vlink="#800000" dir="ltr"><p style="margin-bottom: 0.11in; line-height: 108%">
        <font size="4" style="font-size: 14pt"><u><b>Übersicht der
        Berechnungsergebnisse:</b></u></font></p>
        <p style="margin-bottom: 0.11in; line-height: 108%"><font size="3" style="font-size: 12pt"><font face="Arial, serif">Nutzer:
        				</font><font face="Arial, serif">XXX</font></font></p>
        <p style="margin-bottom: 0.11in; line-height: 108%"><font size="3" style="font-size: 12pt"><font face="Arial, serif">Vorgangsnummer
        (LHW): 		</font><font face="Arial, serif">XXX</font></font></p>
        <p style="margin-bottom: 0.11in; line-height: 108%"><font size="3" style="font-size: 12pt"><font face="Arial, serif">Bearbeitungsdatum:
        		</font><font face="Arial, serif">XXX</font></font></p>
        <p style="margin-bottom: 0.11in; line-height: 108%"><font size="3" style="font-size: 12pt"><font face="Arial, serif">Bearbeitet
        von: 			</font><font face="Arial, serif">XXX</font></font></p>
        <p style="margin-bottom: 0.11in; line-height: 108%"><font size="3" style="font-size: 12pt"><font face="Arial, serif">Koordinaten
        der Nutzung [UTM 32]: </font></font>
        </p>
        <p style="margin-bottom: 0.11in; line-height: 108%"><font size="3" style="font-size: 12pt"><font face="Arial, serif">	</font><font face="Arial, serif">RW
        [OST] </font><font face="Arial, serif">XXX</font></font></p>
        <font size="3" style="font-size: 12pt"><font face="Arial, serif">	</font><font face="Arial, serif">HW
        [Nord] </font><font face="Arial, serif">XXX</font></font></p>
        <p style="margin-bottom: 0.11in; line-height: 108%"><font size="3" style="font-size: 12pt"><font face="Arial, serif">Überschlägige
        Einzugsgebietsfläche [km2]:		</font><font face="Arial, serif">XXX</font></font></p>
        <p style="margin-bottom: 0in; line-height: 100%"><br/>"""
        
        part2 = u"""</p>
        <center>
        	<table width="428" cellpadding="7" cellspacing="0" bgcolor="#ededed" style="background: #ededed">
        		<col width="249"/>
        
        		<col width="71"/>
        
        		<col width="64"/>
        
        		<tr valign="top">
        			<td width="249" style="background: transparent" style="border-top: 1px solid #000000; border-bottom: 1px solid #ffffff; border-left: 1px solid #000000; border-right: 1px solid #ffffff; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<br/>
        
        				</p>
        			</td>
        			<td width="71" style="background: transparent" style="border-top: 1px solid #000000; border-bottom: none; border-left: 1px solid #ffffff; border-right: 1px solid #ffffff; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<font size="3" style="font-size: 12pt">Tm3/Jahr</font></p>
        			</td>
        			<td width="64" style="background: transparent" style="border-top: 1px solid #000000; border-bottom: none; border-left: 1px solid #ffffff; border-right: 1px solid #000000; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<font size="3" style="font-size: 12pt">mm/Jahr</font></p>
        			</td>
        		</tr>"""
                
        part3a = u"""<tr valign="top">
        			<td width="249" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in"><p>
        				<font size="3" style="font-size: 12pt">Beantragte Menge (lokale Entnahme)</font></p>
        			</td>
        			<td width="71" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #000000; border-left: 1px solid #ffffff; border-right: 1px solid #ffffff; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<font size="3" style="font-size: 12pt">XXX</font></p>
        			</td>
        			<td width="64" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #000000; border-left: 1px solid #ffffff; border-right: 1px solid #000000; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<font size="3" style="font-size: 12pt"> </font></p>
        			</td>
        		</tr>"""
                
        part3b = u"""<tr valign="top">
        			<td width="249" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in"><p>
        				<font size="3" style="font-size: 12pt">Summe zusätzlicher angesetzter Entnahmen</font></p>
        			</td>
        			<td width="71" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #000000; border-left: 1px solid #ffffff; border-right: 1px solid #ffffff; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<font size="3" style="font-size: 12pt">XXX</font></p>
        			</td>
        			<td width="64" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #000000; border-left: 1px solid #ffffff; border-right: 1px solid #000000; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<font size="3" style="font-size: 12pt"> </font></p>
        			</td>
        		</tr>"""
        
        part4 = u"""<tr>
        			<td colspan="3" width="412" valign="top" style="background: transparent" style="border-top: 1px solid #000000; border-bottom: 1px solid #ffffff; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<font size="3" style="font-size: 12pt">im Einzugsgebiet der
        				Nutzung <br/>
        liegende Entnahmen </font>
        				</p>
        			</td>
        		</tr>"""
        
        part6 = u"""<tr>
        			<td colspan="3" width="412" valign="top" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #ffffff; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<br/>
        
        				</p>
        			</td>
        		</tr>
        		<tr valign="top">
        			<td width="249" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in"><p>
        				<font size="3" style="font-size: 12pt">Summe der Entnahmen im
        				Einzugsgebiet der Nutzung</font></p>
        			</td>
        			<td width="71" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #000000; border-left: 1px solid #ffffff; border-right: 1px solid #ffffff; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<font size="3" style="font-size: 12pt">XXX</font></p>
        			</td>
        			<td width="64" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #000000; border-left: 1px solid #ffffff; border-right: 1px solid #000000; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<font size="3" style="font-size: 12pt">XXX</font></p>
        			</td>
        		</tr>
        		<tr valign="top">
        			<td width="249" style="background: transparent" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in"><p>
        				<br/>
        
        				</p>
        			</td>
        			<td width="71" style="background: transparent" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #ffffff; border-right: 1px solid #ffffff; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<br/>
        
        				</p>
        			</td>
        			<td width="64" style="background: transparent" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #ffffff; border-right: 1px solid #000000; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<br/>
        
        				</p>
        			</td>
        		</tr>"""
        
        part7 = u"""<tr>
        			<td colspan="3" width="412" valign="top" style="background: transparent" style="border-top: 1px solid #000000; border-bottom: 1px solid #ffffff; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<font size="3" style="font-size: 12pt">betrachtete unterirdische
        				Abflusskomponenten</font></p>
        			</td>
        		</tr>
        		<tr valign="top">
        			<td width="249" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #ffffff; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in"><p>
        				<font size="3" style="font-size: 12pt">RG1</font></p>
        			</td>
        			<td width="71" style="background: transparent" style="border: 1px solid #ffffff; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<font size="3" style="font-size: 12pt">XXX</font></p>
        			</td>
        			<td width="64" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #ffffff; border-left: 1px solid #ffffff; border-right: 1px solid #000000; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<font size="3" style="font-size: 12pt">XXX</font></p>
        			</td>
        		</tr>
        		<tr valign="top">
        			<td width="249" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #ffffff; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in"><p>
        				<font size="3" style="font-size: 12pt">RG2</font></p>
        			</td>
        			<td width="71" style="background: transparent" style="border: 1px solid #ffffff; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<font size="3" style="font-size: 12pt">XXX</font></p>
        			</td>
        			<td width="64" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #ffffff; border-left: 1px solid #ffffff; border-right: 1px solid #000000; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<font size="3" style="font-size: 12pt">XXX</font></p>
        			</td>
        		</tr>
        		<tr valign="top">
        			<td width="249" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #ffffff; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in"><p>
        				<font size="3" style="font-size: 12pt">RD</font></p>
        			</td>
        			<td width="71" style="background: transparent" style="border: 1px solid #ffffff; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<font size="3" style="font-size: 12pt">XXX</font></p>
        			</td>
        			<td width="64" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #ffffff; border-left: 1px solid #ffffff; border-right: 1px solid #000000; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<font size="3" style="font-size: 12pt">XXX</font></p>
        			</td>
        		</tr>
        		<tr valign="top">
        			<td width="249" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #ffffff; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in"><p>
        				<font size="3" style="font-size: 12pt">RH</font></p>
        			</td>
        			<td width="71" style="background: transparent" style="border: 1px solid #ffffff; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<font size="3" style="font-size: 12pt">XXX</font></p>
        			</td>
        			<td width="64" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #ffffff; border-left: 1px solid #ffffff; border-right: 1px solid #000000; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<font size="3" style="font-size: 12pt">XXX</font></p>
        			</td>
        		</tr>
        		<tr valign="top">
        			<td width="249" height="23" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in"><p>
        				<font size="3" style="font-size: 12pt">Summe unterirdische
        				Abflusskomponenten:</font></p>
        			</td>
        			<td width="71" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #000000; border-left: 1px solid #ffffff; border-right: 1px solid #ffffff; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<font size="3" style="font-size: 12pt">XXX</font></p>
        			</td>
        			<td width="64" style="background: transparent" style="border-top: 1px solid #ffffff; border-bottom: 1px solid #000000; border-left: 1px solid #ffffff; border-right: 1px solid #000000; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"><p>
        				<font size="3" style="font-size: 12pt">XXX</font></p>
        			</td>
        		</tr>
        	</table>
        </center>"""
        
#        part8 = u"""<p style="margin-bottom: 0in; line-height: 100%"><br/>
#        </p>
#        <p style="margin-bottom: 0.11in; line-height: 108%"><br/>
#        <br/>
#        </p>
#        <p style="margin-bottom: 0.11in; line-height: 108%"><font size="3" style="font-size: 12pt"><font face="Arial, serif"><font size="4" style="font-size: 14pt"><u><b>Ergebnis:
#        <br/>
#        </b></u></font></font><p style="margin-bottom: 0.11in; line-height: 108%"><font face="Arial, sans-serif">XXX
#        </font></font></p>
#        <p style="margin-bottom: 0.11in; line-height: 108%"><font face="Arial, sans-serif"><font size="3" style="font-size: 12pt">Kartenausschnitt:</font></font></p>
#        <p align="center" style="margin-bottom: 0.11in; border: 1px solid #000000; padding: 0.01in 0.06in; line-height: 108%">
#        <img src="Karte.png" name="Grafik 1" align="bottom" width="285" height="361" border="0"/>
#        <img src="Legende.png" name="Grafik 2" align="bottom" vspace="1" width="252" height="359" border="0"/>
#        </p>
#        <p style="margin-bottom: 0.11in; line-height: 108%"><font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 14pt"><u><b>Zusätzliche
#        Angaben zur internen Verwendung:</b></u></font></font></font></p>
#        <p style="margin-bottom: 0.11in; line-height: 108%"><font size="3" style="font-size: 12pt"><font face="Arial, serif">Koordinaten
#        des Brunneneinzugsgebietendpunktes [UTM 32]: <br/>
#        	RW [OST] </font><font face="Arial, serif">XXX</font><font face="Arial, serif">
#        </font></font>
#        </p>
#        <font size="3" style="font-size: 12pt"><font face="Arial, serif">	HW [Nord]
#        </font><font face="Arial, serif">XXX</font></font></p>
#        <font face="Arial, serif"><font size="3" style="font-size: 12pt">	QL
#        [l/s/km2]:	</font></font><font face="Arial, serif"><font size="3" style="font-size: 12pt">XXX</font></font><font face="Arial, serif"><font size="2" style="font-size: 9pt"><br/>
#        </font></font><br/>
#        
#        </p>
#        <p style="margin-bottom: 0.11in; line-height: 108%"><a name="_GoBack"></a>
#        <font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 14pt"><u><b>Berechnungsgrundlagen
#        für das Brunneneinzugsgebiet:</b></u></font></font></font></p>
#        <p style="margin-bottom: 0.11in; line-height: 108%"><font size="3" style="font-size: 12pt"><font face="Arial, serif">Beantragte
#        Entnahme [Tm3/Jahr]:	XXX</font></p>
#        <p style="margin-bottom: 0.11in; line-height: 108%"><font size="3" style="font-size: 12pt"><font face="Arial, serif">Entnahme
#        pro Tag [m3/Tag]:		XXX</font></p>
#        <p style="margin-bottom: 0.11in; line-height: 108%"><font size="3" style="font-size: 12pt"><font face="Arial, serif">Anzahl
#        der Entnahmetage <br/>
#        für die Berechnung:			XXX</font></p>
#        <p style="margin-bottom: 0.11in; line-height: 108%"><font size="3" style="font-size: 12pt"><font face="Arial, serif">Berechnungsgrundlage
#        für <br/>
#        das Einzugsgebiet [Tm3/Jahr): 		XXX</font></p>"""
    
        part8 = u"""<p style="margin-bottom: 0in; line-height: 100%"><br/>
        </p>
        <p style="margin-bottom: 0.11in; line-height: 108%"><br/>
        <br/>
        </p>
        <p style="margin-bottom: 0.11in; line-height: 108%"><font size="3" style="font-size: 12pt"><font face="Arial, serif"><font size="4" style="font-size: 14pt"><u><b>Ergebnis:
        <br/>
        </b></u></font></font><p style="margin-bottom: 0.11in; line-height: 108%"><font face="Arial, sans-serif">XXX
        </font></font></p>
        <p style="margin-bottom: 0.11in; line-height: 108%"><font face="Arial, sans-serif"><font size="3" style="font-size: 12pt">Kartenausschnitt:</font></font></p>
        <p align="center" style="margin-bottom: 0.11in; border: 1px solid #000000; padding: 0.01in 0.06in; line-height: 108%">
        <img src="Karte.png" name="Grafik 1" align="bottom"/>
        </p>
        <p style="margin-bottom: 0.11in; line-height: 108%"><font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 14pt"><u><b>Zusätzliche
        Angaben zur internen Verwendung:</b></u></font></font></font></p>
        <p style="margin-bottom: 0.11in; line-height: 108%"><font size="3" style="font-size: 12pt"><font face="Arial, serif">Koordinaten
        des Brunneneinzugsgebietendpunktes [UTM 32]: <br/>
        	RW [OST] </font><font face="Arial, serif">XXX</font><font face="Arial, serif">
        </font></font>
        </p>
        <font size="3" style="font-size: 12pt"><font face="Arial, serif">	HW [Nord]
        </font><font face="Arial, serif">XXX</font></font></p>
        <font face="Arial, serif"><font size="3" style="font-size: 12pt">	QL
        [l/s/km2]:	</font></font><font face="Arial, serif"><font size="3" style="font-size: 12pt">XXX</font></font><font face="Arial, serif"><font size="2" style="font-size: 9pt"><br/>
        </font></font><br/>
        
        </p>
        <p style="margin-bottom: 0.11in; line-height: 108%"><a name="_GoBack"></a>
        <font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 14pt"><u><b>Berechnungsgrundlagen
        für das Brunneneinzugsgebiet:</b></u></font></font></font></p>
        <p style="margin-bottom: 0.11in; line-height: 108%"><font size="3" style="font-size: 12pt"><font face="Arial, serif">Beantragte
        Entnahme [Tm3/Jahr]:	XXX</font></p>
        <p style="margin-bottom: 0.11in; line-height: 108%"><font size="3" style="font-size: 12pt"><font face="Arial, serif">Entnahme
        pro Tag [m3/Tag]:		XXX</font></p>
        <p style="margin-bottom: 0.11in; line-height: 108%"><font size="3" style="font-size: 12pt"><font face="Arial, serif">Anzahl
        der Entnahmetage <br/>
        für die Berechnung:			XXX</font></p>
        <p style="margin-bottom: 0.11in; line-height: 108%"><font size="3" style="font-size: 12pt"><font face="Arial, serif">Berechnungsgrundlage
        für <br/>
        das Einzugsgebiet [Tm3/Jahr): 		XXX</font></p>"""
        
        part9 = u"""<p style="margin-bottom: 0.28cm; line-height: 108%"><font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 14pt"><u><b>Weitere
        Angaben:</b></u></font></font></font></p>
        <p style="margin-bottom: 0.28cm; line-height: 108%"><br/>
        <br/>
        
        </p>
        <center>
        	<table width="428" cellpadding="7" cellspacing="0" bgcolor="#ededed" style="background: #ededed">
        		<col width="250"/>
        
        		<col width="150"/>
        
        		<tr valign="top">
        			<td width="250" style="background: transparent" style="border: none; padding: 0cm"><p>
        				<font size="3" style="font-size: 12pt">beantragt qm/h</font></p>
        			</td>
        			<td width="150" style="background: transparent" style="border: none; padding: 0cm"><p>
        				<font size="3" style="font-size: 12pt">XXX</font></p>
        			</td>
        		</tr>
        		<tr valign="top">
        			<td width="250" style="background: transparent" style="border: none; padding: 0cm"><p>
        				<font size="3" style="font-size: 12pt">beantragt qm/d</font></p>
        			</td>
        			<td width="150" style="background: transparent" style="border: none; padding: 0cm"><p>
        				<font size="3" style="font-size: 12pt">XXX</font></p>
        			</td>
        		</tr>
        		<tr valign="top">
        			<td width="250" style="background: transparent" style="border: none; padding: 0cm"><p>
        				<font size="3" style="font-size: 12pt">beantragt mit._tqm/a</font></p>
        			</td>
        			<td width="150" style="background: transparent" style="border: none; padding: 0cm"><p>
        				<font size="3" style="font-size: 12pt">XXX</font></p>
        			</td>
        		</tr>
        		<tr valign="top">
        			<td width="250" style="background: transparent" style="border: none; padding: 0cm"><p>
        				<font size="3" style="font-size: 12pt">beantragt max_tqm/a</font></p>
        			</td>
        			<td width="150" style="background: transparent" style="border: none; padding: 0cm"><p>
        				<font size="3" style="font-size: 12pt">XXX</font></p>
        			</td>
        		</tr>
        		<tr valign="top">
        			<td width="250" height="24" style="background: transparent" style="border: none; padding: 0cm"><p>
        				<font size="3" style="font-size: 12pt">genehmigt qm/h</font></p>
        			</td>
        			<td width="150" style="background: transparent" style="border: none; padding: 0cm"><p>
        				<br/>
        
        				</p>
        			</td>
        		</tr>
        		<tr valign="top">
        			<td width="250" height="24" style="background: transparent" style="border: none; padding: 0cm"><p>
        				<font size="3" style="font-size: 12pt">genehmigt qm/d</font></p>
        			</td>
        			<td width="150" style="background: transparent" style="border: none; padding: 0cm"><p>
        				<br/>
        
        				</p>
        			</td>
        		</tr>
        		<tr valign="top">
        			<td width="250" height="24" style="background: transparent" style="border: none; padding: 0cm"><p>
        				<font size="3" style="font-size: 12pt">genehmigt mit._tqm/a</font></p>
        			</td>
        			<td width="150" style="background: transparent" style="border: none; padding: 0cm"><p>
        				<br/>
        
        				</p>
        			</td>
        		</tr>
        		<tr valign="top">
        			<td width="250" height="24" style="background: transparent" style="border: none; padding: 0cm"><p>
        				<font size="3" style="font-size: 12pt">genehmigt max_tqm/a</font></p>
        			</td>
        			<td width="150" style="background: transparent" style="border: none; padding: 0cm"><p>
        				<br/>
        
        				</p>
        			</td>
        		</tr>
        	</table>
        </center>
        <p style="margin-bottom: 0.28cm; line-height: 108%"><br/>
        <br/>"""
            
        part10 = u"""
        </p>
        <p style="margin-bottom: 0.28cm; line-height: 108%"><font color="#000000"><font face="Arial, serif"><font size="4" style="font-size: 14pt"><u><b>Weitere
        Bilanzierungsgrößen:</b></u></font></font></font></p>
        <p style="line-height: 108%"><br/>
        <br/>
        
        </p>
        <center>
        	<table width="428" cellpadding="7" cellspacing="0" bgcolor="#ededed" style="background: #ededed">
        		<col width="250"/>
        
        		<col width="150"/>
        
        		<tr valign="top">
        			<td width="250" style="background: transparent" style="border: none; padding: 0cm"><p>
        				<font size="3" style="font-size: 12pt">Name Bilanzgröße</font></p>
        			</td>
        			<td width="150" style="background: transparent" style="border: none; padding: 0cm"><p>
        				<font size="3" style="font-size: 12pt">[Tm3/Jahr]</font></p>
        			</td>
        		</tr>"""
        
        part12 = u"""	</table>
        </center>
        <p align="left" style="margin-bottom: 0cm; background: transparent; line-height: 100%; orphans: 0; widows: 0">
        <br/>
        </p>
        </body>
        </html>"""  
            
        cnt_doc = counter()
        #print(1)
        part1 = replace_all_XXX(part1, relation, cnt_doc)
        #print(2)
        part3a = replace_all_XXX(part3a, relation, cnt_doc)
        part3b = replace_all_XXX(part3b, relation, cnt_doc)
        #print(3)
        part5 = table_for_extr_in_ctch(userDict)
        #print(4)
        part6 = replace_all_XXX(part6, relation, cnt_doc)
        part7 = replace_all_XXX(part7, relation, cnt_doc)
        part8 = replace_all_XXX(part8, relation, cnt_doc)
        part9 = replace_all_XXX(part9, relation, cnt_doc)
        part11 = table_for_add_bal(userDict)
        part12 = replace_all_XXX(part12, relation, cnt_doc)
        #%%
        
        htmlx = part1 + part2 + part3a + part3b + part4 + part5 + part6 + part7 + part8 + part9 + part10 + part11 + part12
        htmlx = check_encoding2(htmlx)
        file_html = os.path.join(userDict['path_statement'])
        #print(file_html, type(file_html))
        with codecs.open(file_html, 'w', encoding='utf-8') as fid:
            fid.writelines(htmlx)#.encode('utf-8')
    except:
        traceback.print_exc()
