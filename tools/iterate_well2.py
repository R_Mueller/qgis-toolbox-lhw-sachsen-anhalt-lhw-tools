# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Provides the functionality to iteratively calculate the well catchment given
    spactially distributed ground waster recharge fields iteratively.

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""


#############################################
### DEBUG SWITCH
#############################################
DEBUG = False
#############################################

import math
import os
from qgis.core import QgsMapLayerRegistry
from qgis.core import QgsCoordinateReferenceSystem
from qgis.core import QgsGeometry
from qgis.core import QgsPoint
from qgis.core import QgsFeature
from qgis.core import QgsField
from PyQt4.QtCore import QSettings
from PyQt4.QtCore import QVariant
from qgis.core import QgsVectorLayer
from qgis.core import NULL
from qgis.utils import iface
from qgis.analysis import QgsZonalStatistics
from qgis.gui import QgsMessageBar
from qgis.core import QgsFeatureRequest

import qgis_toolbox
from .. setup import setup__ as SETUP


def well_within_TG(geom):
    '''check if the bufferGeometry intersects the catchment
    
    Parameters
    ----------
    bufferGeometry : buffer geometry object
        the buffer around the well
        
    Returns
    tmp : bool
        true if bufferGeometry is within the area, false otherwise'''
        
    layer = get_layer(SETUP.TGLAYER)
    feats = layer.getFeatures()
    idTG = layer.fieldNameIndex( "TGID" )
    for polyFeat_inner in feats:
        polyGeom_inner = polyFeat_inner.geometry()
        if geom.within(polyGeom_inner):
            return polyFeat_inner[idTG]


def get_position_of_catchment_end(active=False):
    '''get the position of the endpoint of the catchment

    Returns
    -------
    position_of_catchmant_end : list
        [x-axis coordinates, y-axis coordinates] of catchment end point'''
    try:
        if active:
            end_point = active
            load_layer = active.name()
        else:
            load_layer = SETUP.ENDPOINT
            if not qgis_toolbox.check_layer_loaded(load_layer):
                iface.messageBar().pushMessage(u"Kein Layer \"{}\" geladen.".format(load_layer),
                            level=QgsMessageBar.INFO, duration=3)
                return None
     
            end_point = QgsMapLayerRegistry.instance().mapLayersByName(load_layer)[0]
        end_point_Features = end_point.getFeatures()
     
        for end_point_Feature in end_point_Features:
            end_point_Geometrie = end_point_Feature.geometry()
            position_of_catchmant_end = end_point_Geometrie.asPoint()
    except:
        iface.messageBar().pushMessage(u"Kein Endpuntk von Layer \"{}\" lesbar.".format(load_layer),
                            level=QgsMessageBar.INFO, duration=3)
    return position_of_catchmant_end
 


def get_shape_parameter():
    '''returns a dictionary with the parameters a, b and F_threshold'''
    A = 20
    B = 0.18
    F_threshold = 1 + A*(0.0005)**B
    shape_parameters = {'a': A, 'b': B, 'F_treshold': F_threshold}
    return shape_parameters
    
    

def get_Iteration_Parameter():
    '''returns a dictionary with the default parameter set of the iterative search'''
    ipDict = {'iterMax': SETUP.ITERMAX, ### since it's so fast now...
                'Tol': SETUP.EPSITER,
                'factorP': 0.5,
                'relaxUnder': 5,
                'relaxUnderIter1': SETUP.RELAXB1, # 30, ### we should never get to this stage
                'relaxUnderIter2': SETUP.RELAXB2, # 50, ### very unlikely, if we reach this, then it is borked anyway...
                'relaxF1': 0.5,
                'relaxF2': 0.35}
    return ipDict


def get_layer(layername):
    '''return the layer object belonging to the layername
    
    Parameters
    ----------
    layername : str
        the name of the layer'''
    mapCanvas = iface.mapCanvas()
    layers = mapCanvas.layers()
    layer_layername = None
    for layer in layers:
        if layer.name() == layername:
           layer_layername = layer
    if layer_layername is None:
        iface.messageBar().pushMessage(u"Layer {} nicht verfügbar. Deaktiviert?".format(layername),
                                       level=QgsMessageBar.WARNING, duration=3)
    else:
        return layer_layername

def find_direction(cosvalue, sinvalue):
    '''calculate the direction angle from cos and sin
    
    Parameters
    ----------
    cosvalue : float
        the cosine
    sinvalue : float
        the sine
    
    Returns
    -------
    direction : float
        the direction'''
    tanalpha = sinvalue/cosvalue
    if tanalpha > 0:
        if sinvalue > 0:
            direction = math.degrees(math.atan(tanalpha))
        else:
            direction = math.degrees(math.atan(tanalpha))+180
                        
    if tanalpha < 0:
        if sinvalue > 0:
            direction = math.degrees(math.atan(tanalpha))+180
        else:
                direction = math.degrees(math.atan(tanalpha))
    #print("direction {}".format(direction))
    return direction

    
def get_bufferGeometry(wellDict, iterDict):
    '''create a buffer around the well point and return the geometry object
    
    Parameters
    ----------
    wellDict : dict
        contains the informations about the well
    iterDict : dict
        contains the information about parameters at the current iteration
        
    Returns
    -------
    tmp : buffer geometry object
        the buffer around the well'''
        
    bufferGeometry = QgsGeometry.fromPoint(QgsPoint(wellDict['coordinateX'], 
                                                    wellDict['coordinateY']))
    return bufferGeometry.buffer(iterDict['R'], 5)


def is_within_area(bufferGeometry):
    '''check if the bufferGeometry intersects the catchment
    
    Parameters
    ----------
    bufferGeometry : buffer geometry object
        the buffer around the well
        
    Returns
    tmp : bool
        true if bufferGeometry is within the area, false otherwise'''
        
    untersuchungsgebiete = get_layer("untersuchungsgebiet")
    gebiet_untersuchung_Features = untersuchungsgebiete.getFeatures()
    for polyFeat_innner in gebiet_untersuchung_Features:
        polyGeom_inner = polyFeat_innner.geometry()
            # Geometrie von Buffer
        if bufferGeometry.within(polyGeom_inner):
            return True
        else:
            return False


def get_gradient_and_direction(bufferGeometry, iterDict):
    '''calculate the mean direction and the gradient for a geometry
    
    Parameters
    ----------
    bufferGeometry : buffer geometry object
        the buffer around the well
    iterDict : dict
        contains the information about parameters at the current iteration
        
    Returns
    -------
    iterDict : dict
        the updated iteration dictionary'''
    QSettings()
    well_buffer = QgsVectorLayer("Polygon", "pointbuffer", "memory")
    feature = well_buffer.dataProvider()
    poly = QgsFeature()

    poly.setGeometry(bufferGeometry)
    feature.addFeatures([poly])
    well_buffer.updateExtents()
    
    gefaellepath = os.path.join(SETUP.BASEFOLDER, 'gefaelle.tif')
    gefaelle = get_meanvalue(well_buffer, gefaellepath)
    #print('gefaelle {}'.format(gefaelle))
    
    cospath = gefaellepath = os.path.join(SETUP.BASEFOLDER, 'cos.tif') #r"M:\Fan\quasi_ein_Brunnen\cos.tif"
    cos =get_meanvalue (well_buffer, cospath)
    sinpath =gefaellepath = os.path.join(SETUP.BASEFOLDER, 'sin.tif') #  r"M:\Fan\quasi_ein_Brunnen\sin.tif"
    sin = get_meanvalue (well_buffer, sinpath)
    direction = find_direction(cos, sin)
    
    iterDict['slope'] = gefaelle/100
    iterDict['direction'] = direction
    
    return iterDict
        
        
def get_meanvalue(vpoly, path):
    '''calculate the areal mean with ZonalStatistics
    
    Parameters
    ----------
    vpoly : str
        calculate the mean for the layer with this name
    path : str
        path to the layer file
        
    Returns
    meanvalue : float
        the calculated mean
    '''
    zoneStat = QgsZonalStatistics(vpoly, path, "", 1, QgsZonalStatistics.Mean)
    zoneStat.calculateStatistics(None)
    meanvalue = 0
    for feature in vpoly.getFeatures():
        mean_value = feature.attributes()
    meanvalue = mean_value[0]
    return meanvalue
##################################################################

def get_parameter_of_well_catchment(wellDict, iterDict):
    '''calulate various parameters necessary for the catchment geometry
    
    Parameters
    ----------
    wellDict : dict
        contains the informations about the well
    iterDict : dict
        contains the information about parameters at the current iteration
        
    Returns
    -------
    iterDict : dict
        the updated iteration dictionary'''
        
    shpp = get_shape_parameter()
    iterDict['F'] = 1 + shpp['a'] *(iterDict['slope'])**shpp['b']
    if not iterDict['staticL']:
        iterDict['L'] = (1000000 * wellDict['Q'] * iterDict['F'] / iterDict['GWR']) ** 0.5
        # die Laenge der Ende der Stromlinie
        iterDict['endX'] = (math.sin((iterDict['direction']+180)*math.pi/180))*iterDict['L'] + wellDict['coordinateX']
        # Position der Ende der Bahnlinien (x-Koordinate)
        iterDict['endY'] = (math.cos((iterDict['direction']+180)*math.pi/180))*iterDict['L'] + wellDict['coordinateY']
    # Position der Ende der Bahnlinien (y-Koordinate)
    iterDict['Xk'] = (iterDict['L']**2 \
                      + (2*1000000*wellDict['Q']/math.pi/iterDict['GWR']))**0.5 - iterDict['L']
    iterDict['Bmax'] = 2*((((iterDict['L']+iterDict['Xk'])**2) \
                          + (2*(math.pi+2)*1000000*wellDict['Q']/iterDict['GWR']))**0.5 \
                          - iterDict['L']-iterDict['Xk']) / (math.pi+2)
    iterDict['Xbmax'] = ((1000000*wellDict['Q']/math.pi/iterDict['GWR'])-((iterDict['Bmax']**2)/4))**0.5
    #parameter_of_well_catchment = [position, recharge, Q, F_value, L, ende_x, ende_y, X_k, B_MAX, X_bmax]
    return iterDict


def get_polygon_of_well_catchment(iterDict, wellDict):
    '''calculate the well catchment geometry

    Parameters
    ----------
    wellDict : dict
        contains the informations about the well
    iterDict : dict
        contains the information about parameters at the current iteration
        
    Returns
    -------
    catchmentGeometry : geometry object
        the well catchment geometry'''
        
    shapeParameter = get_shape_parameter()
    QSettings()
    
    X_k = iterDict['Xk'] 
    L = iterDict['L']
    B_MAX = iterDict['Bmax']
    ende_x = iterDict['endX']
    ende_y = iterDict['endY']
    X_bmax = iterDict['Xbmax']
    
    xB = [-X_k, -0.8*X_k, 0, X_bmax/3, X_bmax, L/2, 0.75*L, 0.9*L, L, \
          0.9*L, 0.75*L, L/2, X_bmax, X_bmax/3, 0, -0.8*X_k ]
    yB = [0, 0.8*X_k, 1.5*X_k, 0.3*(B_MAX-(3*X_k))+(1.5*X_k), 0.5*B_MAX, 1.9*X_k, \
          1.1*X_k, 0.44*X_k, 0, -0.44*X_k, -1.1*X_k, -1.9*X_k, -0.5*B_MAX, \
          -0.3*(B_MAX-(3*X_k))-(1.5*X_k), -1.5*X_k, -0.8*X_k]
          
    if iterDict['F'] > shapeParameter['F_treshold']:
        # gefaelle = gradient_threshold_Interpolation
        # wenn Gefaell genug gross ist
        # Reihe von temporaeren Konstruktionspunkte des tropfenfoermigen Einzugsgebiets
        x = xB
        y = yB
    else:        
        # Reihe von temporaeren Konstruktionspunkte des kreisfoermigen Einzugsgebiets       
        winkel = list(range(16))
        R = (1000000*wellDict['Q']/iterDict['GWR']/math.pi)**0.5
        x_rund= list(range(16))
        y_rund= list(range(16))
        x = list(range(16)) 
        y = list(range(16))
        
        for d in range(16):
            # Reihe von temporaeren Konstruktionspunkte des kreisfoermigen Einzugsgebiets 
            winkel[d] = -0.5*math.pi+((d)*math.pi/8)
            x_rund[d] = R*math.sin(winkel[d])
            y_rund[d] = R*math.cos(winkel[d])
            
               # Kombination durch Interpolation
            x[d] = ((shapeParameter['F_treshold']-iterDict['F'])*x_rund[d] + ((iterDict['F']-1)*xB[d]))\
                   / (shapeParameter['F_treshold'] - 1)
            y[d] = ((shapeParameter['F_treshold']-iterDict['F'])*y_rund[d] + ((iterDict['F']-1)*yB[d]))\
                   / (shapeParameter['F_treshold'] - 1)

        # mit Rotationsmatrix zur originalen Kartesische Koordinaten drehen
        # Winkel zischen urspruenglichem Koordinatensystem und neuem Koordinatensystem (gegen Uhrzeigersinn)
        # theta: Winkel
    costheta = (wellDict['coordinateX']-ende_x)/ (((wellDict['coordinateX']-ende_x)**2 + (wellDict['coordinateY']-ende_y)**2)**0.5)
    sintheta = (wellDict['coordinateY']-ende_y)/ (((wellDict['coordinateX']-ende_x)**2 + (wellDict['coordinateY']-ende_y)**2)**0.5)
    
        # Erzeugung einer Tabelle fuer die Konstruktionspunkte zur Abgrenzung eines Brunneneinzugsgebietes
    points = list(range(16))
    
    for d3 in range(16):
        points[d3] = QgsPoint(-1*x[d3]*costheta+y[d3]*sintheta + wellDict['coordinateX'], 
                              -1*y[d3]*costheta-x[d3]*sintheta + wellDict['coordinateY'])
    catchmentGeometry = QgsGeometry.fromPolygon([points])
    return catchmentGeometry


def get_well_catchment(catchmentGeometry):
    '''create a layer from catchment geometry
    
    Parameters
    ----------
    catchmentGeometry : geometry object
        the well catchment geometry    
    
    Returns
    -------
    new_well_catchment : layer object
        the polygon layer for the well catchment'''
    QSettings()
    new_well_catchment = QgsVectorLayer('Polygon', "jiushiniubi" , "memory")
    feat = new_well_catchment.dataProvider()
    poly = QgsFeature()
    poly.setGeometry(catchmentGeometry)
    feat.addFeatures([poly])
    new_well_catchment.updateExtents()
    return new_well_catchment

    
    
def draw_catchment(well_catchment):
    '''register and show the layer in gis
    
    Parameters
    ----------
    well_catchment : layer object
        the well layer'''

    QSettings()
    QgsMapLayerRegistry.instance().addMapLayers([well_catchment])
    well_catchment.updateExtents()
    QgsMapLayerRegistry.instance().addMapLayer(well_catchment)
    iface.mapCanvas().refresh()

#################################################


def get_recharge_from_element(catchment_geometry, check_wert):
    '''cycles through all features of the EFL-layer and computes the total GWR
    of of all EFL that are within the catchment
    
    Parameters
    ----------
    catchment_geometry : geometry object
        geometry of the well catchment
    check_wert : list
        multipliers (0 or 1) for the flow components.
        The order is cruical (RG2, RG1, Rdrain, RH)!
    
    Returns
    -------
    gwr_catchment : float
        the mean ground water recharge of the well catchemnt'''

    QSettings()
    efl_layer = get_layer(SETUP.EFLLAYER)
    id_rg2_efl = efl_layer.fieldNameIndex( "RG2" )
    id_rg1_efl = efl_layer.fieldNameIndex( "RG1" )
    id_drain_efl = efl_layer.fieldNameIndex( "DRAIN" )
    id_rh_efl = efl_layer.fieldNameIndex( "RH" )

    area_total = 0
    gwr_temp = 0
    cands = efl_layer.getFeatures(QgsFeatureRequest().setFilterRect(catchment_geometry.boundingBox()))

    for area_feature in cands:
        if area_feature.geometry().intersects(catchment_geometry):
            ueber = area_feature.geometry().intersection(catchment_geometry)
            area_inter = ueber.area()
            area_total += area_inter
            tempRH = area_feature[id_rh_efl] * area_inter if check_wert[3] else 0
            tempRD = area_feature[id_drain_efl] * area_inter if check_wert[2] else 0
            tempRG1 = area_feature[id_rg1_efl] * area_inter if check_wert[1] else 0
            tempRG2 = area_feature[id_rg2_efl] * area_inter if check_wert[0] else 0                        
            tempGWN = tempRH + tempRD + tempRG1 + tempRG2            
            gwr_temp += tempGWN   
            
    gwr_catchment = gwr_temp / area_total
    return gwr_catchment 

def getFactorRelaxation(ipDict, iteration):
    '''return the ralaxation factor
    
    Parameters
    ----------
    ipDict : dict
        the iteration specific parameters
    iteration : int
        the iteration number
    
    Returns
    -------
    tmp : float
        the relaxation factor'''
        
    if iteration > ipDict['relaxUnderIter1'] - 1:
        return ipDict['relaxF1']
    elif iteration > ipDict['relaxUnderIter2'] - 1:
            return ipDict['relaxF2']
    else:
        return 1


def Iteration (wellDict, check_wert, endpoint=[], staticL=False):
    '''iterative search for a well catchment for a given extraction rate and
    spacial distributed ground water recharge areas
    
    Parameters
    ----------
    wellDict : dict
        contains the data of the well
    check_wert : list
        multipliers (0 or 1) for the flow components.
        The order is cruical (RG2, RG1, Rdrain, RH)!
    endpoint : list, optional
        defines the endpoint for the fixed endpoint iteration.
        the list holds the X and Y value.
        the algorithm must not be empty if staticL is False
    staticL : bool
        if True, use a fixed endpoint for the well catchment
        
    Returns
    -------
    iterDict : dict
        the dictionary with the results of the last iteration'''
  
    #dialogL = qgis_toolbox.progdialog(0, 1)
        
    iterDict = {'diff_GWR': 1, 'slope': 0, 'direction': 0, 'Xk': 0, 'L': 0, 'F': 0,
                'Bmax': 0, 'endX': 0, 'endY': 0, 'Xbmax': 0, 'GWR_o': wellDict['firstGWR'],
                'GWR': wellDict['firstGWR'], 'R_o': wellDict['firstRadius'], 
                'R': wellDict['firstRadius'], 'staticL': False}  
    ipDict = get_Iteration_Parameter()
    # Anzahl der Iteration
    Iteravergleich = []
    if staticL:
        iterDict['endX'] = endpoint['coordinateEndX']
        iterDict['endY'] = endpoint['coordinateEndY']
        iterDict['L'] = ((wellDict['coordinateX'] - iterDict['endX']) ** 2 \
                    + (wellDict['coordinateY'] - iterDict['endY']) ** 2)**0.5
        iterDict['staticL'] = True

    
    for iteration in range(0, ipDict['iterMax']):
        if DEBUG: print('Iteration', iteration)
        if iterDict['diff_GWR'] > ipDict['Tol']:
            #print('iteration', iteration)
            factorRelaxation = getFactorRelaxation(ipDict, iteration)

            bufferGeometry = get_bufferGeometry(wellDict, iterDict)
        
            if not is_within_area(bufferGeometry):
                #print("Einzugsgebiet ist teilweise ausserhalb des Untersuchungsgebiets")
                    # Berunnenpunkt nicht angenwendet sein
                #iterDict['diff_GWR'] = ipDict['Tol'] - 1
                    # kleine Defferenz
                    # von Iteration weg gehen
                if DEBUG: print('NOT IN AREA', ipDict['Tol'] - 1)
            if 1:
                if not iterDict['staticL'] or iteration == 0:
                    iterDict = get_gradient_and_direction(bufferGeometry, iterDict)
                
                iterDict = get_parameter_of_well_catchment(wellDict, iterDict)
                catchmentGeometry = get_polygon_of_well_catchment(iterDict, wellDict)
                
                iterDict['GWR'] = get_recharge_from_element(catchmentGeometry, check_wert)
                #print("gwr_catchment_ {}".format(iterDict['GWR']))

                if iterDict['GWR'] <= 0:
                    iterDict['GWR'] = iterDict['GWR_o'] * ipDict['factorP']
                    vergleichfaktor = -1
                        # kennenwert fuer Beurteilung:
                        # Wenn GWN negativ ist, vergleichfaktor = -1
                    iterDict['diff_GWR'] = 5
                        # eine grosse Differenz                                                
                else:
                    vergleichfaktor = 1
                    iterDict['GWR'] = factorRelaxation * (iterDict['GWR'] - iterDict['GWR_o']) + iterDict['GWR_o']
                    iterDict['diff_GWR'] = (abs(iterDict['GWR'] - iterDict['GWR_o'])/iterDict['GWR'])/factorRelaxation
                    #iterDict['diff_GWR'] = (abs(iterDict['GWR'] - wellDict['Q'])/iterDict['GWR'])/factorRelaxation
                if DEBUG: print("diff_GWN {} / recharge_value {} / iterDict['GWR'] {} / Q {} / Area {}".format(iterDict['diff_GWR'], 
                                                                                               iterDict['GWR_o'], 
                                                                                               iterDict['GWR'], 
                                                                                               wellDict['Q'],
                                                                                               catchmentGeometry.area()))
                
                vergleich= list(range(3)) 
                vergleich[0] = iterDict['GWR_o']
                vergleich[1] = vergleichfaktor
                vergleich[2] = iterDict['diff_GWR']
                #print(vergleich)
                Iteravergleich.append(vergleich)
                    # Dokument der Proyesse der Iteration

                iterDict['R'] = (1000000 * wellDict['Q'] / iterDict['GWR'] /math.pi)**0.5

                iterDict['GWR_o'] = iterDict['GWR']
                iterDict['R_o'] = iterDict['R']
        else:
            break

            
    if iterDict['diff_GWR'] > ipDict['Tol'] :
        print('PROBLEM diffGWR > TOL', iterDict['diff_GWR'], ipDict['Tol'])
        #  wenn Iterationszahl > Max. Iterationszahl und Diff > Toleranz
        iterDict['diff_GWR'] = 100
            # Anfangszahl
        for diff in Iteravergleich:
            # Vergleich von ein Schritt zu ein Schritt
            if diff[1] == 1:
                # nur fuer positive GWN
                if diff[2] < iterDict['diff_GWR']:
                    # Suchung der kleinsten Differenz
                    iterDict['diff_GWR'] = diff[2]
                        # Suchung der kleinsten Differenz
                    iterDict['GWR'] =diff[0]
                    iterDict['R'] = (1000000 * wellDict['Q'] / iterDict['GWR'] /math.pi)**0.5

    #print("itration finished")
    #print(","*20)
    #iterDict.update({'catchment': catchmentGeometry})
    return iterDict


def make_catchmentLayer(iterDict, wellDict, wellNr, endpoint=[0, 0]):
    ''' return a layer with the already calculated well catchment

    Parameters
    ----------
    wellDict : dict
        contains the informations about the well
    iterDict : dict
        contains the information about parameters at the current iteration
    wellNr : int
        the counter for the created well layers
    Returns
    -------
    new_well_catchment : layer object
        the well catchment layer'''
        
    new_well_catchment = QgsVectorLayer('Polygon', 
                                        "Brunneneinzugsgebiet" + str(wellNr), #
                                        "memory")
    feat = new_well_catchment.dataProvider()
    poly = QgsFeature()
    tmp = [QgsField("ID", QVariant.String),\
           QgsField("RECHTSWERT",  QVariant.Double),\
           QgsField("HOCHWERT", QVariant.Double),\
           QgsField("GWN (mm)", QVariant.Double),\
           QgsField("JAHR_TM3", QVariant.Double)]
    if endpoint:
        tmp +=[QgsField("Endpunkt_X", QVariant.Double),\
               QgsField("Endpunkt_Y", QVariant.Double)]
    feat.addAttributes(tmp)    
    new_well_catchment.updateFields()
    

    poly.setAttributes([wellDict['ID'], wellDict['coordinateX'], wellDict['coordinateY'],
                        iterDict['GWR'], wellDict['Q'], endpoint[0], endpoint[1]])

    
    ### new geometry
    catchmentGeometry = get_polygon_of_well_catchment(iterDict, wellDict)
    poly.setGeometry(catchmentGeometry)
    feat.addFeatures([poly])
    new_well_catchment.updateExtents()
    return new_well_catchment


def gwrSelection(attrs, idxDict, gwrComponents):
 
    '''look for not defined values in the attribute table
    and set them zero. Further, select only needed components to compute the
    recharge
 
    Parameters
    ----------
    attrs : attributes
        attributes of the layer
    idxDict : dict
        holds the indizes for the components
    gwrComponents : list
        contains zeros or ones to sum up only needed components
        odering is important (RG2, RG1, Rdrain, Rh)!
    Returns
    -------
    gwr : float
        the ground water recharge rate'''

    if attrs[idxDict['DRAIN']] == NULL:
        drain = 0
    else:
        drain = idxDict['DRAIN']
    if attrs[idxDict['RG2']] == NULL:
        rg2 = 0
    else:
        rg2 = attrs[idxDict['RG2']]
    if attrs[idxDict['RG1']] == NULL:
        rg1 = 0
    else:
        rg1 = attrs[idxDict['RG1']]
    if attrs[idxDict['RH']] == NULL:
        rh = 0
    else:
        rh = attrs[idxDict['RH']]
    gwr = rg1*gwrComponents[1] + rg2*gwrComponents[0] \
          + drain*gwrComponents[2] + rh*gwrComponents[3]
    return gwr
 
    

def first_estimate(areaSAFeatures, wellFeatures, tgFeatures, 
                   idxDict, newWellDict, gwrComponents, other): 
    '''first guess of well catchment based on TG layer
    
    Parameters
    ----------
    areaSAFeatures : features object
        federal state area
    wellFeatures : features object
        layer for the new well
    tgFeatures : features object
        sub catchments
    newWellDict : dict
        configuration of the new well
    idxDict : dict
        indices of attributes
    gwrComponents : list
        selected fluxes for groundwater recharge
    other : float
        additional balance components     
    Returns
    -------
    newWellDict : dict
        the updated dictionary'''
 
    iface.messageBar().pushMessage(u"Erstschätzung für Brunneneinzugsgebiet...",
                    level=QgsMessageBar.INFO, duration=3)
 
    ### should be one feature
    for wc, sWellFeature in enumerate(wellFeatures):
        wellGeometry = sWellFeature.geometry()
        ### should be one feature
        for sc, areaSAFeature in enumerate(areaSAFeatures):
            wellAttribute = sWellFeature.attributes()
            Q = float(wellAttribute[idxDict['JAHR_TM3']]) + other
            R = 0
            GWN = 0
            
            if wellGeometry.intersects(areaSAFeature.geometry()):
                for tgFeat in tgFeatures:
                    attrs = tgFeat.attributes()
                    
                    if wellGeometry.intersects(tgFeat.geometry()):
                        GWN = gwrSelection(attrs, idxDict, gwrComponents)
                        GWN = float(GWN)
 
                        if GWN > 0:
                            break
            if GWN <= 0:
                GWN = 10

        R = (Q/GWN/3.1415926)**0.5
        try:
            newWellDict.update({'feasible': True,
                            'firstRadius': R,
                            'firstGWR': GWN,
                            'Q': Q,
                            'TGID': attrs[idxDict['TGID']]
                            })
        except UnboundLocalError:
            iface.messageBar().pushMessage(u"Fehler! Liegt Brunnen innerhalb des Gebiets?",
                        level=QgsMessageBar.WARNING, duration=3)
            return
 
        iface.messageBar().pushMessage(u"... beendet",
                        level=QgsMessageBar.INFO, duration=3)
 
        return newWellDict
 
    return newWellDict.update({'feasible': False})    


def get_idxDictTgEfl(layer, idxDict={}, what='TGID'):
    '''prepare/update a dictionary for the new well with
    certain indices from the sub catchment layer
    
    Parameters
    ----------
    layer : layer object
        the well catchment layer
    idxDict : dict
        this dictionary is updated
    what : get index also for this field, also used as key in idxDict'''
 
    idxDict.update({'RG1': layer.fieldNameIndex("RG1"),
                    'RG2': layer.fieldNameIndex("RG2"),
                    'SICK': layer.fieldNameIndex("SICK"),
                    'DRAIN': layer.fieldNameIndex("DRAIN"),
                    'RH': layer.fieldNameIndex("RH"),
                    what: layer.fieldNameIndex(what)})
    return idxDict
 

def get_idxDictWell(layer, idxDict={}):
 
    '''prepare/update a dictionary for the new well with
    certain indices from the well layer
 
    Parameters
    ----------
    idxDict : dict
        the indizes of parameters in attribute table to update

    Returns
    -------
    idxDict : dict
        the updated dictionary'''
 
    idxDict.update({'coordinateY': layer.fieldNameIndex('coordinateY'),
                    'coordinateX': layer.fieldNameIndex('coordinateX'),
                    'JAHR_TM3': layer.fieldNameIndex("JAHR_TM3"),
                    'wellTG': layer.fieldNameIndex("TG"),
                    'wellID': layer.fieldNameIndex("ID")})
    return idxDict
 

 
def get_QL(layer, idxDict={}):
    '''prepare/update a dictionary for the new well with
    certain indices from the sub catchment layer
    
    Parameters
    ----------
    layer : layer object
        the well catchment layer
    idxDict : dict
        this dictionary is updated
    what : get index also for this field, also used as key in idxDict'''
 
    idxDict.update({'QL_org': layer.fieldNameIndex("QL_SPENDE"),
                    'QL_eff': layer.fieldNameIndex("QL_EFF_SPE")})
    return idxDict
 

def create_wellPointLayer( newWellDict, crsID=None, name='neuer_Brunnen', geo3='JAHR_TM3'):
    '''create a layer for the new well'''
    newWell = QgsVectorLayer('Point', name, "memory")
 
    if not crsID is None:
        newWell.setCrs(QgsCoordinateReferenceSystem(crsID))
 
    feat = newWell.dataProvider()
    # Erzeugung der Attribute-Tabelle
    feat.addAttributes([QgsField("ID", QVariant.String),
                        QgsField('coordinateX',  QVariant.Int),
                        QgsField('coordinateY', QVariant.Int),
                        QgsField(geo3, QVariant.Double)]
                      )
    newWell.updateFields()
    newPoint = QgsPoint(newWellDict['coordinateX'], newWellDict['coordinateY'])
    
    poly = QgsFeature()
    poly.setAttributes([newWellDict['ID'], 
                        newWellDict['coordinateX'], 
                        newWellDict['coordinateY'], 
                        newWellDict[geo3]])
    poly.setGeometry(QgsGeometry.fromPoint(newPoint))
    feat.addFeatures([poly])
    feat.updateExtents()
    return newWell
 







