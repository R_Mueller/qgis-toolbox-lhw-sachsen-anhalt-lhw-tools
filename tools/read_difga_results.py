# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Read the results of DIFGA2000 and populate an layer.

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""

from qgis.core import QgsVectorLayer, QgsField, QgsMapLayerRegistry  
from PyQt4.QtCore import QVariant
import os
#from qgis.core import QgsFeatureRequest
#from qgis.core import QgsMapLayerRegistry

### PATH TO SETUP
PATHS = "c:\\Users\\Ruben.Mueller\\.qgis2\\python\\plugins\\LHW_Tools\\setup"
### PATH TO THE RESULT FILES
PATHD = "M:\\Roters\\DIFGA\\DIFGA_Marie\\DIFGA_mit_Niederschlagsdargebot\\ausgabe" #"C:\\Users\\Ruben.Mueller\\Documents\\DIFGA\\Ausgabe"
#PATHP = 'D:\\SA_Modell\\GIS'
DECODER= 'ISO-8859-1'
REPLACE = ['monat_mitt', 'jahr_mitt']
ST_NAME = 'STATIONSNA'
PEGELLAYER = 'pegel_2018'
X = 'RECHTSWERT' 
Y = 'HOCHWERT'
AREA = 'EZG'
EZG = 'GEWAESSER'

with open(os.path.join(PATHS, 'Difga_Parameter.txt')) as fid:
    tmp = fid.readlines()[1:]
    parameter = [i.strip() for i in tmp]

class Setup:
    def __init__(self):
        self.CRS = 25832

SETUP = Setup()

name = 'difga_pegel'
mem_layer = QgsVectorLayer("Point?crs=epsg:"+ str(SETUP.CRS), name, "memory")
mem_layer_data = mem_layer.dataProvider()
#print('sl', layer.name())
par_s = [QgsField(i+'_'+w, QVariant.Double) for w in ('w', 's', 'y') for i in parameter]
par_m = [QgsField(i+'_'+str(m), QVariant.Double) for m in range(1,13) for i in parameter]
par_all = [QgsField(ST_NAME, QVariant.String),
           QgsField('X', QVariant.Double), 
           QgsField('Y', QVariant.Double),
           QgsField('MNQ', QVariant.Double),
           QgsField('MN', QVariant.Double),
           QgsField('EZG', QVariant.String),
           QgsField('Area', QVariant.Double),
           QgsField('Periode', QVariant.String)] + par_s + par_m

mem_layer_data.addAttributes(par_all)
mem_layer.updateFields()


mem_layer.startEditing()

fields = mem_layer.fields()

if 1:
    player = QgsMapLayerRegistry.instance().mapLayersByName(PEGELLAYER)[0]
    with open(os.path.join(PATHS, 'Difga_Zuordnung.txt'), 'rb') as fid:
        for i, tmp in enumerate(fid):
            if i:
                ### relation[0] is name; relation[1] is file
                relation = [i.strip().strip(';').decode('UTF-8') for i in tmp.split(';')]
                
                for feat in player.getFeatures():
                    if feat.attributes()[player.fieldNameIndex(ST_NAME)] == relation[0]:
                        print(1)
                ### read X and Y from player
                #rtext = u'"' + ST_NAME + u'" = "'+ relation[0] + '"'
                #print(rtext)
                #request = QgsFeatureRequest().setFilterExpression(rtext)
                #for it in player.getFeatures(request):
                        x = feat.attributes()[player.fieldNameIndex(X)]
                        y = feat.attributes()[player.fieldNameIndex(Y)]
                        area = feat.attributes()[player.fieldNameIndex(AREA)]
                        ezg = feat.attributes()[player.fieldNameIndex(EZG)]
                        print(x,y)
                        feature = QgsFeature()
                        feature.setFields(fields)
                        layerPoint = QgsPoint(x, y)
                        feature.setAttributes(par_all)
                        feature.setGeometry(QgsGeometry.fromPoint(layerPoint))
                        #feature.setAttributes([relation[0], x, y])
                        feature.setAttribute(ST_NAME, relation[0])
                        feature.setAttribute('X', x)
                        feature.setAttribute('Y', y)
                        feature.setAttribute('Area', y)
                        feature.setAttribute('EZG', ezg)
                        mem_layer_data.addFeatures([feature])
                        
                        mem_layer.updateFields()
                        #mem_layer.commitChanges()
                        mem_layer_data.updateExtents()

mem_layer.commitChanges()

if 1:
    mem_layer.startEditing()
    with open(os.path.join(PATHS, 'Difga_Zuordnung.txt'), 'rb') as fid: 
        for i, tmp in enumerate(fid):
            ### not the header
            if i:
                ### relation[0] is name; relation[1] is file
                relation = [i.strip().strip(';').decode('UTF-8') for i in tmp.split(';')]
                print(relation)
                for feat in mem_layer.getFeatures():
                    ### look through the memlayer
                    if feat.attributes()[player.fieldNameIndex(ST_NAME)] == relation[0]:
                        idf = feat.id()
                        #print('id', idf)
                        ### first months then years
                        for repl in REPLACE:
                            #print(relation, repl)
                            ### load the DBFs
                            dname = relation[1].replace('XXX', repl)
                            #print(dname)
                            layer = QgsVectorLayer(os.path.join(PATHD, dname), dname, "ogr")
                            #QgsMapLayerRegistry.instance().addMapLayer(layer)
                            
                            
                            if dname.find('monat') > -1:
                                ### loop though the months and then pars 
                                for monat, featL in enumerate(layer.getFeatures()):
                                    for j, par in enumerate(parameter):
                                        parname = par + '_' + str(monat+1)
                                        colnum = mem_layer.fieldNameIndex(parname)
                                        idpar = layer.fieldNameIndex(par)
                                        value = featL.attributes()[idpar]
                                        a = feature.setAttribute(parname, value)
                                        #print('-->', a)
                                        mem_layer.changeAttributeValue(idf, colnum, value)
                            else:
                                for j, featL in enumerate(layer.getFeatures()):
                                    for k, par in enumerate(parameter):
                                        if featL.attributes()[0] == 'Winterhalbjahressumme':
                                            what = 'w'# ('w', 's', 'y')[j]
                                        elif featL.attributes()[0] == 'Sommerhalbjahressumme':
                                            what = 's'
                                        else:
                                            what = 'y'
                                        parname = par + '_' + what
                                        colnum = mem_layer.fieldNameIndex(parname)
                                        idpar = layer.fieldNameIndex(par)
                                        value = featL.attributes()[idpar]
                                        feature.setAttribute(parname, value)
                                        mem_layer.changeAttributeValue(idf, colnum, value)
mem_layer_data.updateExtents()
mem_layer.commitChanges()
QgsMapLayerRegistry.instance().addMapLayer(mem_layer)