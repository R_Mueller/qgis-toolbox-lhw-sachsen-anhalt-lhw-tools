# -*- coding: utf-8 -*-
"""
    This File
    ---------
    Dialog for all the supplementary data.

    Licence
    -------
    This file is part of the LHW Toolbox. A plugin for QGIS.

    Copyright (c) 2018 by

    Büro für Angewandte Hydrologie (BAH Berlin),
    http://www.bah-berlin.de
 
    LHW Toolbox is free software: you can redistribute it and/or modify it under the terms
    of the GNU Lesser General Public License (LGPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later version.

    LHW Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
    General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with the LHW Toolbox.  If not, see <http://www.gnu.org/licenses/>.

    Author:
    Ruben Müller
    ruben.mueller@bah-berlin.de
"""

from datetime import datetime
from PyQt4.QtGui import QDialog
from PyQt4.QtCore import Qt
from PyQt4.QtCore import QDate
from qgis.gui import QgsMessageBar
from qgis.core import NULL
#from .setup import setup__ as SETUP
from pyqtDialogs.dialog_RO_additionalData import Ui_Dialog_RO_additionalData
from .tools.qgis_toolbox import checkForFloat
from .setup import setup__ as SETUP

class Start_Dialog_RO_additionalData(QDialog, Ui_Dialog_RO_additionalData):
    '''class for the  dialog for additiional data'''

    def __init__(self, parent=None,
                 userDict={}, userDictOld={}, userDB=None):
        QDialog.__init__(self, parent.mainWindow())
        self.setupUi(self)
        self.ADD_FIELDS = {'lkr': self.comboBox_lkr,
                           'gemeinde': self.comboBox_gemeinde,
                           'owk_st': self.comboBox_okw_st,
                           'gwk_st': self.comboBox_gkw_st,
                           'gw_schutz': self.comboBox_gw_schutz,
                           'ezg_name': self.lineEdit_ezg_name,
                           'bilanzgebiet': self.lineEdit_bilanzgebiet,
                           'b_qm_h': self.lineEdit_b_qm_h,
                           'b_qm_d': self.lineEdit_b_qm_d,
                           #'b_qm_a': self.lineEdit_b_qm_a,
                           'BIL_JAHR_TM3': self.lineEdit_bil_tm3Jahr,
                           #'b_max_tqm_a': self.lineEdit_b_max_tqm_a,
                           'b_mit_tqm_a': self.lineEdit_b_mit_tqm_a,
                           'g_qm_h': self.lineEdit_g_qm_h,
                           'g_qm_d': self.lineEdit_g_qm_d,
                           #'g_qm_a': self.lineEdit_g_qm_a,
                           'g_mit_tqm_a': self.lineEdit_g_mit_tqm_a,
                           'g_max_tqm_a': self.lineEdit_g_max_tqm_a,
                           'bemerkungen': self.lineEdit_bemerkungen,
                           'bearb_lhw': self.lineEdit_bearb_lhw,
                           'gw_stock':  self.lineEdit_gw_stock,
                           'date_a': self.dateEdit_date_a,
                           #'date_sn': self.dateEdit_date_sn,
                           'date_b': self.dateEdit_date_b,
                           'nutzart': self.lineEdit_nutzart,
                           'nutzzweck': self.lineEdit_nutzzweck,
                           'gewaesser': self.lineEdit_gewaesser,
                           'gemark': self.lineEdit_gemark,
                           'flur': self.lineEdit_flur,
                           'flurst': self.lineEdit_flurst,
                           'bescheid': self.lineEdit_bescheid,
                           'befr_zeitr': self.dateEdit_befr_zeitr,
                           'bereg_flaech': self.lineEdit_bereg_flaech,
                           'pegel': self.lineEdit_pegel,
                           'einstellwert': self.lineEdit_einstellwert}


        self.iface = parent
        self.userDict = userDict
        self.userDictOld = userDictOld
        self.db = userDB
        self.pushButton_accept.clicked.connect(self.acceptP)
        self.pushButton_reject.clicked.connect(self.rejectP)

        self.checkBox_befr_zeitr.stateChanged.connect(self.set_befr_zeitr)


    def set_befr_zeitr(self):
        if self.checkBox_befr_zeitr.isChecked():
            self.dateEdit_befr_zeitr.setEnabled(True)
        else:
            self.dateEdit_befr_zeitr.setEnabled(False)

    def populate(self):
        for field in self.ADD_FIELDS.items():

            ### combobox
            if field[0] in ('lkr', 'gemeinde', 'owk_st', 'gwk_st', 'gw_schutz'):
                self.setCB(field)

            ### dateEdit
            elif field[0] in ('date_a', 'date_sn', 'date_b', 'befr_zeitr'):
                tmp = self.userDict[field[0]]
                if not isinstance(tmp, QDate):
                    tmp = datetime.strftime(tmp, '%Y-%m-%d')
                    tmp = QDate.fromString(tmp, 'yyyy-MM-dd')
                field[1].setDate(tmp)

            elif field[0] in ('befr_zeitr'):
                if self.userDict['befr_zeitr'] == NULL:
                    self.dateEdit_befr_zeitr.setEnabled(False)
                else:
                    self.dateEdit_befr_zeitr.setEnabled(True)
                    tmp = self.userDict[field[0]]
                    if not isinstance(tmp, QDate):
                        tmp = datetime.strftime(tmp, '%Y-%m-%d')
                        tmp = QDate.fromString(tmp, 'yyyy-MM-dd')
                field[1].setDate(tmp)

            ### lineEdit
            else:
                if isinstance(self.userDict[field[0]], (str, unicode)):
                    #print(self.userDict[field[0]], type(self.userDict[field[0]]))
                    tmp = self.userDict[field[0]].encode(SETUP.ENCODING)
                else:
                    #print(self.userDict[field[0]], type(self.userDict[field[0]]))
                    tmp = str(self.userDict[field[0]])
                field[1].setText(tmp)


    def setCB(self, field):
        text = self.userDict[field[0]]
        index = field[1].findText(text, Qt.MatchFixedString)
        if index >= 0:
             field[1].setCurrentIndex(index)


    def acceptP(self):
        for field in self.ADD_FIELDS.items():
            ### combobox

            if field[0] in ('lkr', 'gemeinde', 'owk_st', 'gwk_st', 'gwk_st', 'gw_schutz'):
                selection = field[1].currentText()

            ### dateEdit
            elif field[0] in ('date_a', 'date_sn', 'date_b', 'befr_zeitr'):
                selection = field[1].date().toString('yyyy-MM-dd')
                #print(selection, type(selection))
                selection = datetime.strptime(selection, '%Y-%m-%d')

            elif field[0] in ('befr_zeitr'):
                if not self.checkBox_befr_zeitr.isChecked():
                    selection = NULL
                else:
                    selection = field[1].date().toString('yyyy-MM-dd')
                    #print(selection, type(selection))
                    selection = datetime.strptime(selection, '%Y-%m-%d')

            ### lineEdit
            elif field[0] in ('b_qm_h', 'b_qm_d', 'b_mit_tqm_a', #'b_max_tqm_a',
                              'g_qm_h', 'g_qm_d', 'g_mit_tqm_a', 'g_max_tqm_a',
                              'einstellwert', 'bereg_flaech'):
                tmp = field[1].text().strip()
                if not checkForFloat(tmp):
                    self.iface.messageBar().pushMessage('Keine gültige Eingabe: {}'.format(tmp),
                                                        level=QgsMessageBar.INFO, duration=3)
                    return
                else:
                    selection = tmp
            else:
                selection = field[1].text().strip()

            ### store in userDict
            self.userDict[field[0]] = selection

        self.close()


    def rejectP(self):
        self.close()

    def closePlugin(self):
        self.close()

    def closeEvent(self, event):
       event.accept()