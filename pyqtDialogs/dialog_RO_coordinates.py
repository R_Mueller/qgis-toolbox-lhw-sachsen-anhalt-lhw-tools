# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_RO_coordinates.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog_RO_coordinates(object):
    def setupUi(self, Dialog_RO_coordinates):
        Dialog_RO_coordinates.setObjectName(_fromUtf8("Dialog_RO_coordinates"))
        Dialog_RO_coordinates.resize(261, 106)
        self.gridLayout_2 = QtGui.QGridLayout(Dialog_RO_coordinates)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label = QtGui.QLabel(Dialog_RO_coordinates)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.lineEdit = QtGui.QLineEdit(Dialog_RO_coordinates)
        self.lineEdit.setMinimumSize(QtCore.QSize(0, 24))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.gridLayout.addWidget(self.lineEdit, 0, 1, 1, 1)
        self.label_2 = QtGui.QLabel(Dialog_RO_coordinates)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.lineEdit_2 = QtGui.QLineEdit(Dialog_RO_coordinates)
        self.lineEdit_2.setMinimumSize(QtCore.QSize(0, 24))
        self.lineEdit_2.setObjectName(_fromUtf8("lineEdit_2"))
        self.gridLayout.addWidget(self.lineEdit_2, 1, 1, 1, 1)
        self.pushButtonCancel = QtGui.QPushButton(Dialog_RO_coordinates)
        self.pushButtonCancel.setMinimumSize(QtCore.QSize(100, 24))
        self.pushButtonCancel.setObjectName(_fromUtf8("pushButtonCancel"))
        self.gridLayout.addWidget(self.pushButtonCancel, 2, 0, 1, 1)
        self.pushButton = QtGui.QPushButton(Dialog_RO_coordinates)
        self.pushButton.setMinimumSize(QtCore.QSize(0, 24))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.gridLayout.addWidget(self.pushButton, 2, 1, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)

        self.retranslateUi(Dialog_RO_coordinates)
        QtCore.QMetaObject.connectSlotsByName(Dialog_RO_coordinates)

    def retranslateUi(self, Dialog_RO_coordinates):
        Dialog_RO_coordinates.setWindowTitle(_translate("Dialog_RO_coordinates", "Koordinatenwahl", None))
        self.label.setText(_translate("Dialog_RO_coordinates", "RW (LS89 Zone32)", None))
        self.label_2.setText(_translate("Dialog_RO_coordinates", "HW (LS89 Zone32)", None))
        self.pushButtonCancel.setText(_translate("Dialog_RO_coordinates", "Abbrechen", None))
        self.pushButton.setText(_translate("Dialog_RO_coordinates", "Übernehmen", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog_RO_coordinates = QtGui.QDialog()
    ui = Ui_Dialog_RO_coordinates()
    ui.setupUi(Dialog_RO_coordinates)
    Dialog_RO_coordinates.show()
    sys.exit(app.exec_())

