# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_RO_defineEndpoint.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog_RO_defineEndpoint(object):
    def setupUi(self, Dialog_RO_defineEndpoint):
        Dialog_RO_defineEndpoint.setObjectName(_fromUtf8("Dialog_RO_defineEndpoint"))
        Dialog_RO_defineEndpoint.resize(599, 207)
        self.gridLayout_3 = QtGui.QGridLayout(Dialog_RO_defineEndpoint)
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.gridLayout_2 = QtGui.QGridLayout()
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.label = QtGui.QLabel(Dialog_RO_defineEndpoint)
        self.label.setMinimumSize(QtCore.QSize(0, 24))
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout_2.addWidget(self.label, 0, 0, 1, 1)
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.lineEditWest = QtGui.QLineEdit(Dialog_RO_defineEndpoint)
        self.lineEditWest.setObjectName(_fromUtf8("lineEditWest"))
        self.gridLayout.addWidget(self.lineEditWest, 1, 1, 1, 1)
        self.label_3 = QtGui.QLabel(Dialog_RO_defineEndpoint)
        self.label_3.setMinimumSize(QtCore.QSize(200, 24))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.gridLayout.addWidget(self.label_3, 0, 2, 1, 1)
        self.label_5 = QtGui.QLabel(Dialog_RO_defineEndpoint)
        self.label_5.setMinimumSize(QtCore.QSize(0, 24))
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.gridLayout.addWidget(self.label_5, 2, 0, 1, 1)
        self.label_2 = QtGui.QLabel(Dialog_RO_defineEndpoint)
        self.label_2.setMinimumSize(QtCore.QSize(200, 24))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 2)
        self.lineEditNord = QtGui.QLineEdit(Dialog_RO_defineEndpoint)
        self.lineEditNord.setObjectName(_fromUtf8("lineEditNord"))
        self.gridLayout.addWidget(self.lineEditNord, 2, 1, 1, 1)
        self.label_4 = QtGui.QLabel(Dialog_RO_defineEndpoint)
        self.label_4.setMinimumSize(QtCore.QSize(40, 24))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.gridLayout.addWidget(self.label_4, 1, 0, 1, 1)
        self.pushButtonCoord = QtGui.QPushButton(Dialog_RO_defineEndpoint)
        self.pushButtonCoord.setMinimumSize(QtCore.QSize(0, 24))
        self.pushButtonCoord.setObjectName(_fromUtf8("pushButtonCoord"))
        self.gridLayout.addWidget(self.pushButtonCoord, 3, 1, 1, 1)
        self.pushButtonActive = QtGui.QPushButton(Dialog_RO_defineEndpoint)
        self.pushButtonActive.setMinimumSize(QtCore.QSize(0, 24))
        self.pushButtonActive.setObjectName(_fromUtf8("pushButtonActive"))
        self.gridLayout.addWidget(self.pushButtonActive, 3, 2, 1, 1)
        self.pushButtonMouse = QtGui.QPushButton(Dialog_RO_defineEndpoint)
        self.pushButtonMouse.setObjectName(_fromUtf8("pushButtonMouse"))
        self.gridLayout.addWidget(self.pushButtonMouse, 3, 3, 1, 1)
        self.label_7 = QtGui.QLabel(Dialog_RO_defineEndpoint)
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.gridLayout.addWidget(self.label_7, 0, 3, 1, 1)
        self.lineEdit_xa = QtGui.QLineEdit(Dialog_RO_defineEndpoint)
        self.lineEdit_xa.setEnabled(False)
        self.lineEdit_xa.setObjectName(_fromUtf8("lineEdit_xa"))
        self.gridLayout.addWidget(self.lineEdit_xa, 1, 2, 1, 1)
        self.lineEdit_ya = QtGui.QLineEdit(Dialog_RO_defineEndpoint)
        self.lineEdit_ya.setEnabled(False)
        self.lineEdit_ya.setObjectName(_fromUtf8("lineEdit_ya"))
        self.gridLayout.addWidget(self.lineEdit_ya, 2, 2, 1, 1)
        self.lineEdit_xc = QtGui.QLineEdit(Dialog_RO_defineEndpoint)
        self.lineEdit_xc.setEnabled(False)
        self.lineEdit_xc.setObjectName(_fromUtf8("lineEdit_xc"))
        self.gridLayout.addWidget(self.lineEdit_xc, 1, 3, 1, 1)
        self.lineEdit_yc = QtGui.QLineEdit(Dialog_RO_defineEndpoint)
        self.lineEdit_yc.setEnabled(False)
        self.lineEdit_yc.setObjectName(_fromUtf8("lineEdit_yc"))
        self.gridLayout.addWidget(self.lineEdit_yc, 2, 3, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 1, 0, 1, 1)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.pushButtonDismiss = QtGui.QPushButton(Dialog_RO_defineEndpoint)
        self.pushButtonDismiss.setMinimumSize(QtCore.QSize(100, 0))
        self.pushButtonDismiss.setObjectName(_fromUtf8("pushButtonDismiss"))
        self.horizontalLayout_2.addWidget(self.pushButtonDismiss)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.pushButtonAccept = QtGui.QPushButton(Dialog_RO_defineEndpoint)
        self.pushButtonAccept.setMinimumSize(QtCore.QSize(220, 24))
        self.pushButtonAccept.setObjectName(_fromUtf8("pushButtonAccept"))
        self.horizontalLayout_2.addWidget(self.pushButtonAccept)
        self.gridLayout_2.addLayout(self.horizontalLayout_2, 3, 0, 1, 1)
        self.line = QtGui.QFrame(Dialog_RO_defineEndpoint)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.gridLayout_2.addWidget(self.line, 2, 0, 1, 1)
        self.gridLayout_3.addLayout(self.gridLayout_2, 0, 0, 1, 1)

        self.retranslateUi(Dialog_RO_defineEndpoint)
        QtCore.QMetaObject.connectSlotsByName(Dialog_RO_defineEndpoint)

    def retranslateUi(self, Dialog_RO_defineEndpoint):
        Dialog_RO_defineEndpoint.setWindowTitle(_translate("Dialog_RO_defineEndpoint", "Festlegung eines Endpunkts", None))
        self.label.setText(_translate("Dialog_RO_defineEndpoint", "Auwahl zwischen zwei Methoden treffen um Endpunkt zu definieren", None))
        self.label_3.setText(_translate("Dialog_RO_defineEndpoint", "b) Aktiven Layer zuweisen", None))
        self.label_5.setText(_translate("Dialog_RO_defineEndpoint", "HW (LS89 Zone32)", None))
        self.label_2.setText(_translate("Dialog_RO_defineEndpoint", "a) Koordinaten bereitstellen", None))
        self.label_4.setText(_translate("Dialog_RO_defineEndpoint", "RW (LS89 Zone32)", None))
        self.pushButtonCoord.setText(_translate("Dialog_RO_defineEndpoint", "Zuweisen", None))
        self.pushButtonActive.setText(_translate("Dialog_RO_defineEndpoint", "Zuweisen", None))
        self.pushButtonMouse.setText(_translate("Dialog_RO_defineEndpoint", "Zuweisen", None))
        self.label_7.setText(_translate("Dialog_RO_defineEndpoint", "c) Koordinatenauswahl ", None))
        self.pushButtonDismiss.setText(_translate("Dialog_RO_defineEndpoint", "Abbrechen", None))
        self.pushButtonAccept.setText(_translate("Dialog_RO_defineEndpoint", "Übernehmen und Rechnung beginnen", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog_RO_defineEndpoint = QtGui.QDialog()
    ui = Ui_Dialog_RO_defineEndpoint()
    ui.setupUi(Dialog_RO_defineEndpoint)
    Dialog_RO_defineEndpoint.show()
    sys.exit(app.exec_())

