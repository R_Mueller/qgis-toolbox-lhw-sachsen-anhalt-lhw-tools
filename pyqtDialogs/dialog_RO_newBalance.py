# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_RO_newBalance.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog_RO_newBalance(object):
    def setupUi(self, Dialog_RO_newBalance):
        Dialog_RO_newBalance.setObjectName(_fromUtf8("Dialog_RO_newBalance"))
        Dialog_RO_newBalance.resize(331, 418)
        self.gridLayout_2 = QtGui.QGridLayout(Dialog_RO_newBalance)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label = QtGui.QLabel(Dialog_RO_newBalance)
        self.label.setMinimumSize(QtCore.QSize(0, 24))
        font = QtGui.QFont()
        font.setBold(False)
        font.setUnderline(True)
        font.setWeight(50)
        self.label.setFont(font)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.tableWidget = QtGui.QTableWidget(Dialog_RO_newBalance)
        self.tableWidget.setMaximumSize(QtCore.QSize(300, 16777215))
        self.tableWidget.setObjectName(_fromUtf8("tableWidget"))
        self.tableWidget.setColumnCount(2)
        self.tableWidget.setRowCount(10)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(3, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(4, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(5, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(6, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(7, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(8, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(9, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)
        self.gridLayout.addWidget(self.tableWidget, 1, 0, 1, 1)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.pushButtonReject = QtGui.QPushButton(Dialog_RO_newBalance)
        self.pushButtonReject.setMinimumSize(QtCore.QSize(0, 24))
        self.pushButtonReject.setObjectName(_fromUtf8("pushButtonReject"))
        self.horizontalLayout_2.addWidget(self.pushButtonReject)
        self.pushButtonAccept = QtGui.QPushButton(Dialog_RO_newBalance)
        self.pushButtonAccept.setMinimumSize(QtCore.QSize(0, 24))
        self.pushButtonAccept.setObjectName(_fromUtf8("pushButtonAccept"))
        self.horizontalLayout_2.addWidget(self.pushButtonAccept)
        self.gridLayout.addLayout(self.horizontalLayout_2, 2, 0, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)

        self.retranslateUi(Dialog_RO_newBalance)
        QtCore.QMetaObject.connectSlotsByName(Dialog_RO_newBalance)

    def retranslateUi(self, Dialog_RO_newBalance):
        Dialog_RO_newBalance.setWindowTitle(_translate("Dialog_RO_newBalance", "Zusätzliche Bilanzgrößen", None))
        self.label.setText(_translate("Dialog_RO_newBalance", "Eingabe von zusätzlichen Bilanzgrößen", None))
        item = self.tableWidget.verticalHeaderItem(0)
        item.setText(_translate("Dialog_RO_newBalance", "BG1", None))
        item = self.tableWidget.verticalHeaderItem(1)
        item.setText(_translate("Dialog_RO_newBalance", "BG2", None))
        item = self.tableWidget.verticalHeaderItem(2)
        item.setText(_translate("Dialog_RO_newBalance", "BG3", None))
        item = self.tableWidget.verticalHeaderItem(3)
        item.setText(_translate("Dialog_RO_newBalance", "BG4", None))
        item = self.tableWidget.verticalHeaderItem(4)
        item.setText(_translate("Dialog_RO_newBalance", "BG5", None))
        item = self.tableWidget.verticalHeaderItem(5)
        item.setText(_translate("Dialog_RO_newBalance", "BG6", None))
        item = self.tableWidget.verticalHeaderItem(6)
        item.setText(_translate("Dialog_RO_newBalance", "BG7", None))
        item = self.tableWidget.verticalHeaderItem(7)
        item.setText(_translate("Dialog_RO_newBalance", "BG8", None))
        item = self.tableWidget.verticalHeaderItem(8)
        item.setText(_translate("Dialog_RO_newBalance", "BG9", None))
        item = self.tableWidget.verticalHeaderItem(9)
        item.setText(_translate("Dialog_RO_newBalance", "BG10", None))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("Dialog_RO_newBalance", "Name Bilanzgröße", None))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("Dialog_RO_newBalance", "[Tm³/Jahr]", None))
        self.pushButtonReject.setText(_translate("Dialog_RO_newBalance", "Verwerfen", None))
        self.pushButtonAccept.setText(_translate("Dialog_RO_newBalance", "Übernehmen", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog_RO_newBalance = QtGui.QDialog()
    ui = Ui_Dialog_RO_newBalance()
    ui.setupUi(Dialog_RO_newBalance)
    Dialog_RO_newBalance.show()
    sys.exit(app.exec_())

