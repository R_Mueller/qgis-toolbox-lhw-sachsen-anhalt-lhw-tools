# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_RO_main.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog_RO_main(object):
    def setupUi(self, Dialog_RO_main):
        Dialog_RO_main.setObjectName(_fromUtf8("Dialog_RO_main"))
        Dialog_RO_main.resize(399, 87)
        self.gridLayout_3 = QtGui.QGridLayout(Dialog_RO_main)
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.gridLayout_2 = QtGui.QGridLayout()
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.line = QtGui.QFrame(Dialog_RO_main)
        self.line.setFrameShape(QtGui.QFrame.VLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.gridLayout.addWidget(self.line, 0, 1, 1, 1)
        self.pushButton_01 = QtGui.QPushButton(Dialog_RO_main)
        self.pushButton_01.setMinimumSize(QtCore.QSize(150, 24))
        self.pushButton_01.setObjectName(_fromUtf8("pushButton_01"))
        self.gridLayout.addWidget(self.pushButton_01, 0, 0, 1, 1)
        self.formLayout_2 = QtGui.QFormLayout()
        self.formLayout_2.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout_2.setObjectName(_fromUtf8("formLayout_2"))
        self.line_3 = QtGui.QFrame(Dialog_RO_main)
        self.line_3.setFrameShape(QtGui.QFrame.VLine)
        self.line_3.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_3.setObjectName(_fromUtf8("line_3"))
        self.formLayout_2.setWidget(0, QtGui.QFormLayout.FieldRole, self.line_3)
        self.verticalLayout_4 = QtGui.QVBoxLayout()
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))
        self.pushButton_02 = QtGui.QPushButton(Dialog_RO_main)
        self.pushButton_02.setMinimumSize(QtCore.QSize(200, 24))
        self.pushButton_02.setObjectName(_fromUtf8("pushButton_02"))
        self.verticalLayout_4.addWidget(self.pushButton_02)
        self.pushButton_03 = QtGui.QPushButton(Dialog_RO_main)
        self.pushButton_03.setMinimumSize(QtCore.QSize(200, 24))
        self.pushButton_03.setObjectName(_fromUtf8("pushButton_03"))
        self.verticalLayout_4.addWidget(self.pushButton_03)
        self.formLayout_2.setLayout(1, QtGui.QFormLayout.FieldRole, self.verticalLayout_4)
        self.gridLayout.addLayout(self.formLayout_2, 0, 2, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)
        self.gridLayout_3.addLayout(self.gridLayout_2, 0, 0, 1, 1)

        self.retranslateUi(Dialog_RO_main)
        QtCore.QMetaObject.connectSlotsByName(Dialog_RO_main)

    def retranslateUi(self, Dialog_RO_main):
        Dialog_RO_main.setWindowTitle(_translate("Dialog_RO_main", "Bearbeitung Stellungnahme", None))
        self.pushButton_01.setText(_translate("Dialog_RO_main", "Neuen Nutzer anlegen", None))
        self.pushButton_02.setText(_translate("Dialog_RO_main", "Bestehenden Nutzer auswählen", None))
        self.pushButton_03.setText(_translate("Dialog_RO_main", "Arbeitsstand auswählen", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog_RO_main = QtGui.QDialog()
    ui = Ui_Dialog_RO_main()
    ui.setupUi(Dialog_RO_main)
    Dialog_RO_main.show()
    sys.exit(app.exec_())

