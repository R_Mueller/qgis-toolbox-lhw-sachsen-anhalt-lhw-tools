# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_AE_selectRegion.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog_AE_selectRegion(object):
    def setupUi(self, Dialog_AE_selectRegion):
        Dialog_AE_selectRegion.setObjectName(_fromUtf8("Dialog_AE_selectRegion"))
        Dialog_AE_selectRegion.resize(409, 200)
        self.gridLayout_2 = QtGui.QGridLayout(Dialog_AE_selectRegion)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.formLayout = QtGui.QFormLayout()
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.label_6 = QtGui.QLabel(Dialog_AE_selectRegion)
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label_6)
        self.lineEditArea = QtGui.QLineEdit(Dialog_AE_selectRegion)
        self.lineEditArea.setMinimumSize(QtCore.QSize(0, 24))
        self.lineEditArea.setObjectName(_fromUtf8("lineEditArea"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEditArea)
        self.label_7 = QtGui.QLabel(Dialog_AE_selectRegion)
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_7)
        self.lineEditFGWID = QtGui.QLineEdit(Dialog_AE_selectRegion)
        self.lineEditFGWID.setMinimumSize(QtCore.QSize(0, 24))
        self.lineEditFGWID.setObjectName(_fromUtf8("lineEditFGWID"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineEditFGWID)
        self.gridLayout.addLayout(self.formLayout, 0, 0, 1, 1)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label = QtGui.QLabel(Dialog_AE_selectRegion)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout_2.addWidget(self.label)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.pushButtonSelect = QtGui.QPushButton(Dialog_AE_selectRegion)
        self.pushButtonSelect.setMinimumSize(QtCore.QSize(128, 24))
        self.pushButtonSelect.setObjectName(_fromUtf8("pushButtonSelect"))
        self.horizontalLayout_2.addWidget(self.pushButtonSelect)
        self.gridLayout.addLayout(self.horizontalLayout_2, 1, 0, 1, 1)
        self.line = QtGui.QFrame(Dialog_AE_selectRegion)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.gridLayout.addWidget(self.line, 2, 0, 1, 1)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label_2 = QtGui.QLabel(Dialog_AE_selectRegion)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout.addWidget(self.label_2)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.pushButtonAddArea = QtGui.QPushButton(Dialog_AE_selectRegion)
        self.pushButtonAddArea.setMinimumSize(QtCore.QSize(128, 24))
        self.pushButtonAddArea.setObjectName(_fromUtf8("pushButtonAddArea"))
        self.horizontalLayout.addWidget(self.pushButtonAddArea)
        self.gridLayout.addLayout(self.horizontalLayout, 3, 0, 1, 1)
        self.line_3 = QtGui.QFrame(Dialog_AE_selectRegion)
        self.line_3.setFrameShape(QtGui.QFrame.HLine)
        self.line_3.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_3.setObjectName(_fromUtf8("line_3"))
        self.gridLayout.addWidget(self.line_3, 4, 0, 1, 1)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.label_3 = QtGui.QLabel(Dialog_AE_selectRegion)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_3.addWidget(self.label_3)
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem2)
        self.pushButtonSave = QtGui.QPushButton(Dialog_AE_selectRegion)
        self.pushButtonSave.setMinimumSize(QtCore.QSize(128, 24))
        self.pushButtonSave.setObjectName(_fromUtf8("pushButtonSave"))
        self.horizontalLayout_3.addWidget(self.pushButtonSave)
        self.gridLayout.addLayout(self.horizontalLayout_3, 5, 0, 1, 1)
        self.line_2 = QtGui.QFrame(Dialog_AE_selectRegion)
        self.line_2.setFrameShape(QtGui.QFrame.HLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName(_fromUtf8("line_2"))
        self.gridLayout.addWidget(self.line_2, 6, 0, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)

        self.retranslateUi(Dialog_AE_selectRegion)
        QtCore.QMetaObject.connectSlotsByName(Dialog_AE_selectRegion)

    def retranslateUi(self, Dialog_AE_selectRegion):
        Dialog_AE_selectRegion.setWindowTitle(_translate("Dialog_AE_selectRegion", "Dialog", None))
        self.label_6.setText(_translate("Dialog_AE_selectRegion", "Name für das Gebiet angeben", None))
        self.label_7.setText(_translate("Dialog_AE_selectRegion", "Fließgewässer-ID angeben           ", None))
        self.label.setText(_translate("Dialog_AE_selectRegion", "Gebietsselektion durchführen", None))
        self.pushButtonSelect.setText(_translate("Dialog_AE_selectRegion", "Selektieren", None))
        self.label_2.setText(_translate("Dialog_AE_selectRegion", "Gebiet um selektierte Teilgebiete erweitern ", None))
        self.pushButtonAddArea.setText(_translate("Dialog_AE_selectRegion", "Zuweisen", None))
        self.label_3.setText(_translate("Dialog_AE_selectRegion", "Neues Modell erzeugen", None))
        self.pushButtonSave.setText(_translate("Dialog_AE_selectRegion", "Speichern", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog_AE_selectRegion = QtGui.QDialog()
    ui = Ui_Dialog_AE_selectRegion()
    ui.setupUi(Dialog_AE_selectRegion)
    Dialog_AE_selectRegion.show()
    sys.exit(app.exec_())

