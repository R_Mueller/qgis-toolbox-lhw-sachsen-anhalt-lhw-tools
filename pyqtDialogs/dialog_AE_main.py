# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_AE_Main_tab.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog_AE_main(object):
    def setupUi(self, Dialog_AE_main):
        Dialog_AE_main.setObjectName(_fromUtf8("Dialog_AE_main"))
        Dialog_AE_main.resize(371, 162)
        self.gridLayout_6 = QtGui.QGridLayout(Dialog_AE_main)
        self.gridLayout_6.setObjectName(_fromUtf8("gridLayout_6"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label_3 = QtGui.QLabel(Dialog_AE_main)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout.addWidget(self.label_3)
        self.pushButtonLoad = QtGui.QPushButton(Dialog_AE_main)
        self.pushButtonLoad.setMinimumSize(QtCore.QSize(0, 24))
        self.pushButtonLoad.setObjectName(_fromUtf8("pushButtonLoad"))
        self.horizontalLayout.addWidget(self.pushButtonLoad)
        self.gridLayout_6.addLayout(self.horizontalLayout, 0, 0, 1, 1)
        self.tabWidget = QtGui.QTabWidget(Dialog_AE_main)
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.tab = QtGui.QWidget()
        self.tab.setObjectName(_fromUtf8("tab"))
        self.gridLayout_2 = QtGui.QGridLayout(self.tab)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label = QtGui.QLabel(self.tab)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.label_2 = QtGui.QLabel(self.tab)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 0, 1, 1, 1)
        self.pushButtonLoadArea = QtGui.QPushButton(self.tab)
        self.pushButtonLoadArea.setMinimumSize(QtCore.QSize(140, 24))
        self.pushButtonLoadArea.setObjectName(_fromUtf8("pushButtonLoadArea"))
        self.gridLayout.addWidget(self.pushButtonLoadArea, 1, 0, 1, 1)
        self.pushButtonSelectArea = QtGui.QPushButton(self.tab)
        self.pushButtonSelectArea.setMinimumSize(QtCore.QSize(140, 24))
        self.pushButtonSelectArea.setObjectName(_fromUtf8("pushButtonSelectArea"))
        self.gridLayout.addWidget(self.pushButtonSelectArea, 1, 1, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab, _fromUtf8(""))
        self.tab_2 = QtGui.QWidget()
        self.tab_2.setObjectName(_fromUtf8("tab_2"))
        self.gridLayout_5 = QtGui.QGridLayout(self.tab_2)
        self.gridLayout_5.setObjectName(_fromUtf8("gridLayout_5"))
        self.pushButtonDB = QtGui.QPushButton(self.tab_2)
        self.pushButtonDB.setMinimumSize(QtCore.QSize(128, 24))
        self.pushButtonDB.setObjectName(_fromUtf8("pushButtonDB"))
        self.gridLayout_5.addWidget(self.pushButtonDB, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab_2, _fromUtf8(""))
        self.tab_3 = QtGui.QWidget()
        self.tab_3.setObjectName(_fromUtf8("tab_3"))
        self.gridLayout_4 = QtGui.QGridLayout(self.tab_3)
        self.gridLayout_4.setObjectName(_fromUtf8("gridLayout_4"))
        self.pushButtonStartAE = QtGui.QPushButton(self.tab_3)
        self.pushButtonStartAE.setMinimumSize(QtCore.QSize(128, 24))
        self.pushButtonStartAE.setObjectName(_fromUtf8("pushButtonStartAE"))
        self.gridLayout_4.addWidget(self.pushButtonStartAE, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab_3, _fromUtf8(""))
        self.tab_4 = QtGui.QWidget()
        self.tab_4.setObjectName(_fromUtf8("tab_4"))
        self.gridLayout_3 = QtGui.QGridLayout(self.tab_4)
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.pushButtonShowResults = QtGui.QPushButton(self.tab_4)
        self.pushButtonShowResults.setMinimumSize(QtCore.QSize(128, 24))
        self.pushButtonShowResults.setObjectName(_fromUtf8("pushButtonShowResults"))
        self.gridLayout_3.addWidget(self.pushButtonShowResults, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab_4, _fromUtf8(""))
        self.gridLayout_6.addWidget(self.tabWidget, 1, 0, 1, 1)

        self.retranslateUi(Dialog_AE_main)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(Dialog_AE_main)

    def retranslateUi(self, Dialog_AE_main):
        Dialog_AE_main.setWindowTitle(_translate("Dialog_AE_main", "Untersuchungen mit ArcEGMO", None))
        self.label_3.setText(_translate("Dialog_AE_main", "Kartenmaterial Fließgewässer", None))
        self.pushButtonLoad.setText(_translate("Dialog_AE_main", "Laden", None))
        self.label.setText(_translate("Dialog_AE_main", "Lade ein gepeichertes Gebiet", None))
        self.label_2.setText(_translate("Dialog_AE_main", "Selektiere ein Gebiet aus Karte", None))
        self.pushButtonLoadArea.setText(_translate("Dialog_AE_main", "Gebiet laden", None))
        self.pushButtonSelectArea.setText(_translate("Dialog_AE_main", "Gebiet selektieren", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("Dialog_AE_main", "Gebietsauswahl", None))
        self.pushButtonDB.setText(_translate("Dialog_AE_main", "Editieren", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("Dialog_AE_main", "Datenbestand", None))
        self.pushButtonStartAE.setText(_translate("Dialog_AE_main", "Durchführen", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), _translate("Dialog_AE_main", "Rechnung", None))
        self.pushButtonShowResults.setText(_translate("Dialog_AE_main", "Anzeigen", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_4), _translate("Dialog_AE_main", "Ergebnisse", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog_AE_main = QtGui.QDialog()
    ui = Ui_Dialog_AE_main()
    ui.setupUi(Dialog_AE_main)
    Dialog_AE_main.show()
    sys.exit(app.exec_())

