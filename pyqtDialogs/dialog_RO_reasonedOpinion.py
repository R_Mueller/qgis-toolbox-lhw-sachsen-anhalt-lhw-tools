# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_RO_reasonedOpinion.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog_RO_reasonedOpinion(object):
    def setupUi(self, Dialog_RO_reasonedOpinion):
        Dialog_RO_reasonedOpinion.setObjectName(_fromUtf8("Dialog_RO_reasonedOpinion"))
        Dialog_RO_reasonedOpinion.resize(274, 362)
        font = QtGui.QFont()
        font.setStrikeOut(False)
        Dialog_RO_reasonedOpinion.setFont(font)
        self.gridLayout_2 = QtGui.QGridLayout(Dialog_RO_reasonedOpinion)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label_2 = QtGui.QLabel(Dialog_RO_reasonedOpinion)
        self.label_2.setMinimumSize(QtCore.QSize(0, 24))
        font = QtGui.QFont()
        font.setBold(True)
        font.setUnderline(True)
        font.setWeight(75)
        font.setStrikeOut(False)
        self.label_2.setFont(font)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 1)
        self.line = QtGui.QFrame(Dialog_RO_reasonedOpinion)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.gridLayout.addWidget(self.line, 1, 0, 1, 1)
        self.label_3 = QtGui.QLabel(Dialog_RO_reasonedOpinion)
        self.label_3.setMinimumSize(QtCore.QSize(0, 24))
        font = QtGui.QFont()
        font.setStrikeOut(False)
        self.label_3.setFont(font)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.gridLayout.addWidget(self.label_3, 2, 0, 1, 1)
        self.checkBox_2 = QtGui.QCheckBox(Dialog_RO_reasonedOpinion)
        self.checkBox_2.setMinimumSize(QtCore.QSize(0, 24))
        font = QtGui.QFont()
        font.setStrikeOut(False)
        self.checkBox_2.setFont(font)
        self.checkBox_2.setObjectName(_fromUtf8("checkBox_2"))
        self.gridLayout.addWidget(self.checkBox_2, 3, 0, 1, 1)
        self.checkBox = QtGui.QCheckBox(Dialog_RO_reasonedOpinion)
        self.checkBox.setMinimumSize(QtCore.QSize(0, 24))
        font = QtGui.QFont()
        font.setStrikeOut(False)
        self.checkBox.setFont(font)
        self.checkBox.setObjectName(_fromUtf8("checkBox"))
        self.gridLayout.addWidget(self.checkBox, 4, 0, 1, 1)
        self.line_2 = QtGui.QFrame(Dialog_RO_reasonedOpinion)
        self.line_2.setFrameShape(QtGui.QFrame.HLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName(_fromUtf8("line_2"))
        self.gridLayout.addWidget(self.line_2, 5, 0, 1, 1)
        self.label = QtGui.QLabel(Dialog_RO_reasonedOpinion)
        self.label.setMinimumSize(QtCore.QSize(0, 24))
        font = QtGui.QFont()
        font.setStrikeOut(False)
        self.label.setFont(font)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 6, 0, 1, 1)
        self.textBrowser = QtGui.QTextBrowser(Dialog_RO_reasonedOpinion)
        self.textBrowser.setObjectName(_fromUtf8("textBrowser"))
        self.gridLayout.addWidget(self.textBrowser, 7, 0, 1, 1)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.pushButton_3 = QtGui.QPushButton(Dialog_RO_reasonedOpinion)
        self.pushButton_3.setMinimumSize(QtCore.QSize(0, 24))
        self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
        self.horizontalLayout.addWidget(self.pushButton_3)
        self.pushButton_2 = QtGui.QPushButton(Dialog_RO_reasonedOpinion)
        self.pushButton_2.setMinimumSize(QtCore.QSize(0, 24))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.horizontalLayout.addWidget(self.pushButton_2)
        self.pushButton = QtGui.QPushButton(Dialog_RO_reasonedOpinion)
        self.pushButton.setMinimumSize(QtCore.QSize(0, 24))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.horizontalLayout.addWidget(self.pushButton)
        self.gridLayout.addLayout(self.horizontalLayout, 8, 0, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)

        self.retranslateUi(Dialog_RO_reasonedOpinion)
        QtCore.QMetaObject.connectSlotsByName(Dialog_RO_reasonedOpinion)

    def retranslateUi(self, Dialog_RO_reasonedOpinion):
        Dialog_RO_reasonedOpinion.setWindowTitle(_translate("Dialog_RO_reasonedOpinion", "Dialog", None))
        self.label_2.setText(_translate("Dialog_RO_reasonedOpinion", "Gutachen vorbereiten", None))
        self.label_3.setText(_translate("Dialog_RO_reasonedOpinion", "Einstellungen", None))
        self.checkBox_2.setText(_translate("Dialog_RO_reasonedOpinion", "Einstellung 1", None))
        self.checkBox.setText(_translate("Dialog_RO_reasonedOpinion", "Einstellung 2", None))
        self.label.setText(_translate("Dialog_RO_reasonedOpinion", "Anmerkungen", None))
        self.pushButton_3.setText(_translate("Dialog_RO_reasonedOpinion", "Abbrechen", None))
        self.pushButton_2.setText(_translate("Dialog_RO_reasonedOpinion", "Vorschau", None))
        self.pushButton.setText(_translate("Dialog_RO_reasonedOpinion", "Speichern", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog_RO_reasonedOpinion = QtGui.QDialog()
    ui = Ui_Dialog_RO_reasonedOpinion()
    ui.setupUi(Dialog_RO_reasonedOpinion)
    Dialog_RO_reasonedOpinion.show()
    sys.exit(app.exec_())

