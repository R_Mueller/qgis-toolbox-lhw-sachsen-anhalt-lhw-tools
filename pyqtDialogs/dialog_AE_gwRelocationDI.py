# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_AE_gwRelocationDI.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog_AE_gwRelocationDI(object):
    def setupUi(self, Dialog_AE_gwRelocationDI):
        Dialog_AE_gwRelocationDI.setObjectName(_fromUtf8("Dialog_AE_gwRelocationDI"))
        Dialog_AE_gwRelocationDI.resize(303, 211)
        self.gridLayout = QtGui.QGridLayout(Dialog_AE_gwRelocationDI)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.Bezeichnung_txt_2 = QtGui.QLabel(Dialog_AE_gwRelocationDI)
        font = QtGui.QFont()
        font.setPointSize(9)
        font.setBold(True)
        font.setUnderline(True)
        font.setWeight(75)
        self.Bezeichnung_txt_2.setFont(font)
        self.Bezeichnung_txt_2.setObjectName(_fromUtf8("Bezeichnung_txt_2"))
        self.verticalLayout.addWidget(self.Bezeichnung_txt_2)
        self.formLayout = QtGui.QFormLayout()
        self.formLayout.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.Lagekoordinaten = QtGui.QLabel(Dialog_AE_gwRelocationDI)
        self.Lagekoordinaten.setObjectName(_fromUtf8("Lagekoordinaten"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.Lagekoordinaten)
        self.X_Koordinate = QtGui.QLineEdit(Dialog_AE_gwRelocationDI)
        self.X_Koordinate.setObjectName(_fromUtf8("X_Koordinate"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.X_Koordinate)
        self.Bezugsflaeche_txt = QtGui.QLabel(Dialog_AE_gwRelocationDI)
        self.Bezugsflaeche_txt.setObjectName(_fromUtf8("Bezugsflaeche_txt"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.Bezugsflaeche_txt)
        self.Bezugsflaeche = QtGui.QLineEdit(Dialog_AE_gwRelocationDI)
        self.Bezugsflaeche.setObjectName(_fromUtf8("Bezugsflaeche"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.Bezugsflaeche)
        self.Bezeichnung_txt = QtGui.QLabel(Dialog_AE_gwRelocationDI)
        self.Bezeichnung_txt.setObjectName(_fromUtf8("Bezeichnung_txt"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.Bezeichnung_txt)
        self.spinBox = QtGui.QSpinBox(Dialog_AE_gwRelocationDI)
        self.spinBox.setObjectName(_fromUtf8("spinBox"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.spinBox)
        self.verticalLayout.addLayout(self.formLayout)
        self.line = QtGui.QFrame(Dialog_AE_gwRelocationDI)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.verticalLayout.addWidget(self.line)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.pushButton = QtGui.QPushButton(Dialog_AE_gwRelocationDI)
        self.pushButton.setMinimumSize(QtCore.QSize(128, 0))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.horizontalLayout.addWidget(self.pushButton)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.pushButton_3 = QtGui.QPushButton(Dialog_AE_gwRelocationDI)
        self.pushButton_3.setMinimumSize(QtCore.QSize(128, 0))
        self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
        self.horizontalLayout.addWidget(self.pushButton_3)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(Dialog_AE_gwRelocationDI)
        QtCore.QMetaObject.connectSlotsByName(Dialog_AE_gwRelocationDI)

    def retranslateUi(self, Dialog_AE_gwRelocationDI):
        Dialog_AE_gwRelocationDI.setWindowTitle(_translate("Dialog_AE_gwRelocationDI", "Dialog", None))
        self.Bezeichnung_txt_2.setText(_translate("Dialog_AE_gwRelocationDI", "Grundwasserverlagerung", None))
        self.Lagekoordinaten.setText(_translate("Dialog_AE_gwRelocationDI", "TG-ID Ursprungsgebiet", None))
        self.Bezugsflaeche_txt.setText(_translate("Dialog_AE_gwRelocationDI", "TG-ID Zielgebiet", None))
        self.Bezeichnung_txt.setText(_translate("Dialog_AE_gwRelocationDI", "Anteil der GW-Verlagerung [%]", None))
        self.pushButton.setText(_translate("Dialog_AE_gwRelocationDI", "Abbrechen", None))
        self.pushButton_3.setText(_translate("Dialog_AE_gwRelocationDI", "Übernehmen", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog_AE_gwRelocationDI = QtGui.QDialog()
    ui = Ui_Dialog_AE_gwRelocationDI()
    ui.setupUi(Dialog_AE_gwRelocationDI)
    Dialog_AE_gwRelocationDI.show()
    sys.exit(app.exec_())

