# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_RO_extractionConst.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog_RO_extractionConst(object):
    def setupUi(self, Dialog_RO_extractionConst):
        Dialog_RO_extractionConst.setObjectName(_fromUtf8("Dialog_RO_extractionConst"))
        Dialog_RO_extractionConst.resize(258, 108)
        self.gridLayout_2 = QtGui.QGridLayout(Dialog_RO_extractionConst)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label = QtGui.QLabel(Dialog_RO_extractionConst)
        self.label.setMinimumSize(QtCore.QSize(0, 24))
        font = QtGui.QFont()
        font.setBold(True)
        font.setUnderline(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.lineEdit = QtGui.QLineEdit(Dialog_RO_extractionConst)
        self.lineEdit.setMinimumSize(QtCore.QSize(0, 24))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.gridLayout.addWidget(self.lineEdit, 1, 0, 1, 1)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.pushButtonReject = QtGui.QPushButton(Dialog_RO_extractionConst)
        self.pushButtonReject.setMinimumSize(QtCore.QSize(0, 24))
        self.pushButtonReject.setObjectName(_fromUtf8("pushButtonReject"))
        self.horizontalLayout.addWidget(self.pushButtonReject)
        self.pushButtonAccept = QtGui.QPushButton(Dialog_RO_extractionConst)
        self.pushButtonAccept.setMinimumSize(QtCore.QSize(0, 24))
        self.pushButtonAccept.setObjectName(_fromUtf8("pushButtonAccept"))
        self.horizontalLayout.addWidget(self.pushButtonAccept)
        self.gridLayout.addLayout(self.horizontalLayout, 2, 0, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)

        self.retranslateUi(Dialog_RO_extractionConst)
        QtCore.QMetaObject.connectSlotsByName(Dialog_RO_extractionConst)

    def retranslateUi(self, Dialog_RO_extractionConst):
        Dialog_RO_extractionConst.setWindowTitle(_translate("Dialog_RO_extractionConst", "Dialog", None))
        self.label.setText(_translate("Dialog_RO_extractionConst", "Jährlich konstante Entnahme [Tm³/Jahr]", None))
        self.lineEdit.setText(_translate("Dialog_RO_extractionConst", "0", None))
        self.pushButtonReject.setText(_translate("Dialog_RO_extractionConst", "Verwerfen", None))
        self.pushButtonAccept.setText(_translate("Dialog_RO_extractionConst", "Übernehmen", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog_RO_extractionConst = QtGui.QDialog()
    ui = Ui_Dialog_RO_extractionConst()
    ui.setupUi(Dialog_RO_extractionConst)
    Dialog_RO_extractionConst.show()
    sys.exit(app.exec_())

