# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_AE_gwRelocation.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog_AE_gwRelocation(object):
    def setupUi(self, Dialog_AE_gwRelocation):
        Dialog_AE_gwRelocation.setObjectName(_fromUtf8("Dialog_AE_gwRelocation"))
        Dialog_AE_gwRelocation.resize(382, 241)
        self.gridLayout = QtGui.QGridLayout(Dialog_AE_gwRelocation)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.Bezeichnung_txt_2 = QtGui.QLabel(Dialog_AE_gwRelocation)
        font = QtGui.QFont()
        font.setPointSize(9)
        font.setBold(True)
        font.setUnderline(True)
        font.setWeight(75)
        self.Bezeichnung_txt_2.setFont(font)
        self.Bezeichnung_txt_2.setObjectName(_fromUtf8("Bezeichnung_txt_2"))
        self.verticalLayout.addWidget(self.Bezeichnung_txt_2)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label = QtGui.QLabel(Dialog_AE_gwRelocation)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout_2.addWidget(self.label)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.pushButton = QtGui.QPushButton(Dialog_AE_gwRelocation)
        self.pushButton.setMinimumSize(QtCore.QSize(128, 0))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.horizontalLayout_2.addWidget(self.pushButton)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.line = QtGui.QFrame(Dialog_AE_gwRelocation)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.verticalLayout.addWidget(self.line)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label_2 = QtGui.QLabel(Dialog_AE_gwRelocation)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout.addWidget(self.label_2)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.pushButton_2 = QtGui.QPushButton(Dialog_AE_gwRelocation)
        self.pushButton_2.setMinimumSize(QtCore.QSize(128, 0))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.horizontalLayout.addWidget(self.pushButton_2)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem2)
        self.pushButton_3 = QtGui.QPushButton(Dialog_AE_gwRelocation)
        self.pushButton_3.setMinimumSize(QtCore.QSize(128, 0))
        self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
        self.horizontalLayout_4.addWidget(self.pushButton_3)
        self.verticalLayout.addLayout(self.horizontalLayout_4)
        self.line_2 = QtGui.QFrame(Dialog_AE_gwRelocation)
        self.line_2.setFrameShape(QtGui.QFrame.HLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName(_fromUtf8("line_2"))
        self.verticalLayout.addWidget(self.line_2)
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        spacerItem3 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem3)
        self.pushButton_4 = QtGui.QPushButton(Dialog_AE_gwRelocation)
        self.pushButton_4.setMinimumSize(QtCore.QSize(128, 0))
        self.pushButton_4.setObjectName(_fromUtf8("pushButton_4"))
        self.horizontalLayout_5.addWidget(self.pushButton_4)
        self.verticalLayout.addLayout(self.horizontalLayout_5)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(Dialog_AE_gwRelocation)
        QtCore.QMetaObject.connectSlotsByName(Dialog_AE_gwRelocation)

    def retranslateUi(self, Dialog_AE_gwRelocation):
        Dialog_AE_gwRelocation.setWindowTitle(_translate("Dialog_AE_gwRelocation", "Dialog", None))
        self.Bezeichnung_txt_2.setText(_translate("Dialog_AE_gwRelocation", "Grundwasserverlagerung", None))
        self.label.setText(_translate("Dialog_AE_gwRelocation", "Bestehende GW-Verlagerungen darstellen", None))
        self.pushButton.setText(_translate("Dialog_AE_gwRelocation", "Darstellen", None))
        self.label_2.setText(_translate("Dialog_AE_gwRelocation", "Neue GW-Verlagerung hinzufügen", None))
        self.pushButton_2.setText(_translate("Dialog_AE_gwRelocation", "Pfeilwerkzeug aktivieren", None))
        self.pushButton_3.setText(_translate("Dialog_AE_gwRelocation", "Direkteingabefenster", None))
        self.pushButton_4.setText(_translate("Dialog_AE_gwRelocation", "Anwenden", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog_AE_gwRelocation = QtGui.QDialog()
    ui = Ui_Dialog_AE_gwRelocation()
    ui.setupUi(Dialog_AE_gwRelocation)
    Dialog_AE_gwRelocation.show()
    sys.exit(app.exec_())

