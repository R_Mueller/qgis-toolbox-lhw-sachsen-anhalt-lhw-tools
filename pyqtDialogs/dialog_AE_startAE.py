# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_AE_startAE.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog_AE_startAE(object):
    def setupUi(self, Dialog_AE_startAE):
        Dialog_AE_startAE.setObjectName(_fromUtf8("Dialog_AE_startAE"))
        Dialog_AE_startAE.resize(263, 253)
        self.verticalLayoutWidget = QtGui.QWidget(Dialog_AE_startAE)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 241, 231))
        self.verticalLayoutWidget.setObjectName(_fromUtf8("verticalLayoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label = QtGui.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(9)
        font.setBold(True)
        font.setUnderline(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout.addWidget(self.label)
        self.line = QtGui.QFrame(self.verticalLayoutWidget)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.verticalLayout.addWidget(self.line)
        self.formLayout = QtGui.QFormLayout()
        self.formLayout.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.label_2 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label_2)
        self.dateEditStart = QtGui.QDateEdit(self.verticalLayoutWidget)
        self.dateEditStart.setMinimumSize(QtCore.QSize(0, 24))
        self.dateEditStart.setObjectName(_fromUtf8("dateEditStart"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.dateEditStart)
        self.label_3 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_3)
        self.dateEditEnd = QtGui.QDateEdit(self.verticalLayoutWidget)
        self.dateEditEnd.setMinimumSize(QtCore.QSize(0, 24))
        self.dateEditEnd.setObjectName(_fromUtf8("dateEditEnd"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.dateEditEnd)
        self.verticalLayout.addLayout(self.formLayout)
        self.line_2 = QtGui.QFrame(self.verticalLayoutWidget)
        self.line_2.setFrameShape(QtGui.QFrame.HLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName(_fromUtf8("line_2"))
        self.verticalLayout.addWidget(self.line_2)
        self.label_4 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.verticalLayout.addWidget(self.label_4)
        self.label_5 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.verticalLayout.addWidget(self.label_5)
        self.lineEditVariant = QtGui.QLineEdit(self.verticalLayoutWidget)
        self.lineEditVariant.setMinimumSize(QtCore.QSize(0, 24))
        self.lineEditVariant.setObjectName(_fromUtf8("lineEditVariant"))
        self.verticalLayout.addWidget(self.lineEditVariant)
        self.line_3 = QtGui.QFrame(self.verticalLayoutWidget)
        self.line_3.setFrameShape(QtGui.QFrame.HLine)
        self.line_3.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_3.setObjectName(_fromUtf8("line_3"))
        self.verticalLayout.addWidget(self.line_3)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.pushButtonStart = QtGui.QPushButton(self.verticalLayoutWidget)
        self.pushButtonStart.setMinimumSize(QtCore.QSize(128, 24))
        self.pushButtonStart.setObjectName(_fromUtf8("pushButtonStart"))
        self.horizontalLayout_2.addWidget(self.pushButtonStart)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.retranslateUi(Dialog_AE_startAE)
        QtCore.QMetaObject.connectSlotsByName(Dialog_AE_startAE)

    def retranslateUi(self, Dialog_AE_startAE):
        Dialog_AE_startAE.setWindowTitle(_translate("Dialog_AE_startAE", "Dialog", None))
        self.label.setText(_translate("Dialog_AE_startAE", "ArcEGMO-Rechnung", None))
        self.label_2.setText(_translate("Dialog_AE_startAE", "Berechnungsbeginn", None))
        self.label_3.setText(_translate("Dialog_AE_startAE", "Berechnungsende  ", None))
        self.label_4.setText(_translate("Dialog_AE_startAE", "Name der Berechnungsvariante:", None))
        self.label_5.setText(_translate("Dialog_AE_startAE", "(Ganzes Wort ohne Umlaute und Leerzeichen)", None))
        self.pushButtonStart.setText(_translate("Dialog_AE_startAE", "Rechnung starten", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog_AE_startAE = QtGui.QDialog()
    ui = Ui_Dialog_AE_startAE()
    ui.setupUi(Dialog_AE_startAE)
    Dialog_AE_startAE.show()
    sys.exit(app.exec_())

