# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_RO_components.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog_RO_components(object):
    def setupUi(self, Dialog_RO_components):
        Dialog_RO_components.setObjectName(_fromUtf8("Dialog_RO_components"))
        Dialog_RO_components.resize(309, 236)
        self.gridLayout_3 = QtGui.QGridLayout(Dialog_RO_components)
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.gridLayout_2 = QtGui.QGridLayout()
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label_04 = QtGui.QLabel(Dialog_RO_components)
        self.label_04.setMinimumSize(QtCore.QSize(0, 20))
        font = QtGui.QFont()
        font.setBold(True)
        font.setUnderline(True)
        font.setWeight(75)
        self.label_04.setFont(font)
        self.label_04.setObjectName(_fromUtf8("label_04"))
        self.verticalLayout.addWidget(self.label_04)
        self.label_05 = QtGui.QLabel(Dialog_RO_components)
        self.label_05.setMinimumSize(QtCore.QSize(12, 20))
        font = QtGui.QFont()
        font.setBold(True)
        font.setUnderline(True)
        font.setWeight(75)
        self.label_05.setFont(font)
        self.label_05.setObjectName(_fromUtf8("label_05"))
        self.verticalLayout.addWidget(self.label_05)
        self.gridLayout_2.addLayout(self.verticalLayout, 0, 0, 1, 1)
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label_06 = QtGui.QLabel(Dialog_RO_components)
        self.label_06.setMinimumSize(QtCore.QSize(0, 24))
        self.label_06.setObjectName(_fromUtf8("label_06"))
        self.gridLayout.addWidget(self.label_06, 0, 1, 1, 1)
        self.checkBox_02 = QtGui.QCheckBox(Dialog_RO_components)
        self.checkBox_02.setEnabled(True)
        self.checkBox_02.setText(_fromUtf8(""))
        self.checkBox_02.setCheckable(True)
        self.checkBox_02.setChecked(True)
        self.checkBox_02.setObjectName(_fromUtf8("checkBox_02"))
        self.gridLayout.addWidget(self.checkBox_02, 1, 0, 1, 1)
        self.checkBox_03 = QtGui.QCheckBox(Dialog_RO_components)
        self.checkBox_03.setEnabled(True)
        self.checkBox_03.setText(_fromUtf8(""))
        self.checkBox_03.setCheckable(True)
        self.checkBox_03.setChecked(True)
        self.checkBox_03.setObjectName(_fromUtf8("checkBox_03"))
        self.gridLayout.addWidget(self.checkBox_03, 2, 0, 1, 1)
        self.checkBox_04 = QtGui.QCheckBox(Dialog_RO_components)
        self.checkBox_04.setEnabled(True)
        self.checkBox_04.setText(_fromUtf8(""))
        self.checkBox_04.setCheckable(True)
        self.checkBox_04.setChecked(False)
        self.checkBox_04.setObjectName(_fromUtf8("checkBox_04"))
        self.gridLayout.addWidget(self.checkBox_04, 3, 0, 1, 1)
        self.label_10 = QtGui.QLabel(Dialog_RO_components)
        self.label_10.setMinimumSize(QtCore.QSize(0, 24))
        self.label_10.setObjectName(_fromUtf8("label_10"))
        self.gridLayout.addWidget(self.label_10, 2, 1, 1, 1)
        self.label_08 = QtGui.QLabel(Dialog_RO_components)
        self.label_08.setMinimumSize(QtCore.QSize(0, 24))
        self.label_08.setObjectName(_fromUtf8("label_08"))
        self.gridLayout.addWidget(self.label_08, 1, 1, 1, 1)
        self.label_12 = QtGui.QLabel(Dialog_RO_components)
        self.label_12.setMinimumSize(QtCore.QSize(0, 24))
        self.label_12.setObjectName(_fromUtf8("label_12"))
        self.gridLayout.addWidget(self.label_12, 3, 1, 1, 1)
        self.checkBox_01 = QtGui.QCheckBox(Dialog_RO_components)
        self.checkBox_01.setEnabled(True)
        self.checkBox_01.setText(_fromUtf8(""))
        self.checkBox_01.setCheckable(True)
        self.checkBox_01.setChecked(True)
        self.checkBox_01.setObjectName(_fromUtf8("checkBox_01"))
        self.gridLayout.addWidget(self.checkBox_01, 0, 0, 1, 1)
        self.label_07 = QtGui.QLabel(Dialog_RO_components)
        self.label_07.setMinimumSize(QtCore.QSize(0, 24))
        self.label_07.setObjectName(_fromUtf8("label_07"))
        self.gridLayout.addWidget(self.label_07, 0, 2, 1, 1)
        self.label_09 = QtGui.QLabel(Dialog_RO_components)
        self.label_09.setMinimumSize(QtCore.QSize(0, 24))
        self.label_09.setObjectName(_fromUtf8("label_09"))
        self.gridLayout.addWidget(self.label_09, 1, 2, 1, 1)
        self.label_11 = QtGui.QLabel(Dialog_RO_components)
        self.label_11.setMinimumSize(QtCore.QSize(0, 24))
        self.label_11.setObjectName(_fromUtf8("label_11"))
        self.gridLayout.addWidget(self.label_11, 2, 2, 1, 1)
        self.label_13 = QtGui.QLabel(Dialog_RO_components)
        self.label_13.setMinimumSize(QtCore.QSize(0, 24))
        self.label_13.setObjectName(_fromUtf8("label_13"))
        self.gridLayout.addWidget(self.label_13, 3, 2, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 1, 0, 1, 1)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.pushButtonReject = QtGui.QPushButton(Dialog_RO_components)
        self.pushButtonReject.setMinimumSize(QtCore.QSize(0, 24))
        self.pushButtonReject.setObjectName(_fromUtf8("pushButtonReject"))
        self.horizontalLayout.addWidget(self.pushButtonReject)
        self.pushButtonAccept = QtGui.QPushButton(Dialog_RO_components)
        self.pushButtonAccept.setMinimumSize(QtCore.QSize(0, 24))
        self.pushButtonAccept.setObjectName(_fromUtf8("pushButtonAccept"))
        self.horizontalLayout.addWidget(self.pushButtonAccept)
        self.gridLayout_2.addLayout(self.horizontalLayout, 2, 0, 1, 1)
        self.gridLayout_3.addLayout(self.gridLayout_2, 0, 0, 1, 1)

        self.retranslateUi(Dialog_RO_components)
        QtCore.QMetaObject.connectSlotsByName(Dialog_RO_components)

    def retranslateUi(self, Dialog_RO_components):
        Dialog_RO_components.setWindowTitle(_translate("Dialog_RO_components", "Auswahl Abflusskomponenten", None))
        self.label_04.setText(_translate("Dialog_RO_components", "Unterirdische Abflusskomponenten zur Ermittlung", None))
        self.label_05.setText(_translate("Dialog_RO_components", "der Grundwasserneubildung auswählen", None))
        self.label_06.setText(_translate("Dialog_RO_components", "RG2", None))
        self.label_10.setText(_translate("Dialog_RO_components", "Rdrain", None))
        self.label_08.setText(_translate("Dialog_RO_components", "RG1", None))
        self.label_12.setText(_translate("Dialog_RO_components", "RH", None))
        self.label_07.setText(_translate("Dialog_RO_components", "langsamer GW-abfluss", None))
        self.label_09.setText(_translate("Dialog_RO_components", "schneller GW-abfluss", None))
        self.label_11.setText(_translate("Dialog_RO_components", "Drainabfluss", None))
        self.label_13.setText(_translate("Dialog_RO_components", "hypodermischer Abfluss", None))
        self.pushButtonReject.setText(_translate("Dialog_RO_components", "Verwerfen", None))
        self.pushButtonAccept.setText(_translate("Dialog_RO_components", "Übernehmen", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog_RO_components = QtGui.QDialog()
    ui = Ui_Dialog_RO_components()
    ui.setupUi(Dialog_RO_components)
    Dialog_RO_components.show()
    sys.exit(app.exec_())

