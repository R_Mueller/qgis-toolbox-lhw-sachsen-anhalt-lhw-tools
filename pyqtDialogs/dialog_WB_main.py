# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_WB_main.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog_WB_main(object):
    def setupUi(self, Dialog_WB_main):
        Dialog_WB_main.setObjectName(_fromUtf8("Dialog_WB_main"))
        Dialog_WB_main.resize(406, 463)
        self.gridLayout_2 = QtGui.QGridLayout(Dialog_WB_main)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label = QtGui.QLabel(Dialog_WB_main)
        font = QtGui.QFont()
        font.setBold(True)
        font.setUnderline(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.formLayout = QtGui.QFormLayout()
        self.formLayout.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.label_2 = QtGui.QLabel(Dialog_WB_main)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label_2)
        self.label_3 = QtGui.QLabel(Dialog_WB_main)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_3)
        self.pushButton_01 = QtGui.QPushButton(Dialog_WB_main)
        self.pushButton_01.setObjectName(_fromUtf8("pushButton_01"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.pushButton_01)
        self.label_activeLayer = QtGui.QLabel(Dialog_WB_main)
        self.label_activeLayer.setMinimumSize(QtCore.QSize(0, 18))
        self.label_activeLayer.setObjectName(_fromUtf8("label_activeLayer"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.label_activeLayer)
        self.gridLayout.addLayout(self.formLayout, 1, 0, 1, 1)
        self.line = QtGui.QFrame(Dialog_WB_main)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.gridLayout.addWidget(self.line, 2, 0, 1, 1)
        self.label_4 = QtGui.QLabel(Dialog_WB_main)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.gridLayout.addWidget(self.label_4, 3, 0, 1, 1)
        self.textEdit = QtGui.QTextEdit(Dialog_WB_main)
        self.textEdit.setObjectName(_fromUtf8("textEdit"))
        self.gridLayout.addWidget(self.textEdit, 4, 0, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)

        self.retranslateUi(Dialog_WB_main)
        QtCore.QMetaObject.connectSlotsByName(Dialog_WB_main)

    def retranslateUi(self, Dialog_WB_main):
        Dialog_WB_main.setWindowTitle(_translate("Dialog_WB_main", "Wasserhaushalt", None))
        self.label.setText(_translate("Dialog_WB_main", "Wasserhaushalt", None))
        self.label_2.setText(_translate("Dialog_WB_main", "Aktiver Layer ist:    ", None))
        self.label_3.setText(_translate("Dialog_WB_main", "Unterirdische Abflusskomponenten auswählen  ", None))
        self.pushButton_01.setText(_translate("Dialog_WB_main", "Auswahl", None))
        self.label_activeLayer.setText(_translate("Dialog_WB_main", "Aktiver Layer", None))
        self.label_4.setText(_translate("Dialog_WB_main", "Flächengemittelte Grundwasserneubildung nach ausgewählten Komponenten", None))
        self.textEdit.setHtml(_translate("Dialog_WB_main", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Komponente      [mm/a]</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">---------------------------</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">RG2                     100</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">RG1                     100</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Rdrain                  50</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">==============</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">GWN                    250</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">==============</span></p></body></html>", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog_WB_main = QtGui.QDialog()
    ui = Ui_Dialog_WB_main()
    ui.setupUi(Dialog_WB_main)
    Dialog_WB_main.show()
    sys.exit(app.exec_())

