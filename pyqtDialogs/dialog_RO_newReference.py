# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_RO_newReference.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog_RO_newReference(object):
    def setupUi(self, Dialog_RO_newReference):
        Dialog_RO_newReference.setObjectName(_fromUtf8("Dialog_RO_newReference"))
        Dialog_RO_newReference.resize(264, 103)
        self.gridLayout = QtGui.QGridLayout(Dialog_RO_newReference)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.gridLayout_2 = QtGui.QGridLayout()
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.label = QtGui.QLabel(Dialog_RO_newReference)
        self.label.setMinimumSize(QtCore.QSize(0, 24))
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout_2.addWidget(self.label, 0, 0, 1, 1)
        self.lineEdit = QtGui.QLineEdit(Dialog_RO_newReference)
        self.lineEdit.setMinimumSize(QtCore.QSize(0, 24))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.gridLayout_2.addWidget(self.lineEdit, 1, 0, 1, 1)
        self.pushButton = QtGui.QPushButton(Dialog_RO_newReference)
        self.pushButton.setMinimumSize(QtCore.QSize(24, 0))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.gridLayout_2.addWidget(self.pushButton, 2, 0, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_2, 0, 0, 1, 1)

        self.retranslateUi(Dialog_RO_newReference)
        QtCore.QMetaObject.connectSlotsByName(Dialog_RO_newReference)

    def retranslateUi(self, Dialog_RO_newReference):
        Dialog_RO_newReference.setWindowTitle(_translate("Dialog_RO_newReference", "Dialog", None))
        self.label.setText(_translate("Dialog_RO_newReference", "Neues Aktenzeichen für \"\" eingeben:", None))
        self.pushButton.setText(_translate("Dialog_RO_newReference", "Übernehmen", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog_RO_newReference = QtGui.QDialog()
    ui = Ui_Dialog_RO_newReference()
    ui.setupUi(Dialog_RO_newReference)
    Dialog_RO_newReference.show()
    sys.exit(app.exec_())

